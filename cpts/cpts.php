<?php
 add_action('init', 'register_cpts');

 function register_cpts(){

 	register_post_type( 'property',
    // CPT Options
      array(
          'labels' => array(
              'name' => __( 'Propriétés'),
              'singular_name' => __( 'Propriété' )
          ),
          'public' => true,
          'has_archive' => true,
          'rewrite' => array('slug' => 'property'),
          'show_in_rest' => true,
          'menu_icon' => 'dashicons-admin-home',
      //     'capability_type' => 'post',
				  // 'capabilities' => array(
				  //   'create_posts' => false,
				  // ),
				  // 'map_meta_cap' => true,
      )
    );

    register_post_type( 'proprietaire',
    // CPT Options
      array(
          'labels' => array(
              'name' => __( 'Propriétaires/Propriétés'),
              'singular_name' => __( 'Propriétaire/Propriété' )
          ),
          'public' => true,
          'has_archive' => true,
          'rewrite' => array('slug' => 'proprietaire'),
          'show_in_rest' => true,
          'menu_icon' => 'dashicons-buddicons-buddypress-logo',
          'supports'     => array( 'title', 'custom-fields', ),
      )
    );

    register_post_type( 'faqs',
      // CPT Options
          array(
              'labels' => array(
                  'name' => __( 'FAQs' ),
                  'singular_name' => __( 'FAQ' )
              ),
              'public' => true,
              'has_archive' => true,
              'rewrite' => array('slug' => 'faqs'),
              'show_in_rest' => true,
              'menu_icon' => 'dashicons-editor-spellcheck',
   
          )
      );

    register_post_type( 'reference',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Références' ),
                'singular_name' => __( 'Référence' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'reference'),
            'show_in_rest' => true,
            'supports'     => array( 'title', 'thumbnail', ),
            'menu_icon'     => 'dashicons-awards',
 
        )
    );

    register_post_type( 'temoignage',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Témoignages' ),
                'singular_name' => __( 'Témoignage' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'temoignage'),
            'show_in_rest' => true,
            'supports'     => array( 'title', 'thumbnail', ),
            'menu_icon'     => 'dashicons-awards',
 
        )
    );
 }

