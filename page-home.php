<?php
/**
 * Template Name: Accueil
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
$lead = false;
if( isset($_GET['sim']) && !empty( $_GET['sim'] ) && $_GET['sim'] == 'continue' ){
  if( isset($_GET['lid']) && !empty( $_GET['lid'] ) ){
    if( isset($_GET['email']) && !empty( $_GET['email'] ) ){
      $id = strip_tags( $_GET['lid'] );
      $email = strip_tags( $_GET['email'] );
        $lead = get_unfinished_lead_expertise($email);
        
      }
      
    }
  }
  

get_header(); ?>
<main>
    <section class="blocPrix reverse blue scroll">
        <div class="container">
            <div class="row justify-content-end">
                <div class="blocImg col-md-5 wow slideInLeft">
                    <img src="<?= IMG_DIR ?>bg-confiance.jpg" alt="">
                </div>
                 <div class="col-md-7 wow  slideInRight">
                    <h2 class="no-br-resp"><?php the_field('titre_1', 'option') ?><?php the_field('titre_2', 'option') ?></h2>
                    <div>
                        <?php the_field('paragraphe', 'option') ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="blocPrix scroll" id="Prix">
        <div class="container">
            <div class="row">
                <div class="col-md-7 wow slideInLeft">
                    <h2><?php the_field('p_titre') ?></h2>
                    <div class="d-flex flex-wrap">
                        <?php the_field('p_contenu') ?>
                    </div>
                    <div class="blocBtn d-flex justify-content-start align-items-center">
                        <?php $b1 = get_field('p_bouton_estimer'); ?>
                        <a href="<?= $b1['url'] ?>" class="btn scroll"><?= $b1['title'] ?></a>
                        <?php $b2 = get_field('p_bouton_temoignage'); ?>
                        <a href="<?= $b2['url'] ?>" class="btn scroll"><?= $b2['title'] ?></a>
                    </div>
                </div>
                <div class="blocImg col-md-5 wow slideInRight">
                    <img src="<?php the_field('p_image') ?>" alt="">
                </div>
            </div>
        </div>
    </section>

    <section id="blocForm" class="blocForm wow fadeIn">
        <div class="container">
            <div class="sousForm">
                <h2 id="title" class="wow fadeInDown"><?php the_field('f_titre_1') ?><?php the_field('f_titre_2') ?></h2>
            
                <div class="top-step"></div>
                <div class="formText" id="formText" style="margin-bottom:15px"><?php the_field('f_contenu') ?></div>

                <form id="home_form" class="formulaireA wow fadeInUp">                    
                    <div id="form_content">
                        <?php 
                            $step_to_show = 0;
                            // require_once ASTRA_THEME_CHILD_DIR . 'steps/s1.php'; 
                            require_once ASTRA_THEME_CHILD_DIR . 'steps/s'. $step_to_show .'.php'; 
                        ?>
                    </div>                    
                    <div id="response"></div>
                </form>
                <div class="clear"></div>
            
            </div>
        </div>
    </section>

    <?php  get_template_part( 'template-parts/content/bloc', 'temoignage' ); ?>
   
    <?php
    $args = array(
        'post_type' => 'reference',
        'posts_per_page' => -1
    );

    $q = new WP_Query( $args );

    if( $q->have_posts() ):
    ?>
        <section class="sec_logo">
            <div class="container">
                <div class="titre_logo">VU sur</div>
                <div class="listLogo">
                    <?php while( $q->have_posts() ): $q->the_post(); ?>
                   <span class="item">
                        <?php echo get_the_post_thumbnail( $post->ID, 'full', array( 'alt' => get_the_title() ) ) ?>
                    </span>
                   <?php endwhile; ?> 
                </div>
            </div>
        </section>
    <?php endif; ?>
</main>
<div class="home blockUI blockOverlay"></div>


<script type="text/javascript">
        var $ = jQuery.noConflict();
        // MENU FIXED //
        $(window).scroll(function () {
            var posScroll = $(document).scrollTop();
            if (posScroll > 80) {
                $('.blcTop').addClass('fixed')
            } else {
                $('.blcTop').removeClass('fixed')
            }
        });
        $(".scroll").click(function() {
            var c = $(this).attr("href");
            $('html, body').animate({ scrollTop: $(c).offset().top - 100 }, 800, "linear");
            return false;
        });
    
    </script>
<?php get_footer(); ?>