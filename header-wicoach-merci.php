<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?><!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
    <?php astra_head_top(); ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <?php wp_head(); ?>
</head>

<header>
    

    <section class="header-banner header-wicoach-merci">
        <div class="top">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-10 col-md-12 wow fadeInUp">

                            <a href="#" class="logo wow fadeInUp">
                                <img src="<?= IMG_DIR ?>logo-wicoach.svg" alt="wicoach">
                            </a>
                            <div class="map-head">
                                <img src="<?= IMG_DIR ?>wicoach-map.png" alt="map">
                            </div>
                            <div class="merci">Merci</div>
                            <p>Nos coachs vous recontacteront dans les 48h.</p> 

                        <div class="btnTransform">
                                <a href="<?= site_url(); ?>" class="btn">retour à l'accueil</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </section>
  
</header>
