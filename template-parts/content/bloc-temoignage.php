<section id="Temoinage" class="blocTest text-center wow fadeIn">
    <h2>
        <?php 
            $pageHome = get_option('page_on_front');
            the_field('t_titre', $pageHome); 
        ?>
    </h2>
    <div class="slideTeste">
        <?php if(have_rows('temoignages',$pageHome)):while(have_rows('temoignages',$pageHome)):the_row(); ?>
            <div class="test">
                <div class="icon d-flex justify-content-center align-items-center">
                    <?php $etoile = get_sub_field('nb_etoile'); ?>
                    <?php for($i = 0; $i < $etoile; $i++){ ?>
                        <img src="<?php the_field('image_etoile',$pageHome) ?>" alt="<?php the_sub_field('prenom',$pageHome) ?> <?php the_sub_field('nom',$pageHome) ?>" />
                    <?php } ?>
                </div>
                <div class="text">
                    <?php the_sub_field('texte_test'); ?>
                </div>
                <strong class="text-uppercase"><?php the_sub_field('prenom',$pageHome) ?><span><?php the_sub_field('nom',$pageHome) ?></span></strong>
            </div>
        <?php endwhile; endif; ?>
    </div>
    <?php if( is_home() || is_front_page() ):?>
        <div class="btnTest d-flex justify-content-center align-items-center">
            <?php $link = get_field('bouton_recevoir'); ?>
            <a href="#blocForm" class="btn scroll"><?= $link['title'] ?></a>
        </div>
    <?php endif; ?>
</section>

<script type="text/javascript">
    var $ = jQuery.noConflict();
    $( function() {
      /* Slide Test */
        $('.slideTeste').slick({
            dots: true,
            infinite: true,
            autoplaySpeed: 4000,
            speed: 1000,
            slidesToShow: 3,
            slideToScroll: 1,
            centerMode:true,
            arrows: true,
            autoplay: false,
            pauseOnHover: false,
            responsive: [
                {
                    breakpoint: 1300,
                    settings: {
                        slidesToShow: 2,
                        centerMode:false
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        centerMode:false,
                        arrows:false
                    }
                }
            ]
        });
    } );
</script>