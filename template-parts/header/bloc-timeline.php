

<section class="sec_pourquoi sec_pourquoi_home">
    <div class="container">
        <?= get_field('titre_timeline') ?>
       <div class="listTimeline">
           <div class="item">
               <img src="<?= IMG_DIR ?>carrousel-etape1.png">
           </div>
           <div class="item">
               <img src="<?= IMG_DIR ?>carrousel-etape2.png">
           </div>
           <div class="item">
               <img src="<?= IMG_DIR ?>carrousel-etape3.png">
           </div>
           <div class="item">
               <img src="<?= IMG_DIR ?>carrousel-etape4.png">
           </div>
           <div class="item">
               <img src="<?= IMG_DIR ?>carrousel-etape5.png">
           </div>
           <div class="item">
               <img src="<?= IMG_DIR ?>carrousel-etape6.png">
           </div>
       </div>
    </div>   
</section>

<script type="text/javascript">
    var $ = jQuery.noConflict();
$(function() {
   $('.listTimeline').slick({
    dots: false,
    infinite: true,
    autoplaySpeed: 6000,
    speed: 980,
    cssEase: 'linear',
    arrows: true,
    autoplay: true,
    pauseOnHover: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [{
            breakpoint: 1280,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows:true,
                adaptiveHeight: true,
            }
        },
        {
            breakpoint: 601,
            settings: {
                slidesToShow: 1,
            }
        }
        

    ]
});

    
   

});
</script>