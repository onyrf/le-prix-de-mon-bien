<?php
  // raha checkout normal
  if( !is_wc_endpoint_url( 'order-received' ) ):
  ?>
  <header class="header-arson" style="background-image: url(<?php the_field('banner', 'option') ?>);" >
    <div class="top">
        <a href="<?= site_url(); ?>" class="logo wow fadeInUp">
            <img src="<?php the_field('logo','option') ?>" alt="">
        </a>
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-lg-10 col-md-12 wow fadeInUp">
                    <h1>Encore une étape avant <br>de recevoir votre book</h1>
                </div>
            </div>
        </div>
    </div>
    <a href="#customer_details" class="scroll scrollDown">Défilez pour terminer l'achat de votre book</a>
    <script type="text/javascript">
      jQuery(".scroll").click(function() {
          var c = jQuery(this).attr("href");
          jQuery('html, body').animate({ scrollTop: jQuery(c).offset().top }, 800, "linear");
          return false;
      });
    </script>
  </header>
<?php else: //ato ndray refa merci apres paiement 
    
?>
  <header class="header-arson <?php if(is_wc_endpoint_url( 'order-received' )){ echo 'order-received'; } ?> " style="background-image: url(<?php the_field('banner', 'option') ?>);" >
    <div class="top">
        <a href="<?= site_url(); ?>" class="logo wow fadeInUp">
            <img src="<?= IMG_DIR ?>logo-blanc.svg" alt="">
        </a>
        <div class="map-head">
            <img src="<?= IMG_DIR ?>map5.png" alt="map">
        </div>
    </div>
</header>

<?php 
    endif;