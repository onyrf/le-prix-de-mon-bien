<section class="blocSpec">
    <div class="container">
        <div class="slideBien">
            <?php if( is_array( $imgs ) ): 
                foreach( $imgs as $img ):
            ?>
            <div class="item"><a href="<?= $img['image_large'] ?>" class="galerie_detail"  data-fancybox="groupe1" rel="1"><img src="<?= $img['image_large'] ?>" alt=""></a></div>
            <?php endforeach; endif; ?>
            <?php if( get_field('video_vv', $propID ) ){ ?>
                <div class="item"><div class="content_video"><iframe src="<?= get_field('video_vv', $propID ) ?>"></iframe></div></div>
            <?php } ?>
            <?php if( get_field('video_3d', $propID ) ){ ?>
                <div class="item"><div class="content_video"><iframe src="<?= get_field('video_3d', $propID ) ?>"></iframe></div></div>
            <?php } ?>
        </div>
        <div class="spec">
            <div class="content">
                <div class="titre_spec">
                    <h2><?= get_field('ttire_detail_bein','option') ?></h2>
                    
                </div>
                <div>
                    <?php
                        $jardin = get_field('jardin', $propID) ? 'Oui':'Non';
                        $terasse = get_field('terasse', $propID) ? 'Oui':'Non';
                        $parking = get_field('parking', $propID) ? 'Oui':'Non';
                        $buanderie = get_field('buanderie', $propID) ? 'Oui':'Non';
                        $cave = get_field('avec_cave', $propID) ? 'Oui':'Non';
                    ?>
                    <h3><?= get_field('det_ext','option') ?></h3>
                    <ul>
                        <?php if( get_field('field_60e4461daa23b', $propID) ): ?>
                        <li><?= get_field('etat','option') ?> : <?=  get_field('field_60e4461daa23b', $propID) ?></li>
                        <?php endif; 
                        if( get_field('field_60e4487ad4ded', $propID) ):?>
                        <li>Façade : <?= get_field('field_60e4487ad4ded', $propID) ?></li>
                        <?php endif; 
                        if( get_field('field_60e44887d4dee', $propID) ):?>
                        <li><?= get_field('sur_hab','option') ?> : <?= get_field('field_60e44887d4dee', $propID) ?> m²</li>
                        <?php endif;
                        if( get_field('field_60e44899d4def', $propID) ): ?>
                            <li><?= get_field('sur_terrain','option') ?> : <?= get_field('field_60e44899d4def', $propID) ?> m²</li>
                        <?php endif;
                            
                        ?>
                        <li><?= get_field('jardin','option') ?> : <?= $jardin ?></li>
                        <li><?= get_field('terrasse','option') ?> : <?= $terasse ?></li>
                        <li><?= get_field('parking','option') ?> : <?= $parking ?></li>
                    </ul>
                </div>
                <div>
                    <h3><?= get_field('det_int','option') ?></h3>
                    <ul>
                        <li><?= get_field('nbr_de_chbr','option') ?> : <?= get_field('nombre_de_chambres', $propID) ?></li>
                        <li><?= get_field('nbr_de_sdb','option') ?> : <?= get_field('nombre_de_sdb', $propID) ?></li>
                        <li><?= get_field('cuis','option') ?> : <?= get_field('cuisine', $propID) ?></li>
                        <li><?= get_field('buand','option') ?> : <?= $buanderie ?></li>
                        <li><?= get_field('cavv','option') ?> : <?= $cave ?></li>
                        <li><?= get_field('chaufff','option') ?> : <?= get_field('chauffage', $propID) ?></li>
                        <li><?= get_field('chasss','option') ?> : <?= get_field('chassis', $propID) ?></li>
                    </ul>
                </div>
                <div>
                    <h3><?= get_field('caract_ener','option') ?></h3>
                    <ul>
                        <?php 
                            $peb_img = 'peb-a.png';
                            if( get_field('niveau_peb', $propID) ) $peb_img = 'peb-'.get_field('niveau_peb', $propID).'.png';
                        ?>
                        <li><img src="<?= IMG_DIR . $peb_img ?>" alt="PEB" class="peb"></li>
                        <li><?= get_field('e_spec_','option') ?> : <?= get_field('energie_specifique', $propID) ?> kWh/m².an</li>
                        <li><?= get_field('num_peb','option') ?> : <?= get_field('certficat_peb', $propID) ?></li>
                        <li><?= get_field('niv_peb','option') ?> : <?= get_field('niveau_peb', $propID) ?></li>
                        <li><?= get_field('peb_ttoal','option') ?> : <?= get_field('peb_total', $propID) ?> Kwh/an</li>
                        <li><?= get_field('co2','option') ?> : <?= get_field('emission_co2', $propID) ?> kg/m2/an</li>
                    </ul>
                </div>
                <div class="blocBtnDetail">
                    <a href="<?= get_field('field_60e5ff26eb53e', $propID) ?>" class="btn" target="_blank"><?= get_field('bouton_ag_copie','option') ?></a>
                </div>
            </div>
        </div>
    </div>
</section>