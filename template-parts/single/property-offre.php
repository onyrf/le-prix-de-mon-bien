<?php
    $fiche_cloturee = get_post_meta( $propID, 'fiche_cloturee', true );

    $id_offre_validee = get_field('offre_validee', $propID);

    $now = new DateTime();

    $date_creation = get_field('field_creation', $propID ) ? get_field('field_creation', $propID ) : "01/01/2021 13:42";

    $date_creation_1 = new DateTime( $date_creation );
    $date_creation_1 = $date_creation_1->format('Y-m-d H:i:s');
    $date_creation_1 = new DateTime( $date_creation_1 );

    $date_creation_3 = new DateTime( $date_creation );
    $date_creation_3 = $date_creation_3->format('Y-m-d H:i:s');
    $date_creation_3 = new DateTime( $date_creation_3 );

    $date_creation_0         = new DateTime( $date_creation );
    $date_creation_0 = $date_creation_0->format('Y-m-d H:i:s');
    $date_creation_0 = new DateTime( $date_creation_0 );

    $_10j_apres_creation     = $date_creation_1->add( new DateInterval('P10D') );
    $_12j_apres_creation     = $date_creation_3->add( new DateInterval('P12D') );

    if( 'property' == get_post_type( get_the_ID() ) ) {
?>
        <div class="textPlan titre_bloc_offre">
<?php
        if( $now->diff( $_10j_apres_creation )->invert == 0 ){

            $titre_bloc_offre = "Offres indisponibles";

        }else{
            if( $now->diff( $_12j_apres_creation )->invert == 0 ){

                $titre_bloc_offre = "fin des offres dans : ";
                
                $fin_offre = $now->diff( $_12j_apres_creation );
                $fin_offre = ( $fin_offre->d * 24 * 60 ) + ( $fin_offre->h * 60 ) + ( $fin_offre->i ) + ( $fin_offre->s / 60);
?>
                <ul class="countdown countdown_offre">
                    <li>
                        <div class="temps" data-minutes-left="<?= $fin_offre ?>"></div>
                    </li>
                </ul> 
<?php
            }else{
                $titre_bloc_offre = "Les offres sont terminées.";
            }
        }
?>

            <div class="titre_left"><?php echo $titre_bloc_offre; ?></div>
        </div>
<?php 
    }elseif( 'proprietaire' == get_post_type( get_the_ID() ) ) { 
?>
        <div class="textPlan">
            <?php echo get_field('titre_offre', 'option' ) ?>
        </div>
<?php } ?>
<div class="listOffre <?= $fiche_cloturee ? 'fiche_cloturee' : 'fiche_non_cloturee'; ?>">
    <?php
        $offres = loop_offres( $propID );

        if( is_array( $offres ) ):
            foreach( $offres as $offre ):
                $offre_validee = false;
                $miseo = false;

                if( 'proprietaire' == get_post_type( get_the_ID() ) ) {
                    if( $now->diff( $_12j_apres_creation )->invert > 0 ) {
                        $miseo = true;
                    }

                }
        ?>
            <div class="item <?php echo $miseo ? 'miseho_valider' : '' ?>">
                <div class="date">
                    <?= get_field('date_de_loffre', $offre->ID ) ?> 
                </div>
                <div class="agence">
                    <?= get_field('agence', $offre->ID ) ?>
                </div>
                <div class="prix">
                     <b><?= millier( get_field('prix_de_loffre', $offre->ID ) ) ?>€</b>
                </div>
                
                <?php 
                    if( 'proprietaire' == get_post_type( get_the_ID() ) ) {
                        $class_color = ' couleur_orange';
                    }else{
                        $class_color = ' couleur_doree';
                    }
                ?>

                <div class="details_offre<?= $class_color ?>">
                    <ul>
                        <!-- <li>Agent : <?= get_field('agence', $offre->ID ) ?></li> -->
                        <li>Email Agent : <?= get_post_meta( $offre->ID,'email_agent', true ); ?></li>
                        <li>Date : <?= get_field('date_de_loffre', $offre->ID ) ?></li>
                        <!-- <li>Prix : <?= millier( get_field('prix_de_loffre', $offre->ID ) ) ?></li> -->
                        <li>Condition suspensive : <?= get_field('condition_suspensive', $offre->ID ) ?></li>
                        <li>Paiement accompte : <?= get_field('paiement_accompte', $offre->ID ) ?></li>
                        <?php if( "oui" == get_field('paiement_accompte', $offre->ID ) ){ ?>
                            <li>Taux accompte : <?= get_field('taux_accompte', $offre->ID ) ?></li>
                        <?php } ?>
                    </ul>
                    <div class="btn_doc">
                       <a href="<?= get_field('carte_didentite', $offre->ID ) ?>" target="_blank" class="carte_didentite">
                           Document d'identité
                       </a>
                    </div>
                    <div class="btn_doc">
                       <a href="<?= get_field('doc_offre', $offre->ID ) ?>" target="_blank" class="doc_offre">
                           Document d'offre
                       </a>
                    </div>
                </div>
                <?php
                        if( $miseo ){

                            if( !$fiche_cloturee || '' == $fiche_cloturee ){
                                $tx = get_field('btn_valider_offre', 'option' ) ? get_field('btn_valider_offre', 'option' ) : 'Valider';
                ?>
                                <div class="vadid_offre">
                                    <a href="#" data-offre_id="<?= $offre->ID ?>" data-propID="<?= $propID ?>">
                                        <?php echo $tx ?>
                                    </a>
                                </div>
                <?php
                            }else{
                                $tx = get_field('btn_valider_offre_fini', 'option' ) ? get_field('btn_valider_offre_fini', 'option' ) : 'Validé';
                                if( $offre->ID == $id_offre_validee ){
                ?>
                                    <div class="vadid_offre">
                                        <span><?php echo $tx ?></span>
                                    </div>
                <?php
                                }
                            }
                ?>
                                
                <?php
                        }
                ?>
            </div>
        <?php endforeach; 
        else: ?>
            <div id="sans_offre" class="item">
                <?= get_field('no_offre','option') ?>
            </div>
        <?php endif; ?>
</div>

<?php 
    if( 'property' == get_post_type( get_the_ID() ) ) {
        if( !$hide_button_offre ){
            require_once 'property-offre-form.php'; 
        }
    }
?>