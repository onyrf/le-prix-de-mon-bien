<?php
    if( !$hide_button_rdv ){
        // ato zany ilay formulaire normale: miseo eo ny liste rdv + afaka manao rdv hafa
        echo do_shortcode('[online_reservation ref='. get_field('field_60e443276c38d', $propID) .' prop_id='.$propID.']');
    }else{
        // ato ndray listena daholo fotsn ny rdv, fa tsy afaka miplanifié intsony
        echo do_shortcode('[online_reservation ref='. get_field('field_60e443276c38d', $propID) .' prop_id='.$propID.' view_only='.$hide_button_rdv.']');
    }

