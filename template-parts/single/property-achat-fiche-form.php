<?php
  $is_buy_now = "no";
  if ( isset($_GET['_sc']) && wp_verify_nonce($_GET['_sc'], 'nonce_buy_now')) {
    $is_buy_now = "yes";
  }

  if( in_array( $fiche_id, $boughts ) && current_user_is_subscribed() ){ ?>
    <h2 class="titre">Acheter l'accès exclusif</h2>
<?php
}else{
?>
    <h2 class="titre"><?= get_field('titre_achat_fiche','option') ?></h2>
<?php
} ?>
  <div class="content content_moyen_paiement">
      <form id="submit-buy">
          <div class="champ">
              <input type="text" class="form-control" placeholder="<?= get_field('placeholder_agent','option') ?>" value="<?= $current_user->data->user_login ?>" name="agent_name">
          </div>
          <div class="moyen_paiement">
              <div><?= get_field('selection_moyen_de_paiement','option') ?></div>
              <!-- <form id="submit-buy"> -->
                  <div class="listPaiement">
                      <label for="visa">
                          <input id="visa" type="radio" class="paiement" value="stripe" name="paiement">
                          <img src="<?= IMG_DIR ?>visa.png" alt="vendez mon bien">
                      </label>
                      <label for="ban_contact">
                          <input id="ban_contact" type="radio" class="paiement" value="stripe_bancontact" name="paiement">
                          <img src="<?= IMG_DIR ?>bancontact.png" alt="vendez mon bien">
                      </label>
                      <label for="paypal_">
                          <input id="paypal_" type="radio" class="paiement" value="ppcp-gateway" name="paiement">
                          <img src="<?= IMG_DIR ?>paypal.jpg" alt="vendez mon bien">
                      </label>
                      <label for="tsy_fantatra">
                          <input id="tsy_fantatra" type="radio" class="paiement" value="payconiq" name="paiement">
                          <img src="<?= IMG_DIR ?>Image 4.png" alt="vendez mon bien">
                      </label>
                  </div>
                  <input type="hidden" name="propID" value="<?= $propID ?>">
                  <input type="hidden" name="fiche_id" value="<?= $fiche_id ?>">
                  <input type="hidden" name="buy_now" value="<?= $is_buy_now ?>">
                  <button type="submit" class="btn">
                    <?php 
                      if( $product ){
                        $prix = $product->get_price();
                      }else{
                        $product = wc_get_product( $fiche_id );
                        $prix = $product->get_price();
                      }
                       
                      if( "no" == $is_buy_now ){
                        echo str_replace('{prix_fiche}', $prix , get_field('bouton_achat','option') );
                      }else{
                        $prix = 5 * $prix;
                        echo "Acheter l'accès exclusif à ce bien pour $prix €";
                      }
                    ?>
                  </button>
              <!-- </form> -->
          </div>
      </form>
  </div>
  