<!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
    <?php //astra_head_top(); ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <link rel="icon" href="<?= IMG_DIR . 'fav-part.png'; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >