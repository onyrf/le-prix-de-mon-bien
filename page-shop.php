<?php
	// template name: Page Shop

get_header('shop');
?>

<div class="content_shop">
	<div class="container">
		<div class="row">
			
			<div class="col-book">
				<img src="<?= IMG_DIR.'cover2.png'; ?>" alt="">
				<div class="prix">97€</div>
				<h1 class="titre">10 règles d'or pour vendre votre maison comme un PRO</h1>
				<a href="#" id="buy_book" class="btn">Acheter le book</a>
			</div>

			<div class="col-book">
				<img src="<?= IMG_DIR.'cover1.png'; ?>" alt="">
				<div class="prix">249€</div>
				<h1 class="titre">Rapport d'expertise</h1>
				<a href="#" id="buy_estimation" class="btn">Acheter l'estimation</a>
			</div>	

			<div class="col-book">
				<img src="<?= IMG_DIR.'cover1.png'; ?>" alt="">
				<div class="prix">49€</div>
				<h1 class="titre">Rapport d'expertise <span>+ en bonus: le guide des 10 astuces du vendeur</span></h1>

				<a href="#" id="buy_estimation_reduit" class="btn">Prix réduit</a>
			</div>

		</div>
	</div>
	
</div>

<?php get_footer(); ?>