<?php

/**

 * Template Name: Merci wicoach

 */

$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_components = parse_url($url);
parse_str($url_components['query'], $params);
extract($params);


add_filter( 'wp_mail_from', 'sender_email_wicoach' );
add_filter( 'wp_mail_from_name', 'sender_name_wicoach' );
$sender = get_field('sender','option'); 
  // logique quand origine = newsletter
$headers = array('Cc: '.$sender, "Reply-To: " .$sender, 'Content-Type: text/html; charset=UTF-8');

ob_start();

include 'inc/template_email/email_apres_coach.php';

$body_mail = ob_get_clean();

if(@wp_mail( $email, 'Merci', $body_mail, $headers )){

  $ret['status'] = 'OK';

  $ret['result'] = 1;

}else {

  $ret['status'] = 'KO';

  $ret['result'] = 0;

  $ret['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">une erreur est survenue</p>';

}	

get_header('wicoach-merci'); ?>


<?php get_footer('coach'); ?>
