<!DOCTYPE html>
<?php astra_html_before();?>
<html <?php language_attributes(); ?>>
<head>
    <?php //astra_head_top(); ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJwwWxqvXjx_NN1dq-DgGNhleyuZrSSyI&libraries=places&v=weekly"></script>
    <script src="<?= ASTRA_THEME_CHILD_URI.'assets/js/ac_address.js'; ?>"></script>
    <script>
      var email = '<?php echo isset( $_SESSION['email'] ) ? $_SESSION['email'] : "" ; ?>';
      var telephone = '<?php echo isset( $_SESSION['telephone'] ) ? $_SESSION['telephone'] : "" ; ?>';
      var fname = '<?php echo isset( $_SESSION['fname'] ) ? $_SESSION['fname'] : "" ; ?>';
      var lname = '<?php echo isset( $_SESSION['lname'] ) ? $_SESSION['lname'] : "" ; ?>';
    </script>
    <?php
      if( ! is_wc_endpoint_url( 'order-received' ) ){
        foreach( WC()->cart->get_cart() as $cart_item ){
          $product_id = $cart_item['product_id'];
        }
      }elseif( is_wc_endpoint_url( 'order-received' ) ){
        $order_id  = absint( $wp->query_vars['order-received'] );
        $order = wc_get_order( $order_id );
        $items = $order->get_items();
        foreach( $items as $item ){
          $product_id = $item->get_product_id();
        }
      }

      if( $product_id == 176 ){
        echo '<link rel="icon" href="'. IMG_DIR .'fav.png">';
        get_template_part( 'template-parts/header/checkout-reduit' );
      ?>
        <style type="text/css">
            #thmaf_billing_alt_field{ display: none !important; }
        </style>
      <?php
      }elseif( $product_id == 360 ){
        echo '<link rel="icon" href="'. IMG_DIR .'fav.png">';
        get_template_part( 'template-parts/header/checkout-plein' );
      ?>
        <style type="text/css">
            #thmaf_billing_alt_field{ display: none !important; }
        </style>
      <?php
      }elseif( $product_id == 364 || $product_id == 374 ){ 
        echo '<link rel="icon" href="'. IMG_DIR .'fav.png">';
        get_template_part( 'template-parts/header/checkout-book' );
        ?>
        <style type="text/css">
            #thmaf_billing_alt_field{ display: none !important; }
        </style>
      <?php
      }elseif( $product_id == ID_ABO || $product_id == ID_ABO_AN ){
      echo '<link rel="icon" href="'. IMG_DIR .'fav-part.png">'; ?>
        <style type="text/css">
          #thmaf_billing_alt_field{ display: none !important }
          footer{ display: none !important }
        </style>
      <?php
        if( is_wc_endpoint_url( 'order-received' ) ){

          $PrivateEmail = $_SESSION['email'];
          $Name = $_SESSION['lname'];
          $FirstName = $_SESSION['fname'];
          $OfficeIds = OFFICE_ID;

          create_contact_whise( 1, 'fr-BE', $OfficeIds, $PrivateEmail, $Name, $FirstName );
         ?>
             <script>
              window.top.location.href = "<?= site_url('mon-compte').'?u=nouveau' ?>"; 
            </script>
         <?php
        }

      }else{
        echo '<link rel="icon" href="'. IMG_DIR .'fav-part.png">';
        ?>
        <script>
          var est_fiche = true;
        </script>
        <style type="text/css">
          footer{ display: none !important }
        </style>
        <?php

        if( isset( $_GET['bn'] ) && !empty( $_GET['bn'] ) ) {
          if( "yes" == strip_tags( $_GET['bn'] ) ){
            $_SESSION['buy_now'] = true;
          }
        }

        if( is_wc_endpoint_url( 'order-received' ) ){ 
          $fiche = get_current_product_fiche( $product_id );
        ?>
             <script>
              window.top.location.href = "<?= get_permalink( $fiche ) ?>"; 
            </script>
          <?php
        }
        
      }

       wp_head(); ?>
</head>

<body <?php if( isset( $_GET['ck'] ) && !empty( $_GET['ck'] ) && $_GET['ck'] == 'iframe' ) {
          body_class( 'ck_' );
    }else{ 
        body_class();
    }
     ?> 
    
  >
