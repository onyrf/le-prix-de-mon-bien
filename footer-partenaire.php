<footer>
    <div class="footer1">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <a href="<?= site_url('partenaires'); ?>" class="logoFoot wow fadeInLeft"><img src="<?= IMG_DIR ?>logo-partenaire.svg" alt=""></a>
                <div class="adr">
                    <p>Boulevard du Souverain 24<br> 1170 Bruxelles</p>
                    <!-- <p>Tél. <a href="tel:080012249 ">0800 12 249 </a></p> -->
                </div>
                <ul class="d-flex justify-content-between align-items-center wow fadeInRight">
                    <?php $i = 1; ?>
                    <?php if(have_rows('liens','option')):while(have_rows('liens','option')):the_row(); ?>
                        <li>
                        <?php if($i==1): ?>
                            <?php the_field('texte_copyright','option'); ?>
                        <?php endif; ?>
                        <?php $l = get_sub_field('lien');
                            if( is_array($l) && array_key_exists('url', $l) && array_key_exists('title', $l) ):
                        ?>
                        <a href="<?= $l['url'] ?>" title="<?= $l['title'] ?>"><?= $l['title'] ?></a>
                        </li>
                        <?php endif; $i++; ?>
                    <?php endwhile; endif; ?>
                    <li>
                        <a data-fancybox="gallery" href="#signaler-abus" id="popup-abus-trig" title="Signaler un abus">Signaler un abus</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="signaler-abus" style="display: none;">
        <?php get_template_part('inc/form-abus') ?>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
