<?php

 while( have_posts() ):
    the_post();

    $propID = (int)get_post_meta( get_the_ID(), 'id_de_son_bien', true );

    $fin = get_field('field_60ec3f4463ef4', $propID ); 
      $fin = get_minutes_interval( $fin );

      get_header();

      $imgs = get_field('images', $propID);

      $fiche_id = get_current_product_id() ? get_current_product_id() : 0;

      $sales = (int)get_post_meta( $fiche_id, 'total_sales', true );

      $product = wc_get_product( $fiche_id );
      
      if( is_array( $imgs ) ) {
          $l = random_int( 0, count( $imgs) );
          $bg_url = $imgs[0]['image_large'];
          $bg_descr = $imgs[$l]['image_large'];
      }else{
          $bg_url = IMG_DIR .'banner-detail.jpg';
          $bg_descr = IMG_DIR .'img-descri.jpg';
      }

      require_once 'template-parts/single/property-banner.php';
?>
         <?php require_once 'template-parts/single/property-description.php' ?>

        

        <section class="blocBandeau">
            <div class="container">
                <div class="valeur">Prix de départ : <?= get_field('field_60e442f86c38b',$propID) ?>€</div>
                <!-- <div class="blocBtnDetail">
                    <a href="tel:080012249" class="btn btn-tel"><b>Appelez-nous</b>0800 12 249</a>
                    <a href="#plan_rdv" class="btn scroll">visiter ce bien</a>
                    <a href="#faire_offre" class="btn scroll">faire une offre</a>
                </div> -->
            </div>
        </section>

        <?php require_once 'template-parts/single/property-spec.php'; ?>

        <section class="plan_visite">
            <div class="container">
                <div class="col_left">
                    <div class="content" id="plan_rdv">
                        <?php
                            //require_once 'template-parts/single/property-visite.php'
                            echo do_shortcode('[online_reservation ref='.get_field('field_60e443276c38d', $propID).' list=1]');
                        ?>
                    </div>
                </div>
                <div class="col_right" id="faire_offre">
                    <div class="content">
                        <?php require_once 'template-parts/single/property-offre.php' ?>
                    </div>
                </div>
            </div>
        </section>

        <!-- POPUP VALIDE -->
        <div class="popup_valide" id="popup_valide" style="display:none">
            <div class="content">
                <span id="titre_popup"></span>
                <div id="content_popup"></div>
            </div>
        </div>

        <div class="home blockUI blockOverlay"></div>
   <?php get_footer();
  endwhile;
?>
