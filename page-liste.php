<?php

/**

 * Template Name: Liste Biens

 */
get_header('partenaires'); 

get_template_part( 'template-parts/header/header', 'partenaire' );

?>

<div class="blcFiltre">
  <div class="container">
    <form class="blcSearch">
      <div class="blcChp">
        <div class="chp">
            <label>Vous cherchez ?</label>
            <div class="select">
              <select name="type">
                <option value="">Tout type</option>
                <option value="1">Maison</option>
                <option value="2"> Appartement</option>
                <option value="3">Terrain</option>
                <option value="4">Bureau</option>
                <option value="5">Commercial</option>
              </select>
            </div>
        </div>
        <div class="chp">
          <label>Dans quelle ville ?</label>
          <div class="select">
             <select name="ville">
                <option value="">Toutes</option>
                <optgroup label="Belgique">
                   <option value="Anderlues">Anderlues</option>
                   <option value="Audregnies">Audregnies</option>
                   <option value="Baudour">Baudour</option>
                   <option value="Bernissart harchies">Bernissart harchies</option>
                   <option value="Binche">Binche</option>
                   <option value="Binche bray">Binche bray</option>
                   <option value="Binche waudrez">Binche waudrez</option>
                   <option value="Blaton">Blaton</option>
                   <option value="Boussu">Boussu</option>
                   <option value="Boussu hornu">Boussu hornu</option>
                   <option value="Boussu-bois">Boussu-bois</option>
                   <option value="Colfontaine">Colfontaine</option>
                   <option value="Dour">Dour</option>
                   <option value="Dour elouges">Dour elouges</option>
                   <option value="Dour wiheries">Dour wiheries</option>
                   <option value="Elouges">Elouges</option>
                   <option value="Erquennes">Erquennes</option>
                   <option value="Frameries">Frameries</option>
                   <option value="Haine saint pierre">Haine saint pierre</option>
                   <option value="Hainin">Hainin</option>
                   <option value="Hautrage">Hautrage</option>
                   <option value="Hensies hainin">Hensies hainin</option>
                   <option value="Honnelles">Honnelles</option>
                   <option value="Hornu">Hornu</option>
                   <option value="Houdeng-aimeries">Houdeng-aimeries</option>
                   <option value="Jemappes">Jemappes</option>
                   <option value="La louviere">La louviere</option>
                   <option value="Lessines">Lessines</option>
                   <option value="Mons">Mons</option>
                   <option value="Montroeul-sur-haine">Montroeul-sur-haine</option>
                   <option value="Paturages">Paturages</option>
                   <option value="Petit dour">Petit dour</option>
                   <option value="Quaregnon">Quaregnon</option>
                   <option value="Quevy">Quevy</option>
                   <option value="Quievrain">Quievrain</option>
                   <option value="Saint-ghislain">Saint-ghislain</option>
                   <option value="Sirault">Sirault</option>
                   <option value="Thulin">Thulin</option>
                   <option value="Uccle">Uccle</option>
                   <option value="Wasmes">Wasmes</option>
                   <option value="Wasmuel">Wasmuel</option>
                </optgroup>
                <optgroup label="France">
                   <option value="Crespin">Crespin</option>
                   <option value="Hon hergies">Hon hergies</option>
                </optgroup>
             </select>
          </div>
        </div>
        <div class="chp">
            <label>Quel budget ?</label>
            <div class="select">
              <select name="budget">
                 <option value="">Tous</option>
                 <option value="0,100000">
                    0€ - 100.000€                   
                 </option>
                 <option value="100000,200000">
                    100.000€ - 200.000€                   
                 </option>
                 <option value="200000,300000">
                    200.000€ - 300.000€                   
                 </option>
              </select>
            </div>
        </div>
        <div class="chp btn_search">
            <span class="sipan"><button type="submit" class="submit btn btn_partenaire" id="btn_filter">Chercher</button></span>
        </div>
      </div>
    </form>
  </div>
  
</div>
<div class="triage">
  <div class="container">
    <div class="list_bouton">
        <div class="order">
          <div class="chp">
              <label class="label">Trier par</label>
              <div class="select">
                <select class="order_price" name="orderPrice">
                  <option value="1">Prix croissant</option>
                  <option value="0">Prix décroissant</option>
                </select>
              </div>
          </div>
        </div>
        <div class="etat_biens">
             <div class="label">Voir les biens</div>
             <ul class="list">
               <li><span class="btn btn_partenaire">Disponibles</span></li>
               <li><span class="btn btn_partenaire">Nouveaux </span></li>
               <li><span class="btn btn_partenaire">Sous options</span></li>
             </ul>
        </div>
    </div>
  </div>
</div>

<div class="listBiens" id="listBiens">
      <?php

        $now = new DateTime();

         $par_page = -1;

         $arg = array(
            'post_type'      => 'property',
            'posts_per_page' => $par_page,
            'paged'          => $paged,
            'meta_key'       => 'statut_transaction',
            'orderby'        => array(
               'meta_value' => 'ASC',
               'ID'         => 'DESC',
               
            ),
         );

         if( $statut_transaction ){
            $new_arg = array( 'meta_value' => $statut_transaction );

            $arg = $arg + $new_arg;
         }

         $q = new WP_Query( $arg );

         if( $q->have_posts() ){
            while( $q->have_posts() ){
               $q->the_post();

               $propID = get_the_ID();
               $fiche_cloturee = get_post_meta( get_the_ID(), 'fiche_cloturee', true );

               $statut = get_field('statut_transaction');
               if( $fiche_cloturee && 'Vendu' != $statut ){
                  update_field('statut_transaction','Sous option', $propID);
                  $statut = get_field('statut_transaction');
               }

               if( 'Vendu' == $statut ){
                  $color = '#a29f78 !important';
               }elseif( 'Sous option' == $statut ){
                  $color = '#9b9b9b !important';
               }else{
                  $color = '#00E29D !important';
               }


        $date_creation = get_field('field_creation' ) ? get_field('field_creation' ) : "01/01/2021 13:42";

        $date_creation_1 = new DateTime( $date_creation );
        $date_creation_1 = $date_creation_1->format('Y-m-d H:i:s');
        $date_creation_1 = new DateTime( $date_creation_1 );

        $date_creation_2 = new DateTime( $date_creation );
        $date_creation_2 = $date_creation_2->format('Y-m-d H:i:s');
        $date_creation_2 = new DateTime( $date_creation_2 );

        $_24h_apres_ajout = $date_creation_1->add( new DateInterval('P0DT24H') );


        $fin = $date_creation_2->add( new DateInterval('P2D') );
        $diff = $now->diff($fin);

         $etat_bien_class = "etat_biens d-flex justify-content-center";
         $item_misy_chrono = "item_sans_chrono";
         if( $diff->invert == 0 ){
            $etat_bien_class = 'etat_biens misy_chrono';
            $item_misy_chrono = "item_avec_chrono";
         }
      ?>

      <div class="item <?= 'Vendu' == $statut || 'Sous option' == $statut ? 'no_btn_vente':''; ?> <?= 'Vendu' == $statut ? 'container_vendu':''; ?> <?= $item_misy_chrono ?>">
          <div class="content" <?= 'Vendu' == $statut ? 'style="opacity:0.5"':''; ?>>
              <div class="top_biens">
                  <div class="blcImg">
                      
                          <?php
                             $existing_imgs = get_field('field_60e44529224ad', $propID);
                             if( is_array($existing_imgs) && count( $existing_imgs ) ){
                               $pic = $existing_imgs[0];
                             if( array_key_exists('image_large', $pic)):
                          ?>
                          <div class="img">
                             <img class="image" src="<?= $pic['image_large'] ?>">
                          </div>
                          <?php endif; }else{ ?>
                           <div class="img img_logo">
                             <img class="image logo_vdmb" src="<?= IMG_DIR . 'logo_vendez_partenaires.svg' ?>">
                          </div>
                          <?php } ?>
         
                      
                      <div class="info_top">
                           <?php 
                                $peb_img = 'peb-A.png';
                                if( get_field('niveau_peb', $propID) ) $peb_img = 'peb-'.get_field('niveau_peb', $propID).'.png';
                            ?>
                          <span class="peb"><img  class="img_peb" src="<?= IMG_DIR. $peb_img ?>"></span>
                          <div class="blcSous_option">
                              <span class="tag" style="background:<?= $color ?>">
                                 <?php echo $statut; ?>
                              </span>
                              <?php

                                
                                 
                                 if( $statut != 'Vendu' && $statut != 'Sous option' ){
                                    if( $now->diff( $_24h_apres_ajout )->invert == 0 ){

                                      $was_bought_now = get_post_meta( $propID, 'was_bought_now', true );

                                      if( !$was_bought_now || ''==$was_bought_now ){

                                        $url = wp_nonce_url( get_permalink($propID), "nonce_buy_now", '_sc' );
                              ?>
                                        <a href="<?= $url ?>" class="buy_now" data-prop="<?= $propID ?>">buy now</a>
                           <?php      }
                                    }
                                 } ?>
                          </div>
                      </div>
                      <?php if( 'Vendu' == $statut ){ ?>
                        <div class="bien_vendu">
                           <div class="titre_vendu">Bien vendu par</div> 
                           <div><img class="image_oblique" src="<?= IMG_DIR . 'logo_vendez_partenaires.svg' ?>"></div>
                        </div>
                     <?php } ?>
                  </div>
                  
                  <div class="<?= $etat_bien_class ?>">
                      <div class="left">
                           <?php
                              $fiche_id = get_current_product_id() ? get_current_product_id() : 0;
                              $sales = (int)get_post_meta( $fiche_id, 'total_sales', true );
                              $achat_max = 5;
                              $total_sales = $achat_max - $sales;

                               if( $total_sales < 0 ) {
                                  $total_sales = 0;
                               }
                            ?>
                          Il reste :<br> 
                          <?php echo $total_sales .'/5' ?> achat(s)
                      </div>
                      <?php if( $diff->invert == 0 ){ 
                        $minutes = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i + ( $diff->s / 60);
                      ?>
                         <div class="right">
                             Terminé dans : <br> <span class="temps" data-minutes-left="<?= $minutes ?>"></span>
                         </div>
                     <?php } ?>
                  </div>
              </div>
              <div class="bottom_biens misy_buy_now">
                  <div class="blcPrix">
                      <div class="location"><?= get_field('categorie_batiment') ?> à <?= get_field('ville') ?></div>
                      <div class="prix">
                          Faire offre à partir de
                          <span><?= millier( get_field('prix_de_vente') ); ?>€</span>
                      </div>
                  </div>
                  <div class="blc_caract_biens">
                      <div class="blc">
                          <?php if (get_field('field_60e44a6bf62db', $propID)) { ?>
                              <span class="picto"><img  class="img_peb" src="<?= IMG_DIR.'icon-bed.png' ?>"></span>
                          <?php } ?>
                          <span><?= get_field('field_60e44a6bf62db', $propID) ?></span>
                      </div>
                      <div class="blc">
                          <span class="picto"><img  class="img_peb" src="<?= IMG_DIR.'icon-distance.png' ?>"></span>
                          <span>
                              <?php if( get_field('field_60e44899d4def', $propID) )
                                  echo get_field('field_60e44899d4def', $propID);
                              ?> <sup>m2</sup>
                          </span>
                      </div>
                  </div>
                  <?php 
                     $fiche_id = get_current_product_id() ? get_current_product_id() : 0;
                     $sales = (int)get_post_meta( $fiche_id , 'total_sales', true );
                     if( !$fiche_cloturee && 'Vendu' != $statut ): ?>
                      <div class="hide">
                        <?php
                          $texte_btn = $sales < 5 ? "vendre ce bien" : "accès complet";
                        ?>
                            <!-- miseho rehefa buy now -->
                          <a href="#" class="btn buy_now">buy now</a>

                          <a href="<?= get_permalink( get_the_ID() ) ?>" class="btn"><?= $texte_btn ?></a>

                      </div>
                  <?php endif; ?>
              </div>
          </div>
      </div>
      <?php 
            }

            wp_reset_postdata();
         }
      ?>

  </div>

<? get_footer(); 
