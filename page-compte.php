<?php 
	/*
	* Template name: Mon compte
	*/


    // Envoi emails bienvenues
    if( isset($_GET['u']) && !empty($_GET['u']) && is_user_logged_in() ){
        if( 'nouveau' == wp_strip_all_tags( $_GET['u'] ) ){

            $user = wp_get_current_user(); 
            $u = get_user_meta( $user->ID );

            $nom = $u['last_name'][0];
            $prenom = $u['first_name'][0];
            $email = $user->data->user_email;
            
            add_filter( 'wp_mail_from', 'sender_email_lpdmb' );
            add_filter( 'wp_mail_from_name', 'sender_name_lpdmb' );
            $destinataire = get_field('sender','option');

            $headers = array('Reply-To: '. $destinataire,"Cc: $destinataire",'Content-Type: text/html; charset=UTF-8');

            // Email de bienvenue Partenaire
            ob_start();
                include ASTRA_THEME_CHILD_DIR .'inc/template_email/bienvenue_partenaire.php';
            $body_mail = ob_get_clean();

            if( @wp_mail( $email, 'Nouveau Partenaire', $body_mail, $headers ) ){
              // Email de bienvenue à la plateforme
              ob_start();
                  include ASTRA_THEME_CHILD_DIR .'inc/template_email/bienvenue_plateforme.php';
              $body_mail = ob_get_clean();

              $send = wp_mail( $email, 'Nouveau sur le plateforme', $body_mail, $headers );
            }

            

          }
    }

    if( isset( $_SESSION['REFERER_BIEN'] ) && !empty( $_SESSION['REFERER_BIEN'] ) ){
        wp_safe_redirect( $_SESSION['REFERER_BIEN'] );
        exit;
    }

	get_header('partenaires');
?>

<div class="barre_top">
    <div class="blcTop fixed">
        <div class="container headParteneire">
            <div class="row">
                <div class="col top-partenaire">
                    <a href="<?= site_url('partenaires'); ?>" class="logo logo_partenaire wow fadeInUp">
                        <img src="<?= IMG_DIR ?>logo-partenaire.svg" alt="">
                    </a>
                    <div class="blocLogin">
                        <?php if( is_user_logged_in() ): 
                            $current_user = wp_get_current_user();
                        ?>
                            <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="btn-connect lrm-show-if-logged-in"><?= $current_user->first_name.' '.$current_user->last_name ?></a>
                            <div class="btn_menu_compte"><div></div></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var $ = jQuery.noConflict();
    $(".btn_menu_compte").click(function () {
        $(this).toggleClass('active');
        $('.woocommerce-MyAccount-navigation').toggleClass('active');

    })
</script>


<?php the_content() ?>

<?php get_footer('partenaire'); ?>