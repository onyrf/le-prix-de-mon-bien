<?php

  if( isset( $_SESSION['REFERER_BIEN'] ) && !empty( $_SESSION['REFERER_BIEN'] ) ){
      unset( $_SESSION['REFERER_BIEN'] );
  }

  while( have_posts() ):
    the_post();

    $propID = get_the_ID();
    // var_dump( get_estate_details( 4519369 )->details );die;

    $fin = get_field('field_creation' ) ? get_field('field_creation' ) : "01/01/2021 13:42";
    
    $fin = new DateTime( $fin );
    $fin->format('Y-m-d H:i:s');

    $fin = $fin->add( new DateInterval('P2D') );
  
    $now = new DateTime();

    $diff = $now->diff($fin);

    gt_set_post_view();

    $imgs = get_field('images');

    $boughts = get_bought_products();
    if( !is_array( $boughts ) ) $boughts = array();

    $fiche_id = get_current_product_id() ? get_current_product_id() : 0;

    $sales = (int)get_post_meta( $fiche_id, 'total_sales', true );
    $achat_max = 5;
    $total_sales = $achat_max - $sales;

     if( $total_sales < 0 ) {
        $total_sales = 0;
     }

    $product = wc_get_product( $fiche_id );
    
    if( is_array( $imgs ) ) {
        $l = random_int( 0, count( $imgs) );
        $bg_url = $imgs[0]['image_large'];
        $bg_descr = $imgs[$l]['image_large'];
    }else{
        $bg_url = IMG_DIR .'banner-detail.jpg';
        $bg_descr = IMG_DIR .'img-descri.jpg';
    }

    // get_header();
    require_once 'header.php';
   
    if ( get_field('est_actif') === true  || ( in_array( $fiche_id, $boughts ) && current_user_is_subscribed() ) ):      

      require_once 'template-parts/single/property-banner.php';
      $current_user = wp_get_current_user();
      // var_dump( $sales );
      // if( $sales <= $achat_max ){
        if( in_array( $fiche_id, $boughts ) && current_user_is_subscribed() ){

            require_once 'template-parts/single/property-description.php';

            require_once 'template-parts/single/property-bandeau.php';

            require_once 'template-parts/single/property-spec.php';

            require_once 'template-parts/single/property-doc.php';

            require_once 'template-parts/single/property-visite-offre.php'; 

            if ( isset($_GET['_sc']) && wp_verify_nonce($_GET['_sc'], 'nonce_buy_now')) {
  
              if( $now->diff( $_24H_apres_creation )->invert == 0 ){ 
          ?>
                <div id="popup_pour_buynow" class="blocFiltreNyZo modal_achat_pour_abonnes">
                  <div class="blocNyZo popup_achat_lead popup_agent_biloute" id="popup_achat_lead">
                    <div class="content">
            <?php
                  require_once 'template-parts/single/property-achat-fiche-form.php'; 
            ?>
                    </div>
                  </div>
                </div>
        <?php   
              }        
            }
        }else{
          ?>
        
                <?php            
                if( !is_user_logged_in()) { 
                ?>
                <div class="blocFiltreNyZo">
                  <div style="filter: blur(13px); opacity: 0.7">
                      <?php require_once 'page-detail.php' ?>
                  </div>
                  <div class="blocNyZo popup_achat_lead 
                    <?php if( !is_user_logged_in() ){ 
                      echo 'popup_agent_arson'; }else{ 
                      echo 'popup_agent_biloute'; } ?>" id="popup_achat_lead">
                      <div class="content">
                        <?php echo do_shortcode('[lrm_form default_tab="login" logged_in_message="Vous êtes déjà authentifé"]'); ?>
                        <p class="no_account">Pas encore de compte partenaire, <a href="<?= site_url('partenaires') . '?_rdc=1&p=' . get_permalink( $propID ) ?>">créez votre compte</a>.</p>
                        <input type="hidden" id="registration_link" value="<?= site_url( 'partenaires' ) . '?_rdc=1' ?>">
                      </div>
                    </div>
                  </div>
                <?php 

                }elseif( is_user_logged_in() && !current_user_is_subscribed() ){ 
                ?>
                  <div class="blocFiltreNyZo">
                    <div style="filter: blur(13px); opacity: 0.7">
                        <?php require_once 'page-detail.php' ?>
                    </div>
                    <div class="blocNyZo popup_achat_lead 
                      <?php if( !is_user_logged_in() ){ 
                        echo 'popup_agent_arson'; }else{ 
                        echo 'popup_agent_biloute'; } ?>" id="popup_achat_lead">
                      <div class="content">
                        <p class="no_abo">Vous n'avez pas d'abonnement valide. <br><a href="<?= site_url( 'partenaires' ) . '?_rdc=1' ?>" title="">Souscrivez-vous à nouveau</a>.</p>
                      </div>
                    </div>
                  </div>
                <?php 
                }else{
                    require_once 'template-parts/single/property-description.php';

                    require_once 'template-parts/single/property-bandeau.php';

                    require_once 'template-parts/single/property-spec.php'; 

                    require_once 'template-parts/single/property-doc.php';  

                    if( $total_sales > 0 ){                 
                ?>
                      <div class="blocFiltreNyZo modal_achat_pour_abonnes">
                        <div style="filter: blur(13px); opacity: 0.7">
                            <?php require_once 'page-detail-visite-offre.php'; ?>
                        </div>
                        <div class="blocNyZo popup_achat_lead 
                          <?php if( !is_user_logged_in() ){ 
                            echo 'popup_agent_arson'; }else{ 
                            echo 'popup_agent_biloute'; } ?>" id="popup_achat_lead">
                          <div class="content">
                  <?php
                              require_once 'template-parts/single/property-achat-fiche-form.php'; 
                  ?>
                          </div>
                        </div>
                      </div>
                <?php   
                    }                              
                } //else logged in 
                ?>
            
        <?php }

      // }else{ 
      //   update_field( 'est_actif', false );
      //   require_once 'template-parts/single/property-indisponible.php';
      // }

    
  else:
      require_once 'template-parts/single/property-indisponible.php';
  endif;

  ?>
    <div class="home blockUI blockOverlay"></div>

  <?php get_footer('partenaire'); ?>
    <?php

endwhile;

?>
