<?php
	/* Template name: checkout 
	*/
	get_header('checkout');

	if( !is_wc_endpoint_url( 'order-received' )){
		if( false === authentication_needed() ){
			the_content();
		}else{
			echo do_shortcode('[lrm_form default_tab="login" logged_in_message="Vous êtes déjà authentifé"]');
		}
	}else{
		the_content();
	}
?>

<script type="text/javascript">
    var $ = jQuery.noConflict();
   $('.map-head img').on('load', function() {
        $(this).parents('.header-banner').addClass('afficher');
       
    })
    $(".scroll").click(function() {
      var c = $(this).attr("href");
      $('html, body').animate({ scrollTop: $(c).offset().top }, 800, "linear");
      return false;
  });
</script>
<?php  get_footer(); ?>