<?php
if( ( !isset($_GET['org']) && empty( $_GET['org']) ) || $_GET['org'] != 'nl' ){
    wp_redirect( site_url() );
    exit();
}
/**

 * Template Name: page formulaire wicoach

 */

get_header('coach'); ?>

<div class="formulaire-mail formulaire-coach">
    <div class="content"> 
        <div class="text"><p>Merci de compléter le formulaire ci-dessous :</p></div>
        <form id="form-nl" action="">
            <div class="chp">  
                <input type="text" name="nom" placeholder="Nom" required> 
            </div> 
            <div class="chp">  
                <input type="text" name="prenom" placeholder="Prénom" required> 
            </div> 
            <div class="chp">  
                <input type="email" name="email" placeholder="Email" required> 
            </div> 
            <div class="chp">  
                <input type="tel" name="tel" placeholder="Téléphone" required> 
            </div>  
            <input type="hidden" name="ct" value="<?= $_GET['ct'] ?>">
            <div class="send">
                <span><input type="submit" value="Envoyer" name="envoyer" id="btn-demande"> </span>  
            </div>
        </form>  
    </div>  
</div>

<?php get_footer('coach'); ?>