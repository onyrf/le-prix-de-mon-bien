<!-- Step 9 -->
<input type="hidden" name="prev_step" value="7">
<input type="hidden" name="curr_step" value="8">
<div class="tab tab9 current etape_apropos_de_vous">
    <div class="estimation-form">
        <div class="titleStep text-left">
            <div class="step d-flex justify-content-start align-items-center">
                <a class="precedent"></a>
                <span><strong>8</strong> / 10 Vos informations</span>
            </div>
            <h3>À propos de vous...</h3>
        </div>
        <div class="row_step">
            <div class="estimation-row component-property-sale-status text-left">
                <div class="estimation-row__label">
                    <label>Êtes-vous propriétaire du bien ?</label>
                </div>
                <div class="row_proprietaire_chp">
                    <div class="estimation-row__input">
                        <div class="input--radio">
                            <input type="radio" value="Oui" id="mf_propertySaleStatus_yes"
                                   name="proprietaire_bien" class="input--radio__input ouiNon"
                                   <?php if( sess('proprietaire_bien') == "Oui") echo "checked" ?>
                                   >
                            <label for="mf_propertySaleStatus_yes"
                                   class="input--radio__label">Oui</label>
                        </div>
                    </div>
                    <div class="estimation-row__input">
                        <div class="input--radio">
                            <input type="radio" value="Non" id="mf_propertySaleStatus_no"
                                   name="proprietaire_bien" class="ouiNon input--radio__input"
                                   <?php if( sess('proprietaire_bien') == "Non") echo " checked" ?>
                                   >
                            <label for="mf_propertySaleStatus_no"
                                   class="input--radio__label">Non</label>
                        </div>
                    </div>
                </div>
                <input type="text" name="fake_proprio" class="fake_motiv" required 
                <?php if( sess('proprietaire_bien') == "Oui" || sess('proprietaire_bien') == "Non" ) echo "value='fill'" ?>>
                <div class="estimation-row__error estimation-row__error--full-icon"
                     style="display: none;"><!----></div>
            </div>
        </div>
        <div class="estimation-row component-email text-left">
            <div class="estimation-row__error estimation-row__error--space" style="display: none;"></div>
            <div class="row_step">
                <div class="blc-tel">
                    
                    <!-- <div class="col-leftTel">
                        <div class="estimation-row__label estimation-row__label--space label-select">
                            <label>Indiquez votre numéro téléphone</label>
                        </div>
                        <div class="estimation-row__input estimation-row__input--large">
                           <input id="component-tel__input-text" name="telephone" type="tel" placeholder="téléphone" class="input--text input-text height" required>
                        </div>
                    </div> -->
                    <div class="col-leftTel">
                         <div class="estimation-row__label estimation-row__label--space label-select">
                            <label>Peut-on vous appeler ?</label>
                        </div>
                        <div class="estimation-row__input estimation-row__input--large select">
                           <select class="select-call" name="select_call">
                               <option value="sans">Faites votre choix</option>
                               <option value="non" <?php if( sess('select_call') == "non" ) echo "selected" ?>>Non</option>
                               <option value="oui" <?php if( sess('select_call') == "oui" ) echo "selected" ?>>Oui</option>
                           </select>                           
                        </div>
                        <input type="text" name="fake_choix" class="fake_motiv" required 
                          value="<?php if( sess('select_call') == "non" || sess('select_call') == "oui" ) echo 'filled' ?>">
                    </div>
                    <div class="col-RightTel">
                        <div class="estimation-row__input estimation-row__input--large date">
                           <input id="datepicker" name="date" type="text" placeholder="Date" readonly="readonly" class="date input--text input-text height" value="<?= sess('date') ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row_step row_step_objet">
                <div class="estimation-row__label estimation-row__label--space label-select">
                    <label>Quel est l'objet de cette estimation&nbsp;?</label>
                </div>
                <div class="estimation-row__input estimation-row__input--large select obje-estim">
                   <select class="select_objet" name="obje_estim">
                    <?php $obj = sess('obje_estim') ? sess('obje_estim') : '' ?>
                      <option value="sans" <?php if( $obj == '' ) echo 'selected' ?>>Faites votre choix</option>
                       <option value="Par curiosité" <?php if( $obj == "Par curiosité" ) echo 'selected' ?>>Par curiosité</option>
                       <option value="Une vente dans les 1 à 3 mois" <?php if( $obj == "Une vente dans les 1 à 3 mois" ) echo "selected" ?>>Une vente dans les 1 à 3 mois</option>
                       <option value="Une vente dans 3 à 6 mois" <?php if( $obj == "Une vente dans 3 à 6 mois") echo "selected" ?>>Une vente dans 3 à 6 mois</option>
                       <option value="Une vente dans plus de 6 mois" <?php if( $obj == "Une vente dans plus de 6 mois") echo "selected" ?>> Une vente dans plus de 6 mois</option>
                   </select>
                </div>
                <input type="text" name="fake_objet" class="fake_motiv" required 
                          value="<?php if( sess('obje_estim') != "sans" && (
                            $obj == "Par curiosité" ||
                            $obj == "Une vente dans les 1 à 3 mois" ||
                            $obj == "Une vente dans 3 à 6 mois" ||
                            $obj == "Une vente dans plus de 6 mois"
                          ) ) echo 'filled' ?>">
            </div>

            <div class="row_step">
                <div class="component-consent component-email__consent">
                    <div class="component-consent__label">
                        <label class="field__label">Je souhaite être informé par "vendezmonbien.be"</label>
                    </div>
                    <div class="component-consent__input">
                        <div class="component-check-box field">
                            <div class="input--checkbox">
                                <input id="checkbox_consent_evolution_of_the_real_estate_market"
                                       type="checkbox" checked class="input--checkbox__input" name="informe_evolution_marche" value="Oui">
                                <label for="checkbox_consent_evolution_of_the_real_estate_market"
                                       class="input--checkbox__label">Sur l'évolution du marché immobilier
                                    <span data-v-d0628c3a="" class="component-tooltip tooltip">
                                            <a data-v-d0628c3a=""
                                               class="component-tooltip tooltip__button">
                                                <svg width="16" height="16" viewBox="0 0 20 20"
                                                     fill="none" xmlns="http://www.w3.org/2000/svg"
                                                     data-v-d0628c3a="">
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                          d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM9 5V7H11V5H9ZM11 14C11 14.55 10.55 15 10 15C9.45 15 9 14.55 9 14V10C9 9.45 9.45 9 10 9C10.55 9 11 9.45 11 10V14ZM2 10C2 14.41 5.59 18 10 18C14.41 18 18 14.41 18 10C18 5.59 14.41 2 10 2C5.59 2 2 5.59 2 10Z"
                                                          fill="#6288B9"></path>
                                                </svg>
                                            </a>
                                            <span data-v-d0628c3a=""
                                                  class="component-tooltip tooltip__popin"
                                                  style="left: 0px;">
                                                <span data-v-d0628c3a=""
                                                      class="component-tooltip tooltip__text">Vous recevrez des conseils pour vous aider à mieux suivre l’évolution du marché et à préparer vos futurs projets (immobiliers).</span>
                                            </span>
                                        </span>
                                </label>
                            </div>
                        </div>
                        <div class="component-check-box field">
                            <div class="input--checkbox">
                                <input id="checkbox_consent_buying_a_property" type="checkbox"
                                       class="input--checkbox__input" checked name="informe_achat_bien" value="Oui">
                                <label for="checkbox_consent_buying_a_property"
                                       class="input--checkbox__label">Sur l'achat d'un bien immobilier
                                    <span data-v-d0628c3a="" class="component-tooltip tooltip">
                                            <a data-v-d0628c3a=""
                                               class="component-tooltip tooltip__button">
                                                <svg width="16" height="16" viewBox="0 0 20 20"
                                                     fill="none" xmlns="http://www.w3.org/2000/svg"
                                                     data-v-d0628c3a="">
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                          d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM9 5V7H11V5H9ZM11 14C11 14.55 10.55 15 10 15C9.45 15 9 14.55 9 14V10C9 9.45 9.45 9 10 9C10.55 9 11 9.45 11 10V14ZM2 10C2 14.41 5.59 18 10 18C14.41 18 18 14.41 18 10C18 5.59 14.41 2 10 2C5.59 2 2 5.59 2 10Z"
                                                          fill="#6288B9"></path>
                                                </svg>
                                            </a>
                                            <span data-v-d0628c3a=""
                                                  class="component-tooltip tooltip__popin">
                                                <span data-v-d0628c3a=""
                                                      class="component-tooltip tooltip__text">Vous recevrez des conseils pour vous aider à acheter un bien immobilier.</span>
                                            </span>
                                        </span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
       
        
    </div>
</div>
<div class="blcBoutton btnForm clr btnForm float-right">
    <button type="button" id="prevBtn" class="precedent bouton btn misy_scrol">< étape
        précédente
    </button>
    <span class="sipan">
        <button type="submit" id="nextBtn" class="bouton btn misy_scrol">étape suivante ></button>
    </span>
</div>

<?php    
    
    if( sess('select_call') == "oui" ){ ?>
      <script>
        setTimeout(function(){
          $('option[value=oui]').prop('selected', true);
          $('.select-call').trigger('change');
        },2000)
      </script>
<?php 
    }

  if( sess('obje_estim') == 'Une vente dans les 1 à 3 mois' ){ ?>
     <script>
      setTimeout(function(){
        $('option[value=Une vente dans les 1 à 3 mois]').prop('selected', true);
        $('.select_objet').trigger('change');
      },2000)
    </script>
  <?php }
  if( sess('obje_estim') == 'Une vente dans 3 à 6 mois' ){ ?>
     <script>
      setTimeout(function(){
        $('option[value=Une vente dans 3 à 6 mois]').prop('selected', true);
        $('.select_objet').trigger('change');
      },2000)
    </script>
  <?php }
  if( sess('obje_estim') == 'Une vente dans plus de 6 mois' ){ ?>
     <script>
      setTimeout(function(){
        $('option[value=Une vente dans plus de 6 mois]').prop('selected', true);
        $('.select_objet').trigger('change');
      },2000)
    </script>
  <?php } ?>


<script>
  $(".evaluation").click(function(){
    var vis = $(this);
    vis_parent = vis.parents();
    vis_parent_prev = vis_parent.prevAll();
    vis_parent_prev.addClass('checked');
    vis_parent_next = vis_parent.nextAll();
    vis_parent_next.removeClass('checked');
    vis.attr('checked',true);
    vis_parent.addClass('checked');
    $('[name=fake_motiv]').val("filled");
  });
</script>
<script>
  $('.precedent').on('click', function(){
    var prev_step = $('[name=prev_step]').val();
    var curr_step = $('[name=curr_step]').val();
    var oldHtml = $('#form_content').html();
    $.ajax(ajaxurl, {
      type: 'POST',
      data: 'action=back_from_' + curr_step +'&show='+prev_step,
      dataType: 'html',
      success: function(resp){
        $('#form_content').html( resp);
        console.log(resp);
        // $('html, body').animate({
        // scrollTop: $(".top-step").offset().top
        // }, 1000)              
      }
    });

    return false;
})
</script>
<script>
  $('[name=proprietaire_bien]').click(function(){
    if( $('[name=proprietaire_bien]:checked').val() == 'Oui' || $('[name=proprietaire_bien]:checked').val() == 'Non' ){
      $('[name=fake_proprio').val('filled');
    }else{
      $('[name=fake_proprio').val();
    }
  })

  $('[name=select_call]').change(function(){
   var valueSelected = $(this).find("option:selected").attr("value");
    if( valueSelected == 'oui' || valueSelected == 'non' ){
      $('[name=fake_choix]').val('filled');
    }else{
      $('[name=fake_choix]').val('');
    }
  })

  $(".select-call").change(function(){
    var value = $(this).find("option:selected").attr("value");
    $('#fake_choix-error').css('display','none')
    if(value=='oui'){
      $('.date').addClass('show');

    } else{ 
      $('.date').removeClass('show');
    }
  });

  $(".select_objet").change(function(){ 
    var valueSelected = $(this).find("option:selected").attr("value");
    if( valueSelected != 'sans' ){
      $('[name=fake_objet]').val('filled');
    }else{
      $('[name=fake_objet]').val('');
    }
  });
  
// DATEPICKER
$( "#datepicker" ).datepicker({
    altField: "#datepicker",
    closeText: 'Fermer',
    prevText: 'Précédent',
    nextText: 'Suivant',
    currentText: 'Aujourd\'hui',
    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
    monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    weekHeader: 'Sem.',
    dateFormat: 'dd/mm/yy',
    minDate: 0
});

</script>