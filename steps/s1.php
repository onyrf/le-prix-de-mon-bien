<!-- Step 1 -->
<input type="hidden" name="prev_step" value="0">
<input type="hidden" name="curr_step" value="1">
<div class="tab current">
    <div class="estimation-form">
        <div class="titleStep text-left">
            <div class="step d-flex justify-content-start align-items-center">
                <a class="precedent"></a>
                <span><strong>1</strong> / 10 Espaces</span>
            </div>
            <h3>Quels sont les espaces disponibles&nbsp;?</h3>
        </div>
        <div>
            <div class="estimation-row component-habitable-surface">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>surface.svg" alt="icon">
                    </div>
                    <label>Quelle est la surface habitable en m²&nbsp;?</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon">
                    <div class="component-input-range-with-value input-range-with-value" labelkey="mf_component_net_habitable_surface">
                        <div class="input-range-with-value__slider range-wrap">
                            <div class="range-value field" id="surface_habitable"></div>
                            <input id="surface_habitable_input" name="HabitableSurface" type="range" min="0" max="2000" value="<?= $t= sess('HabitableSurface') != '' ? sess('HabitableSurface') : '0' ?>" step="1" tabindex="0" class="input--range__input">
                        </div>
                        <div class="input-range-with-value__input">
                            <div class="field">
                                <div class="input-group input-group--append">
                                    <input type="number" id="surfaceOutput" name="classified_property_livingDescription_netHabitableSurface" min="0" max="2000"  value="<?= $t= sess('classified_property_livingDescription_netHabitableSurface') != '' ? sess('classified_property_livingDescription_netHabitableSurface') : '0' ?>" class="input--text input-group__main input-group__main--append">
                                    <div id="surfaceOutput-front" class="output"><?= $t= sess('classified_property_livingDescription_netHabitableSurface') != '' ? sess('classified_property_livingDescription_netHabitableSurface') : '0' ?></div>
                                    <div class="input-group__append"><abbr class="input-group__abbr">m²</abbr></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="estimation-row component-bedroom-count d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>bedrooms.svg" alt="icon">
                    </div>
                    <label>Nombre de chambres</label>
                </div>
                <div class="estimation-row__input estimation-row__input--medium">
                    <div class="field">
                        <div class="input-group input-group--stepper">
                            <button class="input-group--stepper__button input-group--stepper__button--prepend">
                                -
                            </button>
                            <input type="number"
                                   name="chambre_nbr"
                                   class="input-group__main input-group--stepper__main input-group__main--prepend input-group__main--append input--text"
                                   value="<?= $t= sess('chambre_nbr') != '' ? sess('chambre_nbr') : '0' ?>">
                            <button class="input-group--stepper__button input-group--stepper__button--append">
                                +
                            </button>
                        </div>
                    </div>
                </div><!---->
            </div>
        </div>
        <div>
            <div class="estimation-row component-standing">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>standing-house.svg" alt="icon">
                    </div>
                    <label>Nombre de façades</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div role="group" class="component-input-group-radio field">
                        <?php $nbr_facade = sess('facade_nbr'); ?>
                        <div class="input--radio">
                            <input type="radio" name="facade_nbr" id="facade2"
                                   class="input--radio__input" value="2"
                                   <?php if( $nbr_facade == "2" || $nbr_facade != "3" || $nbr_facade == "4" ) echo 'checked'; ?>
                                   >
                            <label for="facade2" class="input--radio__label">2</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" name="facade_nbr" id="facade3"
                                   class="input--radio__input" value="3"
                                   <?php if( $nbr_facade == "3" ) echo 'checked'; ?>
                                   >
                            <label for="facade3" class="input--radio__label">3</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" name="facade_nbr" id="facade4"
                                   class="input--radio__input" value="4"
                                   <?php if( $nbr_facade == "4" ) echo 'checked'; ?>
                                   >
                            <label for="facade4" class="input--radio__label">4</label>
                        </div>
                    </div>
                </div><!---->
            </div>
        </div>
        <div>
            <div class="estimation-row component-bathroom-count d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center justify-content-start">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>bathrooms.svg" alt="icon">
                    </div>
                    <label>Nombre de salles de bains ?</label>
                </div>
                <div class="estimation-row__input estimation-row__input--medium">
                    <div class="field">
                        <div class="input-group input-group--stepper">
                            <button class="input-group--stepper__button input-group--stepper__button--prepend">-</button>
                            <input type="number" name="sdb_nbr" class="input-group__main input-group--stepper__main input-group__main--prepend input-group__main--append input--text" value="<?= $t= sess('sdb_nbr') != '' ? sess('sdb_nbr') : '0' ?>">
                            <button class="input-group--stepper__button input-group--stepper__button--append">+</button>
                        </div>
                    </div>
                </div>
            </div><!---->
        </div>
    </div>
</div>
<div class="blcBoutton btnForm clr btnForm float-right">
    <button type="button" id="prevBtn" class="precedent bouton btn misy_scrol">< étape
        précédente
    </button>
    <span class="sipan">
        <button type="submit" id="nextBtn" class="bouton btn misy_scrol">étape suivante ></button>
    </span>
</div>

<script type="text/javascript">

    jQuery('#surface_habitable_input').on('change',function(){
        var val = jQuery(this).val();console.log(val);
        jQuery('#surfaceOutput-front').html(val);
        jQuery('#surfaceOutput').val(val);
    });

     $('#surface_habitable_input').on('input', function() {
        let val = $(this).val();
        $('#surfaceOutput').val(val);

    });

    $('#surfaceOutput').on('input', function(){
      //console.log($(this).val())
      $('#surface_habitable_input').val($(this).val())
    });

    /* - or + value */
    $(".input-group--stepper button").on("click", function() {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        var btnTXT = $button.text();
        var trimBtnTXT = $.trim(btnTXT);

        if (trimBtnTXT == "+") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.parent().find("input").val(newVal);
        return false;
    });

    

</script>
<script>
$('#more_files').on('click', function(){
    // fileInit();
        
    var input_id = 'ip_'+getRandomInt(9999999); 
    var img_class = 'img_'+getRandomInt(9999999); 
    var input_id_sharp = '#'+input_id; 

    $('.upload-file .chp').append('<div class="cont-file"><div class="content_cont_file"><input type="file"  class="input-file '+input_id+'" id= "'+input_id+'" name="file[]"><div class="content-preview"><img class="imgPreview '+img_class+'"></div><div class="listBoutton"><i> Charger</i><i class="reset" style="display: none">Supprimer</i></div></div><div class="more">Ajouter une autre photo</div></div>');

    var imgNextID_dyn = '.'+img_class;
    $(input_id_sharp).change(function() {
        readURL(this, imgNextID_dyn);
    });

    fileInitOter();
    reset_file_upload();
    more_file();
    $('.cont-file').each(function(){
        var index = $(this).index();
        var img = $(this).find(".imgPreview");
        img.attr('id',index);
        var input = $(this).find(".input-file");
    });

});
fileInit();
fileInitOter();
reset_file_upload();
more_file();


  function handleFileSelect(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.cont-file').each(function(){
                imgId = '#'+$('.imgPreview').attr('id');
                $(this).find(imgId).attr('src', e.target.result);
            });
        }

        reader.readAsDataURL(input.files[0]);
    }
  }

    
  $("input[type='file']").change(function(){
    handleFileSelect(this);
  });

function fileInit(){
    $('.input-file').change(function() {
        iz=$(this)
        val=iz.val()
        par=iz.parent(".cont-file")
        txtExt=par.find("span")
        txtBroswer=par.find(".listBoutton i")
        txtExt.text(val) 
        // txtBroswer.text("Modifier");
        txtBroswer.css('display','none');
        var index = $(this).parents(".cont-file").index();
        var img = $(this).siblings(".imgPreview");
        img.attr('id',index);

        // iz.hide();
        txtExt.hide();

        imgPreview=iz.siblings('.content-preview').find('.imgPreview');
        imgPreview_parent =imgPreview.parents('.content-preview') ;
        imgPreview.addClass('shown');
        imgPreview_parent.addClass('shown');

    });
}
function fileInitOter(){
    $('.input-file').each(function(){
        $(this).change(function() {
            $(this).parents(".cont-file").addClass('uploaded');
            
            iz=$(this)
            resetBtn=iz.siblings('.listBoutton').find('.reset');
            imgPreview=iz.siblings('.content-preview').find('.imgPreview');
            imgPreview_parent =imgPreview.parents('.content-preview') ;
            imgPreview.addClass('shown');
            imgPreview_parent.addClass('shown');
            val=iz.val()
            if(val!=""){
                par=iz.parent(".content_cont_file")
                txtExt=par.find("span")
                txtBroswer=par.find(".listBoutton i")
                par.siblings('.more').addClass('show')
                txtExt.text(val) 
                // txtBroswer.text("Modifier")
                txtBroswer.css('display','none');
                resetBtn.show()
                resetBtn.text('Supprimer');

                // iz.hide();
                txtExt.hide();
            }
            
        })
    })
}

function reset_file_upload(){
    $('.reset').each(function(){
        $('.reset').on('click', function(){
            reset = $(this);

            btnChanger = $(this).siblings('i');
            inputFile  = $(this).siblings('.input-file');
            fakeText   = $(this).siblings('span');
            imgPreview   = $(this).parents('.listBoutton').siblings('.content-preview').find('.imgPreview');
            imgPreview_parent =imgPreview.parents('.content-preview') ;
            btnChanger.text("Charger");
            inputFile.val('');

            inputFile.show();
            fakeText.show();
            $('.more').removeClass('show')


            fakeText.text("Aucun fichier sélectionné");

            imgPreview.attr('src', '');
            imgPreview.removeClass('shown');
            imgPreview_parent.removeClass('shown');

            if ( reset.parents('#spn_inputs').length == 1 ){
                reset.parent('.cont-file').remove();
            }else{
                reset.hide();

            }
            return false;
        });
    });
}
function more_file(){
$('.more').each(function(){
    $(this).click(function(){
        $('#more_files').trigger('click');
        $(this).removeClass('show')

    })
})
}

$('.precedent').on('click', function(){
    var prev_step = $('[name=prev_step]').val();
    var curr_step = $('[name=curr_step]').val();
    var oldHtml = $('#form_content').html();
    $.ajax(ajaxurl, {
      type: 'POST',
      data: 'action=back_from_' + curr_step +'&show='+prev_step,
      dataType: 'html',
      success: function(resp){
        $('#form_content').html( resp);
        
        $('html, body').animate({
        scrollTop: $(".top-step").offset().top
        }, 1000)              
      }
    });

    return false;
});
</script>