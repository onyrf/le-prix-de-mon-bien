<!-- début -->
<input type="hidden" name="prev_step" value="-1">
<input type="hidden" name="curr_step" value="0">
<input type="hidden" id='current_nbr_demande' name="current_nbr_demande" value="<?= get_field('total_nbr_demande', get_option('page_on_front')) ?>">
<div class="tab current">
    
    <div class="form-group marge-mob">
        <div class="row">
            <div class="col-md-6">
                <div class="champ">
                    <input type="text" for="lname" class="form-control" placeholder="Nom" name="lname" value="<?php if( $lead){ echo $lead->lname; }else{ echo sess('lname'); } ?>" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="champ">
                    <input type="text" for="fname" class="form-control" placeholder="Prénom" name="fname" value="<?php if($lead){ echo $lead->fname; }else{ echo sess('fname'); } ?>" required>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group marge-mob">
        <div class="row">
            <div class="col-md-6">
                <div class="champ">
                    <input id="component-tel__input-text" name="telephone" type="tel" placeholder="téléphone" class="form-control" value="<?php if( $lead){ echo $lead->telephone; }else{ echo sess('telephone'); } ?>" required>   
                </div>   
            </div>
            <div class="col-md-6">
                <div class="champ">
                    <input id="component-email__input-text" name="email" type="email" placeholder="email@domaine.com"  class="form-control etp1"  value="<?php if( $lead){ echo $lead->email; }else{ echo sess('email'); } ?>" required> 
                </div>  
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <div class="champ">
                        <input type="text" for="adresse" class="form-control" placeholder="Adresse du bien" value="<?php if( $lead){ echo $lead->adresse; }else{ echo sess('adresse'); } ?>"
                       name="adresse" id="autocomplete" required>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group row d-flex">
        <div class="col-md-3">
            <div class="champ">
                    <input type="number" for="numero" class="form-control" placeholder="Numéro"
                   name="numero" id="street_number"  value="<?php if( $lead){ echo $lead->numero; }else{ echo sess('numero'); } ?>" required>
            </div>
        </div>
        <div class="col-md-3">
            <div class="champ">
                    <input type="number" for="postal" class="form-control" placeholder="Code Postal"
                   name="postal" id="postal_code"  value="<?php if( $lead){ echo $lead->postal; }else{ echo sess('postal'); } ?>" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="champ">
                <input type="text" for="ville" class="form-control" placeholder="Ville" name="ville" id="locality" value="<?php if( $lead){ echo $lead->ville; }else{ echo sess('ville'); } ?>" required>
            </div>
        </div>
    </div>
    <div class="form-group row d-flex align-items-start form-group-type-appartement ">
        <div class="col-md-6 d-flex align-items-start no_padding">
            <div class="col-sm-6 lasa">
                <div class="input-radio">
                    <input name="property_type" id="APARTMENT" type="radio" value="Appartement" required <?php if( sess('property_type') == 'Appartement') echo 'checked' ?>>
                    <label for="APARTMENT" class="input-radio-label prop_type">
                        <div class="iconLabel">
                            <img src="<?= IMG_DIR ?>apartment.svg" alt="icon">
                        </div>
                        <div class="textLabel">
                            <span>Appartement</span>
                        </div>
                    </label>
                </div>
            </div>
            <div class="col-sm-6 lasa" id="scroll-form">
                <div class="input-radio">
                    <input name="property_type" id="MAISON" type="radio" value="Maison" required <?php if( sess('property_type') == 'Maison') echo 'checked' ?>>
                    <label for="MAISON" class="input-radio-label prop_type">
                        <div class="iconLabel">
                            <img src="<?= IMG_DIR ?>house.svg" alt="icon">
                        </div>
                        <div class="textLabel">
                            <span>Maison</span>
                        </div>
                    </label>
                </div>
            </div>
        </div>
        <div class="blcBoutton btnForm clr btnForm col-md-6 d-flex justify-content-end">
            <span class="sipan">
                <button type="submit" id="nextBtn" class="bouton btn misy_scrol">étape suivante ></button>
            </span>
        </div>
    </div>
</div>
<script>
     jQuery("#APARTMENT, #MAISON").on('click',function() {
        jQuery("#nextBtn").trigger('click');
      });
</script>