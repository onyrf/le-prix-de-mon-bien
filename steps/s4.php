<!-- Step 5 -->
<input type="hidden" name="prev_step" value="3">
<input type="hidden" name="curr_step" value="4">
<div class="tab tab4 current">
    <div class="estimation-form">
        <div class="titleStep text-left">
            <div class="step d-flex justify-content-start align-items-center">
                <a class="precedent"></a>
                <span><strong>4</strong> / 10 Spécifications</span>
            </div>
            <h3>Intérieurs</h3>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>cuisine.svg" alt="icon">
                    </div>
                    <label>Cuisine</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="À renouveler" id="arenouveler-cuisine"
                               name="cuisine" class="input--radio__input batiment restore"
                               <?php if( !sess('cuisine') || sess('cuisine') == '' || sess('cuisine') == "À renouveler") echo "checked" ?>
                               >
                        <label for="arenouveler-cuisine" class="input--radio__label">À renouveler</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatmoyen-cuisine" name="cuisine" value="État moyen"
                               class="input--radio__input batiment"
                               <?php if( sess('cuisine') == "État moyen") echo " checked" ?>
                               >
                        <label for="etatmoyen-cuisine" class="input--radio__label">État moyen</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="bonEtat-cuisine" name="cuisine" value="Bon état général"
                               class="input--radio__input batiment"
                               <?php if( sess('cuisine') == "Bon état général") echo " checked" ?>
                               >
                        <label for="bonEtat-cuisine" class="input--radio__label">Bon état général</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatNeuf-cuisine" name="cuisine"
                               class="input--radio__input batiment" value="État neuf ou assimilé"
                               <?php if( sess('cuisine') == "État neuf ou assimilé") echo " checked" ?>
                               >
                        <label for="etatNeuf-cuisine" class="input--radio__label">État neuf ou assimilé</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>sanitaires.svg" alt="icon">
                    </div>
                    <label>Sanitaires</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="À renouveler" id="arenouveler-sanitaire"
                               name="sanitaire" class="input--radio__input batiment restore"
                               <?php if( !sess('sanitaire') || sess('sanitaire') == '' || sess('sanitaire') == "À renouveler") echo "checked" ?>
                               >
                        <label for="arenouveler-sanitaire" class="input--radio__label">À renouveler</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatmoyen-sanitaire" name="sanitaire" value="État moyen"
                               class="input--radio__input batiment"
                               <?php if( sess('sanitaire') == "État moyen") echo " checked" ?>
                               >
                        <label for="etatmoyen-sanitaire" class="input--radio__label">État moyen</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="bonEtat-sanitaire" name="sanitaire" value="Bon état général"
                               class="input--radio__input batiment"
                               <?php if( sess('sanitaire') == "Bon état général") echo " checked" ?>
                               >
                        <label for="bonEtat-sanitaire" class="input--radio__label">Bon état général</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatNeuf-sanitaire" name="sanitaire"
                               class="input--radio__input batiment" value="État neuf ou assimilé"
                               <?php if( sess('sanitaire') == "État neuf ou assimilé") echo " checked" ?>
                               >
                        <label for="etatNeuf-sanitaire" class="input--radio__label">État neuf ou assimilé</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>revetement.svg" alt="icon">
                    </div>
                    <label>Revêtement de sol</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="À renouveler" id="arenouveler-revetSol"
                               name="revetement_sol" class="input--radio__input batiment restore"
                               <?php if( !sess('revetement_sol') || sess('sanitaire') == '' || sess('sanitaire') == "À renouveler") echo "checked" ?>
                               >
                        <label for="arenouveler-revetSol" class="input--radio__label">À renouveler</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatmoyen-revetSol" name="revetement_sol" value="État moyen"
                               class="input--radio__input batiment"
                               <?php if( sess('revetement_sol') == "État moyen") echo " checked" ?>
                               >
                        <label for="etatmoyen-revetSol" class="input--radio__label">État moyen</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="bonEtat-revetSol" name="revetement_sol" value="Bon état général"
                               class="input--radio__input batiment"
                               <?php if( sess('revetement_sol') == "Bon état général") echo " checked" ?>
                               >
                        <label for="bonEtat-revetSol" class="input--radio__label">Bon état général</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatNeuf-revetSol" name="revetement_sol"
                               class="input--radio__input batiment" value="État neuf ou assimilé"
                               <?php if( sess('revetement_sol') == "État neuf ou assimilé") echo " checked" ?>
                                >
                        <label for="etatNeuf-revetSol" class="input--radio__label">État neuf ou assimilé</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>peinture.svg" alt="icon">
                    </div>
                    <label>Peinture</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="À renouveler" id="arenouveler-peinture"
                               name="peinture" class="input--radio__input batiment restore" 
                               <?php if( !sess('peinture') || sess('peinture') == '' || sess('peinture') == "À renouveler") echo "checked" ?>
                               >
                        <label for="arenouveler-peinture" class="input--radio__label">À renouveler</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatmoyen-peinture" name="peinture" value="État moyen"
                               class="input--radio__input batiment"
                               <?php if( sess('peinture') == "État moyen") echo " checked" ?>
                               >
                        <label for="etatmoyen-peinture" class="input--radio__label">État moyen</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="bonEtat-peinture" name="peinture" value="Bon état général"
                               class="input--radio__input batiment"
                               <?php if( sess('peinture') == "Bon état général") echo " checked" ?>
                               >
                        <label for="bonEtat-peinture" class="input--radio__label">Bon état général</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatNeuf-peinture" name="peinture"
                               class="input--radio__input batiment" value="État neuf ou assimilé"
                               <?php if( sess('peinture') == "État neuf ou assimilé") echo " checked" ?>
                               >
                        <label for="etatNeuf-peinture" class="input--radio__label">État neuf ou assimilé</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="titleStep text-left">
            <h3>Quelles sont les caractéristiques du bien&nbsp;?</h3>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>electricite.svg" alt="icon">
                    </div>
                    <label>Electricité</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="À renouveler" id="arenouveler-elect"
                               name="electricite" class="input--radio__input batiment restore"
                               <?php if( !sess('electricite') || sess('electricite') == '' || sess('electricite') == "À renouveler") echo "checked" ?>
                               >
                        <label for="arenouveler-elect" class="input--radio__label">À renouveler</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatmoyen-elect" name="electricite" value="État moyen"
                               class="input--radio__input batiment"
                               <?php if( sess('electricite') == "État moyen") echo " checked" ?>
                               >
                        <label for="etatmoyen-elect" class="input--radio__label">État moyen</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="bonEtat-elect" name="electricite" value="Bon état général"
                               class="input--radio__input batiment"
                               <?php if( sess('electricite') == "Bon état général") echo " checked" ?>
                               >
                        <label for="bonEtat-elect" class="input--radio__label">Bon état général</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatNeuf-elect" name="electricite"
                               class="input--radio__input batiment" value="État neuf ou assimilé"
                               <?php if( sess('electricite') == "État neuf ou assimilé") echo " checked" ?>
                               >
                        <label for="etatNeuf-elect" class="input--radio__label">État neuf ou assimilé</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>toiture.svg" alt="icon">
                    </div>
                    <label>Toiture</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="À renouveler" id="arenouveler-toit"
                               name="toiture" class="input--radio__input batiment restore"
                               <?php if( !sess('toiture') || sess('toiture') == '' || sess('toiture') == "À renouveler") echo "checked" ?>
                               >
                        <label for="arenouveler-toit" class="input--radio__label">À renouveler</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatmoyen-toit" name="toiture" value="État moyen"
                               class="input--radio__input batiment"
                               <?php if( sess('toiture') == "État moyen") echo " checked" ?>
                               >
                        <label for="etatmoyen-toit" class="input--radio__label">État moyen</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="bonEtat-toit" name="toiture" value="Bon état général"
                               class="input--radio__input batiment"
                               <?php if( sess('toiture') == "Bon état général") echo " checked" ?>
                               >
                        <label for="bonEtat-toit" class="input--radio__label">Bon état général</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatNeuf-toit" name="toiture"
                               class="input--radio__input batiment" value="État neuf ou assimilé"
                               <?php if( sess('toiture') == "État neuf ou assimilé") echo " checked" ?>
                               >
                        <label for="etatNeuf-toit" class="input--radio__label">État neuf ou assimilé</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>chassis.svg" alt="icon">
                    </div>
                    <label>Châssis/ Vitrage</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="À renouveler" id="arenouveler-vitrage"
                               name="chassis" class="input--radio__input batiment restore"
                               <?php if( !sess('chassis') || sess('chassis') == '' || sess('chassis') == "À renouveler") echo "checked" ?>
                                >
                        <label for="arenouveler-vitrage" class="input--radio__label">À renouveler</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatmoyen-vitrage" name="chassis" value="État moyen"
                               class="input--radio__input batiment"
                               <?php if( sess('chassis') == "État moyen") echo " checked" ?>
                               >
                        <label for="etatmoyen-vitrage" class="input--radio__label">État moyen</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="bonEtat-vitrage" name="chassis" value="Bon état général"
                               class="input--radio__input batiment"
                               <?php if( sess('chassis') == "Bon état général") echo " checked" ?>
                               >
                        <label for="bonEtat-vitrage" class="input--radio__label">Bon état général</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatNeuf-vitrage" name="chassis"
                               class="input--radio__input batiment" value="État neuf ou assimilé"
                               <?php if( sess('chassis') == "État neuf ou assimilé") echo " checked" ?>
                               >
                        <label for="etatNeuf-vitrage" class="input--radio__label">État neuf ou assimilé</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>chauffage.svg" alt="icon">
                    </div>
                    <label>Chauffage</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="À renouveler" id="arenouveler-chauffage"
                               name="chauffage" class="input--radio__input batiment restore"
                               <?php if( !sess('chauffage') || sess('chauffage') == '' || sess('chauffage') == "À renouveler") echo "checked" ?>
                               >
                        <label for="arenouveler-chauffage" class="input--radio__label">À renouveler</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatmoyen-chauffage" name="chauffage" value="État moyen"
                               class="input--radio__input batiment"
                               <?php if( sess('chauffage') == "État moyen") echo " checked" ?>
                               >
                        <label for="etatmoyen-chauffage" class="input--radio__label">État moyen</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="bonEtat-chauffage" name="chauffage" value="Bon état général"
                               class="input--radio__input batiment"
                               <?php if( sess('chauffage') == "Bon état général") echo " checked" ?>
                               >
                        <label for="bonEtat-chauffage" class="input--radio__label">Bon état général</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="etatNeuf-chauffage" name="chauffage"
                               class="input--radio__input batiment" value="État neuf ou assimilé"
                               <?php if( sess('chauffage') == "État neuf ou assimilé") echo " checked" ?>
                               >
                        <label for="etatNeuf-chauffage" class="input--radio__label">État neuf ou assimilé</label>
                    </div>
                </div>
            </div>
        </div>
        

    </div>
</div>

<div class="blcBoutton btnForm clr btnForm float-right">
    <button type="button" id="prevBtn" class="precedent bouton btn misy_scrol">< étape précédente
    </button>
    <span class="sipan">
        <button type="submit" id="nextBtn" class="bouton btn misy_scrol">étape suivante ></button>
    </span>
</div>
<script>
  $('.precedent').on('click', function(){
    var prev_step = $('[name=prev_step]').val();
    var curr_step = $('[name=curr_step]').val();
    var oldHtml = $('#form_content').html();
    $.ajax(ajaxurl, {
      type: 'POST',
      data: 'action=back_from_' + curr_step +'&show='+prev_step,
      dataType: 'html',
      success: function(resp){
        $('#form_content').html( resp);
        console.log(resp);
        // $('html, body').animate({
        // scrollTop: $(".top-step").offset().top
        // }, 1000)              
      }
    });

    return false;
})
</script>