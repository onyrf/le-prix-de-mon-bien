<!-- Step 2 -->
<input type="hidden" name="prev_step" value="2">
<input type="hidden" name="curr_step" value="3">
<div class="tab current" id="tab2">
    <div class="estimation-form">
        <div class="titleStep text-left">
            <div class="step d-flex justify-content-start align-items-center">
                <a class="precedent"></a>
                <span><strong>3</strong> / 10 Spécifications</span>
            </div>
            <h3>Quelles sont les caractéristiques de l'immeuble&nbsp;?</h3>
        </div>
        <div>
            <div class="estimation-row component-construction-year">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>construction-year.svg" alt="icon">
                    </div>
                    <label>Quelle est l'année de construction&nbsp;?</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="input-range-with-value__slider range-wrap">
                            <div class="range-value field" id="anneSlide"></div>
                            <?php $year = sess('construct_year') ? sess('construct_year') : '1600' ?>
                             <input id="anneSlide_input" name="construct_year" type="range" min="1600" max="2021" tabindex="1600" class="input--range__input" value="<?= $year ?>" step="1">
                        </div>
                        <div class="input-range-with-value__input">
                            <div class="input-group input-group--append d-flex align-items-center">
                                <input id="yearOutput" type="number" min="1600" max="2021" name="AnneSlide" class="input--text input-group__main input-group__main--append" value="<?= $year ?>">
                                <div id="surfaceOutput_front5" class="output"></div>
                            </div>
                        </div>
                    </div>
                </div><!---->
            </div>
        </div>
        <div>
            <div class="estimation-row component-facade-count d-flex justify-content-between align-items-center">
                <div class="estimation-row__icon">
                    <img src="<?= IMG_DIR ?>house1.svg" alt="icon">
                </div>
                <div class="w-100 etage">
                    <div class="d-flex justify-content-between align-items-center w-100">
                        <label>Combien d'étages a le bâtiment ?</label>
                        <div class="estimation-row__input estimation-row__input--medium">
                            <div class="field">
                                <div class="input-group input-group--stepper">
                                    <button class="input-group--stepper__button input-group--stepper__button--prepend">
                                        -
                                    </button>
                                    <input type="number" name="floor_batiment_nbr" 
                                           class="input-group__main input-group--stepper__main input-group__main--prepend input-group__main--append input--text"
                                           value="<?= $t = sess('floor_batiment_nbr') ? sess('floor_batiment_nbr') : 0 ?>">
                                    <button class="input-group--stepper__button input-group--stepper__button--append">
                                        +
                                    </button>
                                </div>
                            </div>
                        </div><!---->
                    </div>
                    <div id="sub3"
                         class="estimation-row__subfield d-flex justify-content-between align-items-center w-100">
                        <label>À quel étage se situe le bien ?</label>
                        <div class="estimation-row__subfield__input estimation-row__subfield__input--medium">
                            <div class="field">
                                <div class="input-group input-group--stepper">
                                    <button class="input-group--stepper__button input-group--stepper__button--prepend">
                                        -
                                    </button>
                                    <input type="number" name="floor_batiment" 
                                           class="input-group__main input-group--stepper__main input-group__main--prepend input-group__main--append input--text"
                                           value="<?= $t = sess('floor_batiment') ? sess('floor_batiment') : 0 ?>">
                                    <button class="input-group--stepper__button input-group--stepper__button--append">
                                        +
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="estimation-row component-amenities">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>amenities-house.svg" alt="icon">
                    </div>
                    <label>Aménagements</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon estimation-row__input--font-body d-flex text-left">
                    <div class="input--checkbox">
                        <input type="checkbox" id="mf_component_basement"
                               name="cave"
                               class="input--checkbox__input" value="Oui"
                               <?php if( sess('cave') == 'Oui' ) echo 'checked' ?>
                               >
                        <label for="mf_component_basement" class="input--checkbox__label">Y a-t-il
                            une cave ?</label>
                    </div>
                   <!--  <div class="component-has-attic">
                        <div class="input--checkbox">
                            <input type="checkbox" id="mf_component_attic_label"
                                   name="grenier"
                                   class="input--checkbox__input" value="Oui">
                            <label for="mf_component_attic_label" class="input--checkbox__label">Y
                                a-t-il un grenier ?</label>
                        </div>
                    </div> -->
                    <div class="component-has-parking-with-parking-surface">
                        <div class="input--checkbox">
                            <input type="checkbox" id="mf_component_parking" name="parking"
                                   class="input--checkbox__input" value="Oui">
                            <label for="mf_component_parking" class="input--checkbox__label">Y
                                a-t-il un parking ?</label>
                        </div>
                        <div class="row_parking">
                            <div id="sub1"
                                 class="estimation-row__subfield d-flex justify-content-start align-items-center">
                                <div class="estimation-row__subfield__label estimation-row__subfield__label--medium">
                                    <label>Extérieur</label>
                                </div>
                                <div class="estimation-row__subfield__input estimation-row__subfield__input--medium">
                                    <div class="field">
                                        <div class="input-group input-group--stepper">
                                            <button class="input-group--stepper__button input-group--stepper__button--prepend">
                                                -
                                            </button>
                                            <input type="number" name="parking_exterieur" 
                                                   class="input-group__main input-group--stepper__main input-group__main--prepend input-group__main--append input--text"
                                                   value="<?= $t = sess('parking_exterieur') ? sess('parking_exterieur') : 0 ?>">
                                            <button class="input-group--stepper__button input-group--stepper__button--append">
                                                +
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="estimation-row__error" style="display: none;">
                                </div>
                            </div>
                            <div id="sub2"
                                 class="estimation-row__subfield d-flex justify-content-start align-items-center">
                                <div class="estimation-row__subfield__label estimation-row__subfield__label--medium">
                                    <label for="mf_component_parking_indoor">Intérieur</label>
                                </div>
                                <div class="estimation-row__subfield__input estimation-row__subfield__input--medium">
                                    <div class="field">
                                        <div class="input-group input-group--stepper">
                                            <button class="input-group--stepper__button input-group--stepper__button--prepend">
                                                -
                                            </button>
                                            <input type="number" name="parking_interieur"
                                                   class="input-group__main input-group--stepper__main input-group__main--prepend input-group__main--append input--text"
                                                   value="<?= $t = sess('parking_interieur') ? sess('parking_interieur') : 0 ?>">
                                            <button class="input-group--stepper__button input-group--stepper__button--append">
                                                +
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="estimation-row__error" style="display: none;"></div>
                            </div>
                        </div>
                    </div>
                </div><!---->
            </div>
            
        </div>
    </div>
</div>
<div class="blcBoutton btnForm clr btnForm float-right">
    <button type="button" id="prevBtn" class="precedent bouton btn misy_scrol">< étape
        précédente
    </button>
    <span class="sipan">
        <button type="submit" id="nextBtn" class="bouton btn misy_scrol">étape suivante ></button>
    </span>
</div>
<?php 
    if( sess('parking') == "Oui") 
        echo '<script>setTimeout(function(){$("#mf_component_parking").trigger("click")},2000)</script>';
?>
<script>
   
    $('#mf_component_parking').change(function(){
        // $( "#sub1, #sub2" ).toggleClass( "show", 200 );
        if (this.checked) {
            $( "#sub1, #sub2" ).addClass( "show", 200 );
        }
        else {
            $( "#sub1, #sub2" ).removeClass( "show", 200 );
        }
    });

    $(document).ready(function () {
        if($('#mf_component_parking').is(':checked')){
            $( "#sub1, #sub2" ).addClass( "show", 200 );
        }
        else {
            $( "#sub1, #sub2" ).removeClass( "show", 200 );
        }

    })




    $('#anneSlide_input').on('input', function() {
        let val = $(this).val();
        $('#yearOutput').val(val);
    });

    $('#yearOutput').on('input', function(){
      //console.log($(this).val())
      $('#anneSlide_input').val($(this).val())
    });

    /* - or + value */
    $(".input-group--stepper button").on("click", function() {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        var btnTXT = $button.text();
        var trimBtnTXT = $.trim(btnTXT);

        if (trimBtnTXT == "+") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.parent().find("input").val(newVal);
        return false;
    });

    $('.precedent').on('click', function(){
        var prev_step = $('[name=prev_step]').val();
        var curr_step = $('[name=curr_step]').val();
        var oldHtml = $('#form_content').html();
        $.ajax(ajaxurl, {
          type: 'POST',
          data: 'action=back_from_' + curr_step +'&show='+prev_step,
          dataType: 'html',
          success: function(resp){
            $('#form_content').html( resp);
            console.log(resp);
            // $('html, body').animate({
            // scrollTop: $(".top-step").offset().top
            // }, 1000)              
          }
        });

        return false;
    })
</script>