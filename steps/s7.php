<!-- Step 8 -->
<input type="hidden" name="prev_step" value="6">
<input type="hidden" name="curr_step" value="7">
<div class="tab current step_some_question">
    <div class="estimation-form">
        <div class="titleStep text-left">
            <div class="step d-flex justify-content-start align-items-center">
                <a class="precedent"></a>
                <span><strong>7</strong> / 10 Informations supplémentaires</span>
            </div>
            <h3>Allez, plus que quelques questions …</h3>
        </div>
        <!-- Orientation -->
        <div class="orientation">
            <div class="estimation-row component-orientation">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>solar-orientation.svg" alt="icon">
                    </div>
                    <label>Quelle est l'orientation du jardin&nbsp;?</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon estimation-row__input--center text-left">
                    <div class="component-compass-slider compass-slider">
                        <div class="compass-slider__output">
                            <?php $default = sess('orientation') ? sess('orientation') : 'Nord-Est';
                                echo $default;
                             ?>
                        </div>
                        <input type="hidden" id="orientation" name="orientation" value="<?= $default ?>">
                        <div class="compass-slider__label">
                            <p class="compass-slider__label__letter n"><span>N</span></p>
                            <p class="compass-slider__label__letter ne">
                                <span>NE</span></p>
                            <p class="compass-slider__label__letter e"><span>E</span></p>
                            <p class="compass-slider__label__letter se"><span>SE</span></p>
                            <p class="compass-slider__label__letter s"><span>S</span></p>
                            <p class="compass-slider__label__letter sw"><span>SO</span></p>
                            <p class="compass-slider__label__letter w"><span>O</span></p>
                            <p class="compass-slider__label__letter nw"><span>NO</span></p>
                        </div>
                        <div class="compass-slider__body">
                            <div class="compass-slider__input">
                                <button data-cardinal="n" data-orientation="0"
                                        class="compass-slider__input__area n" data-txt="Nord" id="nord"></button>
                                <button data-cardinal="ne" data-orientation="45"
                                        class="compass-slider__input__area ne" data-txt="Nord-Est" id="nordest" ></button>
                                <button data-cardinal="e" data-orientation="90"
                                        class="compass-slider__input__area e" data-txt="Est" id="est"></button>
                                <button data-cardinal="se" data-orientation="135"
                                        class="compass-slider__input__area se" data-txt="Sud-Est" id="sudest"></button>
                                <button data-cardinal="s" data-orientation="180"
                                        class="compass-slider__input__area s" data-txt="Sud" id="sud"></button>
                                <button data-cardinal="sw" data-orientation="225"
                                        class="compass-slider__input__area sw" data-txt="Sud-Ouest" id="sudouest"></button>
                                <button data-cardinal="w" data-orientation="270"
                                        class="compass-slider__input__area w" data-txt="Ouest" id="ouest"></button>
                                <button data-cardinal="nw" data-orientation="315"
                                        class="compass-slider__input__area nw" data-txt="Nord-Ouest" id="nordouest"></button>
                                <div class="compass-slider__cursor"
                                     style="transform: rotate(45deg);">
                                    <div class="compass-slider__cursor__handle">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!---->
                </div>
            </div>
        </div>
        <div class="autre_element">
            <div class="estimation-row component-value-increasing-criteria">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>estimation-house.svg" alt="icon">
                    </div>
                    <label>Quels autres éléments influencent le prix&nbsp;?</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div>
                        <?php
                            $influences = sess('influence');
                            $influences = json_decode( $influences );
                        ?>
                        <div class="component-check-box field">
                            <div class="input--checkbox">
                                <input id="calme" type="checkbox" class="input--checkbox__input"
                                       value="Quartier calme" name="influence[]"
                                       <?php if( in_array("Quartier calme", $influences) ) echo "checked"; ?>
                                       >
                                <label for="calme" class="input--checkbox__label">Quartier calme</label>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="component-check-box field">
                            <div class="input--checkbox">
                                <input id="bonne" type="checkbox" class="input--checkbox__input"
                                       value="Bonne performance énergétique" name="influence[]"
                                       <?php if( in_array("Bonne performance énergétique", $influences) ) echo "checked"; ?>
                                       >
                                <label for="bonne" class="input--checkbox__label">Bonne performance énergétique</label>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="component-check-box field">
                            <div class="input--checkbox">
                                <input id="solaire" type="checkbox" class="input--checkbox__input"
                                       value="Panneaux solaires" name="influence[]"
                                       <?php if( in_array("Panneaux solaires", $influences) ) echo "checked"; ?>
                                       >
                                <label for="solaire" class="input--checkbox__label">Panneaux solaires</label>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="component-check-box field">
                            <div class="input--checkbox">
                                <input id="piscine" type="checkbox" class="input--checkbox__input"
                                       value="Piscine" name="influence[]"
                                       <?php if( in_array("Piscine", $influences) ) echo "checked"; ?>
                                       >
                                <label for="piscine" class="input--checkbox__label">Piscine</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="estimation-row__error"><!----></div>
            </div>
        </div>
    </div>
</div>
<div class="blcBoutton btnForm clr btnForm float-right">
    <button type="button" id="prevBtn" class="precedent bouton btn misy_scrol">< étape
        précédente
    </button>
    <span class="sipan">
        <button type="submit" id="nextBtn" class="bouton btn misy_scrol">étape suivante ></button>
    </span>
</div>
<?php 
    if( sess('orientation') == "Nord-Est") 
        echo '<script>setTimeout(function(){$("#nordest").trigger("click")},1100)</script>';
    if( sess('orientation') == "Est") 
        echo '<script>setTimeout(function(){$("#est").trigger("click")},1500)</script>';
    if( sess('orientation') == "Sud-est") 
        echo '<script>setTimeout(function(){$("#sudest").trigger("click")},1500)</script>';
    if( sess('orientation') == "Sud") 
        echo '<script>setTimeout(function(){$("#sud").trigger("click")},1500)</script>';
    if( sess('orientation') == "Sud-Ouest") 
        echo '<script>setTimeout(function(){$("#sudouest").trigger("click")},1500)</script>';
    if( sess('orientation') == "Ouest") 
        echo '<script>setTimeout(function(){$("#ouest").trigger("click")},1500)</script>';
    if( sess('orientation') == "Nord-ouest") 
        echo '<script>setTimeout(function(){$("#nordouest").trigger("click")},1500)</script>';
    if( sess('orientation') == "Nord") 
        echo '<script>setTimeout(function(){$("#nord").trigger("click")},1500)</script>';
?>
<script>
    // Orientation
$('.compass-slider__input button').click(function(){
    var vis = $(this);
    var orientation = $('#orientation');
    var dataOrientation = vis.attr('data-orientation');
    var vCursor = $('.compass-slider__cursor');
    var screen = $('.compass-slider__output');

    orientation.val(vis.attr('data-txt'));
    screen.text(vis.attr('data-txt'));
    vCursor.css( { 'transform': 'rotate(' + dataOrientation + 'deg)'});

    return false
});    
</script>
<script>
  $('.precedent').on('click', function(){
    var prev_step = $('[name=prev_step]').val();
    var curr_step = $('[name=curr_step]').val();
    var oldHtml = $('#form_content').html();
    $.ajax(ajaxurl, {
      type: 'POST',
      data: 'action=back_from_' + curr_step +'&show='+prev_step,
      dataType: 'html',
      success: function(resp){
        $('#form_content').html( resp);
        // $('html, body').animate({
        // scrollTop: $(".top-step").offset().top
        // }, 1000)              
      }
    });

    return false;
})
</script>