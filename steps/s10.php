<?php prepare_checkout(); ?>
<div class="tab tab3 current">
    <div class="estimation-form">
        <div class="titleStep text-left">
            <div class="step d-flex justify-content-start align-items-center">
                <span>Nous vous redirigeons à la caisse...</span>
            </div>
        </div>        
    </div>
</div>
<script>
  window.location.replace('<?= site_url("/paiement") ?>');
</script>