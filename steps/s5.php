<!-- Step 3 -->
<input type="hidden" name="prev_step" value="4">
<input type="hidden" name="curr_step" value="5">
<div class="tab step3 current step_etat_bien">
    <div class="estimation-form">
        <div class="titleStep text-left">
            <div class="step d-flex justify-content-start align-items-center">
                <a class="precedent"></a>
                <span><strong>5</strong> / 10 Spécifications</span>
            </div>
            <h3>Quelles sont les caractéristiques du bien&nbsp;?</h3>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>condition-house.svg" alt="icon">
                    </div>
                    <label>Dans quel état est actuellement le bâtiment&nbsp;?</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="À rénover" id="arenove"
                               name="batiment" class="input--radio__input batiment restore"
                               <?php if( !sess('batiment') || sess('batiment') == '' || sess('batiment') == "À rénover") echo "checked" ?>
                               >
                        <label for="arenove" class="input--radio__label">À
                            rénover</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="bonEtat" name="batiment" value="En bon état"
                               class="input--radio__input batiment">
                        <label for="bonEtat" class="input--radio__label">En bon état</label>
                    </div>
                    <div class="etat estimation-row__subfield" style="display: none;">
                        <div class="estimation-row__subfield__input estimation-row__subfield__input--full">
                            <div class="last-renovation-year">
                                <div class="last-renovation-year__field">
                                    <div class="last-renovation-year__row">
                                        <div class="input--radio">
                                            <input type="radio" id="renovation_year1"
                                                   name="renovation_date"
                                                   class="input--radio__input sousDate"
                                                   value="with_date"
                                                   <?php if( !sess('renovation_date') || sess('renovation_date') == '' || sess('renovation_date') == "with_date") echo "checked" ?>
                                                   >
                                            <label for="renovation_year1"
                                                   class="input--radio__label">Date de la dernière rénovation / construction</label>
                                        </div>
                                    </div>
                                    <div class="last-renovation-year__row">
                                        <div class="field">
                                            <?php 
                                                $renovation = sess('renovation_date_nbr') && sess('renovation_date_nbr') != '' ? sess('renovation_date_nbr') : '';
                                            ?>
                                            <input type="number" placeholder="Année"
                                                   name="renovation_date_nbr"
                                                   class="enterDate input--text last-renovation-year__row__input"
                                                   value="<?= $renovation ?>" 
                                                   >
                                        </div>
                                    </div>
                                </div>
                                <div class="input--radio">
                                    <input type="radio" id="renovation_year2"
                                           name="renovation_date"
                                           class="input--radio__input sousDate" value="no_date"
                                           <?php if( sess('renovation_date') == "no_date") echo "checked" ?>
                                           >
                                    <label for="renovation_year2" class="input--radio__label">Je ne sais pas</label>
                                </div><!---->
                            </div>
                        </div>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="renove" name="batiment"
                               class="input--radio__input batiment" value="Rénové/Neuf">
                        <label for="renove" class="input--radio__label">Rénové/Neuf</label>
                    </div>
                    <div class="estimation-row__subfield renove" style="display: none;">
                        <div class="estimation-row__subfield__input estimation-row__subfield__input--full">
                            <div class="last-renovation-year">
                                <div class="last-renovation-year__field">
                                    <div class="last-renovation-year__row">
                                        <div class="input--radio">
                                            <input type="radio"
                                                   id="mf_last_renovation_year_label-114"
                                                   name="renew_date"
                                                   class="sousDate input--radio__input"
                                                   value="with_date"
                                                   <?php if( !sess('renew_date') || sess('renew_date') == '' || sess('renew_date') == "with_date") echo "checked" ?>
                                                   >
                                            <label for="mf_last_renovation_year_label-114"
                                                   class="input--radio__label">Date de la dernière rénovation / construction</label>
                                        </div>
                                    </div>
                                    <?php 
                                        $renovation = sess('renew_date_nbr') && sess('renew_date_nbr') != '' ? sess('renew_date_nbr') : '';
                                    ?>
                                    <div class="last-renovation-year__row">
                                        <div class="field">
                                            <input type="number" placeholder="Année"
                                                   name="renew_date_nbr"
                                                   class="enterDate input--text last-renovation-year__row__input"
                                                   value="<?= $renovation ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="input--radio">
                                    <input type="radio"
                                           id="mf_last_renovation_year_label_unknown-114"
                                           name="renew_date"
                                           class="sousDate input--radio__input" value="no_renew_date"
                                           <?php if( sess('renew_date') == "no_renew_date") echo "checked" ?>
                                           >
                                    <label for="mf_last_renovation_year_label_unknown-114"
                                           class="input--radio__label">Je ne sais pas</label>
                                </div><!---->
                            </div>
                        </div>
                    </div><!---->
                </div>
            </div>
        </div>
        <div class="item">
            <div class="estimation-row component-property-view">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>property-view.svg" alt="icon">
                    </div>
                    <label>De quelle vue bénéficie le bien&nbsp;?</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div role="group" class="component-input-group-radio field">
                        <div class="input--radio">
                            <input type="radio" name="beneficie" id="mycbid-118"
                                   class="input--radio__input" value="Vis-à-vis"
                                   <?php if( !sess('beneficie') || sess('beneficie') == '' || sess('beneficie') == "Vis-à-vis") echo "checked" ?>
                                   >
                            <label for="mycbid-118" class="input--radio__label">Vis-à-vis</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" name="beneficie" id="mycbid-119"
                                   class="input--radio__input" value="Vue dégagée"
                                   <?php if( sess('beneficie') == "Vue dégagée") echo "checked" ?>
                                   >
                            <label for="mycbid-119" class="input--radio__label">Vue dégagée</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" name="beneficie" id="mycbid-120"
                                   class="input--radio__input" value="Vue exceptionnelle"
                                   <?php if( sess('beneficie') == "Vue exceptionnelle") echo "checked" ?>
                                   >
                            <label for="mycbid-120" class="input--radio__label">Vue exceptionnelle</label>
                        </div>
                    </div>
                </div><!---->
            </div>
        </div>
        <div class="item">
            <div class="estimation-row component-standing">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>standing-house.svg" alt="icon">
                    </div>
                    <label>Quel est le standing du bien&nbsp;?</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div role="group" class="component-input-group-radio field">
                        <div class="input--radio">
                            <input type="radio" name="standing" id="mycbid-123"
                                   class="input--radio__input" value="Standard"
                                   <?php if( !sess('standing') || sess('standing') == '' || sess('standing') == "Standard") echo "checked" ?>
                                   >
                            <label for="mycbid-123" class="input--radio__label">Standard</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" name="standing" id="mycbid-124"
                                   class="input--radio__input" value="Supérieur"
                                   <?php if( sess('standing') == "Supérieur") echo "checked" ?>
                                   >
                            <label for="mycbid-124" class="input--radio__label">Supérieur</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" name="standing" id="mycbid-125"
                                   class="input--radio__input" value="Très haut de gamme"
                                   <?php if( sess('standing') == "Très haut de gamme") echo "checked" ?>
                                   >
                            <label for="mycbid-125" class="input--radio__label">Très haut de gamme</label>
                        </div>
                    </div>
                </div><!---->
            </div>
        </div>
    </div>
    <div class="tab tab3 current">
        <div class="estimation-form">
           <div class="titleStep text-left">
                <h3>Appréciation </h3>
            </div>
            <div class="item">
                <div class="estimation-row component-property-condition">
                    <div class="d-flex justify-content-start align-items-center">
                        <div class="estimation-row__icon">
                            <img src="<?= IMG_DIR ?>charme.svg" alt="icon">
                        </div>
                        <label>Charme/ Style Global</label>
                    </div>
                    <div class="estimation-row__input estimation-row__input--full-icon text-left">
                        <div class="input--radio">
                            <input type="radio" value="Aucun" id="aucun"
                                   name="charme" class="input--radio__input batiment restore"
                                   <?php if( !sess('charme') || sess('charme') == '' || sess('charme') == "Aucun") echo "checked" ?>
                                   >
                            <label for="aucun" class="input--radio__label">Aucun</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" id="un-peu" name="charme" value="Un peu"
                                   class="input--radio__input batiment"
                                   <?php if( sess('charme') == "Un peu") echo " checked" ?>
                                   >
                            <label for="un-peu" class="input--radio__label">Un peu</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" id="assez-bien" name="charme" value="Moyen"
                                   class="input--radio__input batiment"
                                   <?php if( sess('charme') == "Moyen") echo " checked" ?>
                                   >
                            <label for="assez-bien" class="input--radio__label">Moyen</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" id="beaucoup" name="charme"
                                   class="input--radio__input batiment" value="Beaucoup"
                                   <?php if( sess('charme') == "Beaucoup") echo " checked" ?>
                                   >
                            <label for="beaucoup" class="input--radio__label">Beaucoup</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" id="enormement" name="charme"
                                   class="input--radio__input batiment" value="Enormément"
                                   <?php if( sess('charme') == "Enormément") echo " checked" ?>
                                   >
                            <label for="enormement" class="input--radio__label">Enormément</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="estimation-row component-property-condition">
                    <div class="d-flex justify-content-start align-items-center">
                        <div class="estimation-row__icon">
                            <img src="<?= IMG_DIR ?>luminosite.svg" alt="icon">
                        </div>
                        <label>Luminosité</label>
                    </div>
                    <div class="estimation-row__input estimation-row__input--full-icon text-left">
                        <div class="input--radio">
                            <input type="radio" value="Très sombre" id="tres-sombre"
                                   name="luminosite" class="input--radio__input batiment restore"
                                   <?php if( !sess('luminosite') || sess('luminosite') == '' || sess('luminosite') == "Très sombre") echo "checked" ?>
                                   >
                            <label for="tres-sombre" class="input--radio__label">Trés sombre</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" id="sombre" name="luminosite" value="Sombre"
                                   class="input--radio__input batiment"
                                   <?php if( sess('luminosite') == "Sombre") echo " checked" ?>
                                   >
                            <label for="sombre" class="input--radio__label">Sombre</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" id="normal" name="luminosite" value="Normal"
                                   class="input--radio__input batiment"
                                   <?php if( sess('luminosite') == "Normal") echo " checked" ?>
                                   >
                            <label for="normal" class="input--radio__label">Normal</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" id="lumineux" name="luminosite"
                                   class="input--radio__input batiment" value="Lumineux"
                                   <?php if( sess('luminosite') == "Lumineux") echo " checked" ?>
                                   >
                            <label for="lumineux" class="input--radio__label">Lumineux</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" id="tres-lumineux" name="luminosite"
                                   class="input--radio__input batiment" value="Très Lumineux"
                                   <?php if( sess('luminosite') == "Très Lumineux") echo " checked" ?>
                                   >
                            <label for="tres-lumineux" class="input--radio__label">Très Lumineux</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="estimation-row component-property-condition">
                    <div class="d-flex justify-content-start align-items-center">
                        <div class="estimation-row__icon">
                            <img src="<?= IMG_DIR ?>vuedegage.svg" alt="icon">
                        </div>
                        <label>Vue dégagée</label>
                    </div>
                    <div class="estimation-row__input estimation-row__input--full-icon text-left">
                        <div class="input--radio">
                            <input type="radio" value="Aucune" id="aucune"
                                   name="vue_degagee" class="input--radio__input batiment restore"
                                   <?php if( !sess('vue_degagee') || sess('vue_degagee') == '' || sess('vue_degagee') == "Aucune") echo "checked" ?>
                                   >
                            <label for="aucune" class="input--radio__label">Aucune</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" id="partielle" name="vue_degagee" value="Partielle"
                                   class="input--radio__input batiment"
                                   <?php if( sess('vue_degagee') == "Partielle") echo " checked" ?>
                                   >
                            <label for="partielle" class="input--radio__label">Partielle</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" id="assez-belle" name="vue_degagee" value="Assez belle"
                                   class="input--radio__input batiment"
                                   <?php if( sess('vue_degagee') == "Assez belle") echo " checked" ?>
                                   >
                            <label for="assez-belle" class="input--radio__label">Assez belle</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" id="belle" name="vue_degagee"
                                   class="input--radio__input batiment" value="Belle"
                                   <?php if( sess('vue_degagee') == "Belle") echo " checked" ?>
                                   >
                            <label for="belle" class="input--radio__label">Belle</label>
                        </div>
                        <div class="input--radio">
                            <input type="radio" id="exceptionnelle" name="vue_degagee"
                                   class="input--radio__input batiment" value="Exceptionnelle"
                                   <?php if( sess('vue_degagee') == "Exceptionnelle") echo " checked" ?>
                                >
                            <label for="exceptionnelle" class="input--radio__label">Exceptionnelle</label>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>
<div class="blcBoutton btnForm clr btnForm float-right">
    <button type="button" id="prevBtn" class="precedent bouton btn misy_scrol">< étape
        précédente
    </button>
    <span class="sipan">
        <button type="submit" id="nextBtn" class="bouton btn misy_scrol">étape suivante ></button>
    </span>
</div>
<?php
    if( sess('batiment') == "En bon état"){
        echo '<script>setTimeout(function(){$("#bonEtat").trigger("click")},1500)</script>';
        if( sess('renovation_date_nbr') != ""){
            echo '<script>setTimeout(function(){$("#renovation_year1").trigger("click")},1700)</script>';            
        } 
    } 

    if( sess('batiment') == "Rénové/Neuf"){
        echo '<script>setTimeout(function(){$("#renove").trigger("click")},1500)</script>';
        if( sess('renew_date_nbr') != ""){
            echo '<script>setTimeout(function(){$("#renovation_year1").trigger("click")},1700)</script>';            
        } 
    } 
        
?>
<script>
    $('.batiment').change(function(){
        var etat = $('.etat');
        var renove = $('.renove');
        var i = $(this).attr("id");
        if (i=="bonEtat") {
            etat.fadeIn(200);
            renove.fadeOut(200);
        }
        else if(i=="renove") {
            renove.fadeIn(200);
            etat.fadeOut(200);
        }
        else if(i=="arenove") {
            renove.fadeOut(200);
            etat.fadeOut(200);
        }
        else {
            etat.fadeOut(200);
            renove.fadeOut(200);
        }
    });

    $('.sousDate').change(function(){
        var j = $(this).attr("value");
        if (j == "with_date") {
            $('.enterDate').prop("disabled", false);
        }
        else{
            $('.enterDate').prop("disabled", true).val('');
        }
    });

    $('.precedent').on('click', function(){
      var prev_step = $('[name=prev_step]').val();
      var curr_step = $('[name=curr_step]').val();
      var oldHtml = $('#form_content').html();
      $.ajax(ajaxurl, {
        type: 'POST',
        data: 'action=back_from_' + curr_step +'&show='+prev_step,
        dataType: 'html',
        success: function(resp){
          $('#form_content').html( resp);
          console.log(resp);
          // $('html, body').animate({
          // scrollTop: $(".top-step").offset().top
          // }, 1000)              
        }
      });

      return false;
  })
</script>