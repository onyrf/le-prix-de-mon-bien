var $ = jQuery.noConflict();

$( function(){

	jQuery.validator.addMethod("validTVA", function(value, element) {
	  return this.optional( element ) || /^BE\d{4}\.\d{3}\.\d{3}$/.test( value );
	}, 'Numéro de TVA Invalide');

	jQuery.validator.addMethod("ipi_nbr", function(value, element) {
	  return this.optional( element ) || /^\d{6}$/.test( value );
	}, 'Numéro IPI invalide');

	$('.woocommerce-EditAccountForm.edit-account').validate({
		rules:{
				account_first_name:{
					required: true
				},
				account_last_name:{
					required: true
				},
				account_display_name:{
					required: true
				},
				account_email:{
					required: true
				},
				invoice_name:{
					required: true
				},
				invoice_agence_name:{
					required: true
				},
				invoice_ipi_number:{
					required: true,
					ipi_nbr: true
				},
				invoice_adresse:{
					required: true
				},
				invoice_postal_code:{
					required: true
				},
				invoice_ville:{
					required: true
				},
				invoice_tva_number:{
					required: true,
					validTVA: true
				}
		},
		messages:{
				account_first_name:{
					required: "Veuillez remplir ce champ"
				},
				account_last_name:{
					required: "Veuillez remplir ce champ"
				},
				account_display_name:{
					required: "Veuillez remplir ce champ"
				},
				account_email:{
					required: "Veuillez remplir ce champ"
				},
				invoice_name:{
					required: "Veuillez remplir ce champ"
				},
				invoice_agence_name:{
					required: "Veuillez remplir ce champ"
				},
				invoice_ipi_number:{
					required: "Veuillez remplir ce champ"
				},
				invoice_adresse:{
					required: "Veuillez remplir ce champ"
				},
				invoice_postal_code:{
					required: "Veuillez remplir ce champ"
				},
				invoice_ville:{
					required: "Veuillez remplir ce champ"
				},
				invoice_tva_number:{
					required: "Veuillez remplir ce champ"
				}
		},
		submitHandler: function( form ){
			form.unbind('submit').submit();
		}
	});

	 $('.temps').startTimer();

	 // $('.buy_now').on('click',function(e){
	 // 	e.preventDefault();

	 // 	var propID = $(this).data('prop');

		// $.ajax(ajaxurl, {
		// 	type: "POST",
		// 	data: {
		// 		'action':'buy_now',
		// 		'propid':propID
		// 	},
		// 	dataType: "JSON",
		// 	success: function( resp ){
		// 		console.log( resp );
		// 	}
		// });
		
	 // });
});