var $ = jQuery.noConflict();

        $( function() {
            

            /***  slide images description ***/
             $('#slideImgDescri').slick({
                dots: false,
                infinite: true,
                autoplaySpeed: 4000,
                speed: 1000,
                slidesToShow: 1,
                slideToScroll: 1,
                arrows: true,
                autoplay: false,
                fade:true,
                pauseOnHover: false,
               
            });

            $(window).load(function(){
                var h_desc = $('.descri .textDescri').outerHeight();
                if (h_desc > 306){
                    $('.descri').addClass('minHeight');
                }
                else{
                     $('.descri').removeClass('minHeight');
                }

            });

             $('.descri .lire_la_suite').click(function () {
                $('.descri .textDescri').addClass('fullHeight');
                $(this).addClass('hide')
             })


            // $('.blc_chp_offre').slideUp();
            // $(document).mouseup(function(e) {
            //     var container = $(".plan_visite .col_right .content");
            //     if (!container.is(e.target) && container.has(e.target).length === 0)
            //     {
            //         // $('.blcTitreOffre').removeClass('hide');
            //         $('.plan_visite .col_right .content').removeClass('active');
            //         $('.blc_chp_offre').slideUp();
            //     }else{
            //         $('.blc_chp_offre').slideDown();
            //         // $('.blcTitreOffre').addClass('hide')
            //         $('.plan_visite .col_right .content').addClass('active');
            //     }
            // });

            // $('#faire_offre .blcOffre .bouton_close').click(function () {
            //     $('.blc_chp_offre').slideUp();
            // });

            // $('.plan_visite .col_right .content .bouton_close').click(function () {
            //     $('.blc_chp_offre').slideUp();
            //     // $('.blcTitreOffre').removeClass('hide');
            //     $('.plan_visite .col_right .content').removeClass('active');
            // });

            $('#cta_btn_offre').click(function () {
                $('.blc_chp_offre').slideDown();
                $('.plan_visite .col_right .content').addClass('active');

            })

            $('#faire_offre .bouton_close').click(function () {
                $('.blc_chp_offre').slideUp();
                $('.plan_visite .col_right .content').removeClass('active');
            })



            // DATE TIME PICKER

            $('#date1').datetimepicker({

                altField: "#hour_date1",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                dateFormat: 'dd/mm/yy',
                hourText: 'Heure',
                minuteText: 'Minute',
                secondText: 'Seconde',
                onSelect: function(){
                }
            });

            $('#date2').datetimepicker({
                altField: "#hour_date2",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                dateFormat: 'dd/mm/yy',
                onSelect: function(){
                }
            });

            $('#date3').datetimepicker({
                altField: "#hour_date3",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                dateFormat: 'dd/mm/yy',
                onSelect: function(){
                }
            });

            $('#date4').datetimepicker({
                altField: "#hour_date4",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                dateFormat: 'dd/mm/yy',
                onSelect: function(){
                }
            });

            $('#date5').datetimepicker({
                altField: "#hour_date5",
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                dateFormat: 'dd/mm/yy',
                onSelect: function(){
                }
            });

            $('.picto_date').on('click', function () {
                $('.picto_date').removeClass('active');
                $(this).addClass('active');
            })

            $('.plan_visite .listDate input').change(function(){
                
            });

            $('#date_offre').datetimepicker({
                format: 'd/m/y',
                minDate: 0,
                yearStart: 2020,
                disabledWeekDays: [0,6],
                dayOfWeekStart: 1,
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                dateFormat: 'dd/mm/yy',
                hourText: 'Heure',
                minuteText: 'Minute',
                secondText: 'Seconde',
                
            });

            $('#validite').datetimepicker({
                format: 'd/m/y',
                minDate: 0,
                yearStart: 2020,
                disabledWeekDays: [0,6],
                dayOfWeekStart: 1,
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                dateFormat: 'dd/mm/yy',
                hourText: 'Heure',
                minuteText: 'Minute',
                secondText: 'Seconde',
                
            });

            $('.blc_chp_radio_acompte .input_radio input').change(function(){
                if($('#paiement_acompte_oui').is(':checked')){ 
                    $('.blc_chp_radio_taux').show();
                }
                else{ 
                    $('.blc_chp_radio_taux').hide()
                }
            });

            $('.temps').startTimer();
        });

        // $(window).on( "load", function() {
        //     function startFancy(){
        //         $('.link_pp').click();
        //         $('.fancybox-bg').addClass('bg_pop')
        //     }
        //     startFancy()
        // });

        

    
        fileInit();
        fileInitOter();
        reset_file_upload();


    

        function fileInit() {
            $('.input-file').change(function () {
                iz = $(this)
                val = iz.val()
                par = iz.parent(".cont-file")
                txtExt = par.find("span")
                txtBroswer = par.find(".listBoutton .txtBroswer")
                var file = $(this)[0].files[0].name
                txtExt.text(file)
                txtBroswer.hide();
                var index = $(this).parents(".cont-file").index();
                var img = $(this).siblings(".imgPreview");
                img.attr('id', index);


                // iz.hide();
                // txtExt.hide();
                txtExt.addClass('show');
                txtExt.css('display','flex');

                imgPreview = iz.siblings('.content-preview').find('.imgPreview');
                imgPreview_parent = imgPreview.parents('.content-preview');
                imgPreview.addClass('shown');
                imgPreview_parent.addClass('shown');

            });
        }

        function fileInitOter() {
            $('.input-file').each(function () {
                $(this).change(function () {
                    $(this).parents(".cont-file").addClass('uploaded');
                    iz = $(this)
                    resetBtn = iz.siblings('.listBoutton').find('.reset');
                    imgPreview = iz.siblings('.content-preview').find('.imgPreview');
                    imgPreview_parent = imgPreview.parents('.content-preview');
                    imgPreview.addClass('shown');
                    imgPreview_parent.addClass('shown');
                    var file = $(this)[0].files[0].name
                    txtExt.text(file)
                    val = iz.val()
                    if (val != "") {
                        par = iz.parent(".content_cont_file")
                        txtExt = par.find("span")
                        txtBroswer = par.find(".listBoutton .txtBroswer")
                        par.siblings('.more').addClass('show')
                        txtExt.text(file)
                        txtBroswer.hide()
                        resetBtn.css('display','block')
                        resetBtn.text('Supprimer');
                        resetBtn.addClass('w_50');
                        par_chp = iz.parents('.w_50');
                        par_chp.addClass('full_width')

                        // iz.hide();
                        txtExt.css('display','flex');
                        txtExt.addClass('show');
                    }

                })
            })
        }

        function reset_file_upload() {
            $('.reset').each(function () {
                $('.reset').on('click', function () {
                    reset = $(this);
                    btnChanger = $(this).siblings('i');
                    inputFile = $(this).parents('.chp').find('.input-file');
                    fakeText = $(this).siblings('span');
                    inputFile.val('');
                    txtBroswer=reset.siblings('.txtBroswer');
                    txtBroswer.show();
                    btnChanger.text(txtBroswer.text());
                    par_chp = reset.parents('.w_50');
                    inputFile.show();
                    fakeText.show();

                    par_chp.removeClass('full_width')
                    fakeText.css('display','none')
                    fakeText.removeClass('show')
                    reset.removeClass('w_50')
                    reset.css('display','none')
                   
                    return false;
                });
            });
        }


        $(window).load(function(){
          $(".listOffre").mCustomScrollbar({
            theme: "inset-dark",
            scrollButtons: {enable:true}
          });

          $(".tableau_rdv").mCustomScrollbar({
            theme: "inset-dark",
            scrollButtons: {enable:true}
          });

          $(".inner_tableau").mCustomScrollbar({
            theme: "inset-dark",
            axis:"x",
            scrollButtons: {enable:true}
          });
        });

        $(".scroll").click(function() {
            var c = $(this).attr("href");
            $('html, body').animate({ scrollTop: $(c).offset().top }, 1000, "linear");
            return false;
        });

        jQuery('.lrm-ficon-register').on('click',function(){
            var r = jQuery('#registration_link').val();
            window.location.replace( r )    
            return false;
        });



        $(".galerie_detail").fancybox({
            'width':1300,
            'autoSize' : false,
           
            afterShow : function( instance, current ) {
                current.opts.$orig.closest(".slick-initialized").slick('slickGoTo', parseInt(current.index), true);
            }
        });

        /* Slide Test */
        $('.slideBien').slick({
            dots: false,
            infinite: true,
            autoplaySpeed: 4000,
            speed: 1000,
            slidesToShow: 1,
            slideToScroll: 1,
            arrows: true,
            autoplay: false,
            pauseOnHover: false,
            centerMode: true,
            centerPadding: '15%',
            responsive: [
                
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        centerMode:false
                    }
                }
            ]
        });

        // ============================================
        // Attach custom click event on cloned elements, 
        // trigger click event on corresponding link
        // ============================================
        $(document).on('click', '.slick-cloned', function(e) {
          var $slides = $(this)
          .parent()
          .children('.slick-slide:not(.slick-cloned)');

          $slides
            .eq( ( $(this).attr("data-slick-index") || 0) % $slides.length )
            .trigger("click.fb-start", { $trigger: $(this) });

          return false;
        });



