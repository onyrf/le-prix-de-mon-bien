jQuery(function($){
  // $("#APARTMENT, #MAISON").on('click',function() {
  //   $("#nextBtn").trigger('click');
  // });
jQuery.validator.setDefaults({
  debug: true,
  rules: {
    date: {
      required: true,
      date: false
    }
  }
});
  jQuery.validator.addMethod("phoneFormat", function(value, element) {
  // allow any non-whitespace characters as the host part
  return this.optional( element ) || /^[+]?\d{9,20}$/.test( value );
}, 'Entrez un numéro de téléphone valide svp.');

	// regle de validation
	$("#home_form").validate({
        rules: {          
          lname: {
          	required: true,
          	minlength: 1
          },
          fname: {
          	required: true,
          	minlength: 1
          },
          telephone: {
              required: true,
							minlength: 5,
              phoneFormat: true
			    },
			    email: {
              required: true,
              email: true,
          },
			    adresse: {
              required: true,
          },
			    numero: {
              required: true,
          },
			    postal: {
              required: true,
          },
			    ville: {
              required: true,
          },
			    property_type: {
              required: true,
          },
          fake_motiv: {
            required: true,
          },
          fake_proprio: {
            required: true,
          },
          fake_choix: {
            required: true,
          },
          acceptement: {
            required: true,
          },
          fake_objet: {
            required: true,
          }
        },
        // Specify validation error messages
        messages: {
          email: {
              required: "Veuillez compléter votre adresse e-mail",
              email: "Veuillez saisir un e-mail valide"
          },
          lname: {
          	required: "Veuillez entrer votre nom",
          	minlength: 1
          },
          fname: {
          	required: "Veuillez entrer votre prénom",
          	minlength: 1
          },
          telephone: {
              required: "Veuillez entrer votre numéro de téléphone",
              minlength: "Entrez au moins 5 chiffres",
              // number: "Entrez un numero de telephone valide"
			    },
			    adresse: {
              required: "Veuillez entrer l'adresse du bien",
          },
			    numero: {
              required: "Veuillez entrer le numéro",
          },
			    postal: {
              required: "Veuillez entrer le code postal",
          },
			    ville: {
              required: "Veuillez entrer la ville",
          },
			    property_type: {
              required: "Veuillez choisir : Appartement ou Maison",
          },
          fake_motiv: {
            required: "Veuillez préciser votre motivation à vendre le bien",
          },
          fake_proprio: {
            required: "Veuillez choisir: Propriétaire ou non",
          },
          fake_choix: {
            required: "Veuillez préciser si on peut vous appeler",
          },
          date: {
            required: "Veuillez sélectionner une date",
          },
          acceptement: {
            required: "Veuillez cocher cette case.",
          },
          fake_objet: {
            required: "Veuillez sélectionner l'objet de l'estimation",
          }
        },
        submitHandler: function(form){
          var curr_step = $('[name=curr_step]').val();
 
          var action = "handler_" + curr_step;
          var methodType = "POST";

          var data = new FormData( form );

          if( curr_step == '2' ){
            /* fichiers */
            var total_upload = $('input[type=file]').length;

            for( var i = 0; i < total_upload; i++){
             if ( $('.input-file')[i].value != "" ){
                 data.append('files['+i+']', $('.input-file')[i].files[0] );
             }
            }
          }

          var dataType = 'html';
          if( curr_step == '0' ){
            dataType = 'json';
          }
          if( curr_step == '9' ){
            $('.home.blockUI.blockOverlay').fadeIn('300');
            dataType = 'json';
          }

          data.append('action', action );

          $.ajax(ajaxurl, {
            type: methodType,
            processData: false,
            contentType: false,
            data: data,
            dataType: dataType,
            success: function(resp){
              if( curr_step != '9' && curr_step != '0' ){
                $('#form_content').html('');
                $('#form_content').html(resp);
                // $('html, body').animate({
                //   scrollTop: $(".top-step").offset().top - 200
                // }, 1000);

              }else if( curr_step == '9' ){
                console.log( curr_step );
                window.location.replace(resp.ck);

              }else if( curr_step == '0' ){
                $('#form_content').html('');
                $('#form_content').html(resp.step);
                // $('html, body').animate({
                //   scrollTop: $(".top-step").offset().top - 200
                // }, 1000);
                if( 'nbr_updated' == resp.msg ){
                  var suffix = ' DEMANDES';
                  if( resp.value < 2 ){
                    suffix = ' DEMANDE';
                  }
                  $('#span_1 p').text( resp.value + suffix);
                }
              }
                   
            }
          });
        }
	});

  $('#form-abus').validate({
    rules: {          
      sujet: {
        required: true,
        minlength: 1
      },
      prenom: {
        required: true,
        minlength: 1
      },
      nom: {
        required: true,
        minlength: 1
      },
      email: {
        required: true,
        email: true
      },
      telephone: {
        required: true,
        phoneFormat: true
      }
    },
    messages: {
      sujet: {
        required: "Veuillez choisir le sujet"
      },
      prenom: {
        required: "Veuillez entrer votre prénom",
      },
      nom: {
        required: "Veuillez entrer votre nom",
      },
      email: {
        required: "Veuillez compléter votre adresse e-mail",
        email: "Veuillez saisir un e-mail valide"
      },
      telephone: {
        required: "Veuillez entrer votre numéro de téléphone",
        phoneFormat: "Format de téléphone incorrect"
      }
    },
    submitHandler: function(form){
      $('.home.blockUI.blockOverlay').fadeIn('300');

      var data = new FormData( form );

      data.append('action','signalement_abus');

      $.ajax(ajaxurl, {
        type: 'POST',
        processData: false,
        contentType: false,
        data: data,
        dataType: 'html',
        success: function(resp){
          $('.home.blockUI.blockOverlay').fadeOut('300');
            $('.content').html('');
            $('.content').html('<p class="abus_success">Merci ! Votre message a bien été envoyé.');       
        }
      });

    } //end submit handler
  }); //end-valide

  /*
  ajout offre 
  */
  $('#form_offre').validate({
    rules: {          
      agence: {
        required: true
      },
      date: {
        required: true
      },
      validite: {
        required: true
      },
      prix: {
        required: true,
        number: true
      },
      condition_suspensive: {
        required: true
      },
      paiement_accompte: {
        required: true
      },
      file:{
        required: true
      },
      file_offre:{
        required: true
      }
    },
    messages: {
      agence: {
        required: "Veuillez entrer le nom de l'agence"
      },
      date: {
        required: "Veuillez entrer la date"
      },
      validite: {
        required: "Veuillez entrer le delai de validité"
      },
      prix: {
        required: "Veuillez entrer le prix",
        number: "Entrez un montant valide"
      },
      condition_suspensive: {
        required: "Requis"
      },
      paiement_accompte: {
        required: "Requis"
      },
      file:{
        required: "Requis"
      },
      file_offre:{
        required: "Requis"
      }
    },
    submitHandler: function(form){
      $('.home.blockUI.blockOverlay').fadeIn('300');

      var data = new FormData( form );

      data.append('action','faire_offre');

      /* fichiers */
      var total_upload = $('input[type=file]').length;

      for( var i = 0; i < total_upload; i++){
       if ( $('.input-file')[i].value != "" ){
           data.append('files['+i+']', $('.input-file')[i].files[0] );
       }
      }

      // data.append("file_cin", $('.input_file')[0].files[0] );
      // data.append("file_offre", $('.input_file_offre')[0].files[0] );

      $.ajax(ajaxurl, {
        type: 'POST',
        processData: false,
        contentType: false,
        data: data,
        dataType: 'html',
        success: function( row ){
          $('.blc_chp_offre').slideUp();
          $('.blcTitreOffre').removeClass('hide');
          $('.plan_visite .col_right .content').removeClass('active');
          if( $('#sans_offre:visible').length ){
            $('#sans_offre').hide();
          }
          $('#cta_btn_offre').text('FAIRE UNE OFFRE');
          $('.home.blockUI.blockOverlay').fadeOut('300');
          $('#mCSB_1_container').prepend( row );
          $('#plan_rdv').addClass('height_regle');
        }
      });

    } //end submit handler
  }); //end

  /* fin offre */

  /* Valider offre */
  $('.vadid_offre a').on('click', function(e){
    e.preventDefault();

    var a = $(this);

    $('.home.blockUI.blockOverlay').fadeIn('300');

    var offre_id = $(this).data('offre_id');
    var propID   = $(this).data('propid');

    var action = 'valid_offre';

    $.ajax(ajaxurl, {
      type: 'POST',
      dataType: 'json',
      data: "action=" + action + "&offre_id=" + offre_id + "&propID=" + propID,
      success: function( out ){        
        $('#popup_valide #titre_popup').text( out.titre );
        $('#popup_valide #content_popup').text( out.message );

        a.text('Validé');

        $('.home.blockUI.blockOverlay').fadeOut('300');

        $('.link-pp').trigger( "click" );

        setTimeout( function(){
          location.reload();
        }, 3000 );
      }
    });

  });
  /* Fin Valider offre */

  /*
  Achat fiche 
  */
  $('#submit-buy').validate({
    rules: {          
      agent_name: {
        required: true
      },
      paiement: {
        required: true
      }
    },
    messages: {
      agent_name: {
        required: "Veuillez entrer le nom de l'agence"
      },
      paiement: {
        required: "Veuillez choisir un mode de paiement"
      }
    },
    submitHandler: function(form){
      $('.home.blockUI.blockOverlay').fadeIn('300');

      var data = new FormData( form );

      data.append('action','buy_lead');

      var method = $('.paiement:checked').val();

      var url = $('#paiement_url').val();

      $.ajax(ajaxurl, {
        type: 'POST',
        processData: false,
        contentType: false,
        data: data,
        dataType: 'html',
        success: function(resp){
          $('.home.blockUI.blockOverlay').fadeOut('300');
          // console.log( resp );
          // window.location.href = resp;
          $('#popup_achat_lead').addClass( 'class_miavaka' );
          $('body').addClass('fozabe');
          $('.modal_achat_pour_abonnes #popup_achat_lead.class_miavaka .content').addClass('checkout_container');

          $('#popup_achat_lead .content').html('');            
          $('#popup_achat_lead .content').html( resp );

                   
        }
      });

    } //end submit handler
  }); //end
  /*
  Fin achat fiche
  */

}) // fin ready
