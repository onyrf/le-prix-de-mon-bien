<footer class="footer_coach">
    <div class="wrapper">
        <div class="content">
            <div class="top_footer">
                <a href="<?= site_url(); ?>" class="logo-foot-coach wow fadeInUp">
                   <img src="<?= IMG_DIR.'logo-wicoach.svg'; ?>">
                </a>
                <!-- <div class="blcCoord">
                    <div class="">
                        <b>Tél. : </b><a href="tel:065879887">065 87 98 87</a><br>
                        <b>Email : </b><a href="mailto:contact@wicoach.be">contact@wicoach.be</a>
                    </div>
                </div> -->
            </div>
            <div class="bottom_footer">
                © 2021 wicoach.be  <!-- -  <a href="#">CONDITIONS D'UTILISATIONS </a> -  <a href="#">CONDITIONS GÉNÉRALES</a>  -  <a href="#">CONFIDENTIALITÉ</a> -->
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
