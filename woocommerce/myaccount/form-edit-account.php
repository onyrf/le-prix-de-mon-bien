<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_edit_account_form' ); ?>

<h2 class="titre_detail_compte">Détails du compte</h2>
<p class="welcome"><b>Ajoutez un moyen de paiement qui sera utilisé lors de vos achats et du paiement de votre abonnement partenaire.</b></p>

<form class="woocommerce-EditAccountForm edit-account" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>

	<div class="bg_form">
		<div class="row_form">
			<div class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
				<label for="account_first_name"><?php esc_html_e( 'First name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr( $user->first_name ); ?>" required/>
			</div>
			<div class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
				<label for="account_last_name"><?php esc_html_e( 'Last name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr( $user->last_name ); ?>" required/>
			</div>
		</div>

		<div class="row_form">
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row">
				<label for="account_display_name"><?php esc_html_e( 'Display name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_display_name" id="account_display_name" value="<?php echo esc_attr( $user->display_name ); ?>" required/> <span><em><?php esc_html_e( 'Le nom qui apparaîtra dans la section relative à vos fiches immobilières.', 'woocommerce' ); ?></em></span>
			</div>
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="account_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
				<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr( $user->user_email ); ?>" />
			</div>
		</div>
	</div>
	
	<?php 
		$i_f_n = get_user_meta( $user->ID, 'billing_first_name', true );
		$i_l_n = get_user_meta( $user->ID, 'billing_last_name', true );
		$i_a_n = get_user_meta( $user->ID, 'invoice_agence_name', true );
		$i_i_n = get_user_meta( $user->ID, 'invoice_ipi_number', true );
		$i_a_1 = get_user_meta( $user->ID, 'billing_address_1', true );
		$i_p_c = get_user_meta( $user->ID, 'billing_postcode', true );
		$i_c_v = get_user_meta( $user->ID, 'billing_city', true );
		$i_t_n = get_user_meta( $user->ID, 'invoice_tva_number', true );
	?>
	<div class="bg_form bg_form2">
		<h3 class="titre_bg_form">Données de facturation</h3>
		<div class="row_form">
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row">
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="invoice_first_name" id="invoice_first_name" placeholder="Prénom*" value="<?= $i_f_n ?>" required/>
			</div>
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row">
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="invoice_name" id="invoice_name" placeholder="Nom*" value="<?= $i_l_n ?>" required/>
			</div>
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row">
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="invoice_agence_name" id="invoice_agence_name" placeholder="Nom de l'agence*" value="<?= $i_a_n ?>" required/>
			</div>
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row">
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="invoice_ipi_number" id="invoice_ipi_number" placeholder="Numéro I.P.I." value="<?= $i_i_n ?>" required/>
			</div>
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row w_100">
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="invoice_adresse" id="invoice_adresse" placeholder="Adresse *" value="<?= $i_a_1 ?>" required/>
			</div>
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row">
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="invoice_postal_code" id="invoice_postal_code" placeholder="Code Postal *" value="<?= $i_p_c ?>" required/>
			</div>
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row">
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="invoice_ville" id="invoice_ville" placeholder="Ville *" value="<?= $i_c_v ?>" required/>
			</div>
			<div class="woocommerce-form-row woocommerce-form-row--wide form-row w_100">
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="invoice_tva_number" id="invoice_tva_number" placeholder="Numéro de TVA" value="<?= $i_t_n ?>" required/>
			</div>
		</div>
	</div>


	<div class="bg_form bg_form2 bg_form_password">
		<fieldset>
			<legend class="titre_bg_form"><?php esc_html_e( 'Password change', 'woocommerce' ); ?></legend>
			<div class="row_form">
				<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="password_current"><?php esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" autocomplete="off" placeholder="....." />
				</div>
			</div>
			<div class="row_form">
				<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="password_1"><?php esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" autocomplete="off" placeholder="....." />
				</div>
				<div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="password_2"><?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" autocomplete="off" placeholder="....." />
				</div>
			</div>
		</fieldset>

		<?php do_action( 'woocommerce_edit_account_form' ); ?>

		<div>
			<?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
			<button type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'sauvegarder', 'woocommerce' ); ?></button>
			<input type="hidden" name="action" value="save_account_details" />
		</div>
	</div>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
