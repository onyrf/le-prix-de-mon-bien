<?php
/**
 * My Subscriptions section on the My Account page
 *
 * @author   Prospress
 * @category WooCommerce Subscriptions/Templates
 * @version  2.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$desabonne = false;

if (isset($_GET['order_id']) && isset($_GET['subscription'])){
	$order_id  = absint( $_GET['order_id'] );
	$order     = wc_get_order( $order_id );
	$subscription_id = $_GET['subscription'];
	$subscription = wcs_get_subscription( $subscription_id );
	
	//if ( function_exists( 'wcs_order_contains_subscription' ) && wcs_order_contains_subscription( $order ) ) {
		// The array of subscriptions dates
		$suscription_date_types = array('start', 'trial_end', 'next_payment', 'last_payment', 'end');
		$dates      = array(); // Initializing
		        
		foreach( $suscription_date_types as $date_type ) {
		    $dates[$date_type] = $subscription->get_date($date_type);
		}
		        
		// Set subscription end date based on subscription variation
		$dates['end'] = date( 'Y-m-d H:i:s',strtotime( "+12 months", strtotime( $dates['start'] ) ) );

		$subscription->update_dates($dates);
		if ($subscription->save()) {
			$reply = get_field('sender','option');
			$user = wp_get_current_user();
			$nom = $user->user_firstname ;			
			$prenom = $user->user_lastname ;
			$email = $user->user_email ;
            $destinataire = $email;
			
			$headers = array("Reply-To: $reply","Cc: $reply", 'Content-Type: text/html; charset=UTF-8');
			$obj = 'Désabonnement - ' . $order_id;

			ob_start();
			include INC_DIR . 'template_email/desabonnement.php';
			$body_mail = ob_get_clean();

            wp_mail( $destinataire, $obj, $body_mail, $headers );
            
		}
		$desabonne = true;
}

?>
<div class="woocommerce_account_subscriptions">

	<?php if ( ! empty( $subscriptions ) ) : ?>
	<table class="my_account_subscriptions my_account_orders woocommerce-orders-table woocommerce-MyAccount-subscriptions shop_table shop_table_responsive woocommerce-orders-table--subscriptions">

	<thead>
		<tr>
			<th class="subscription-id order-number woocommerce-orders-table__header woocommerce-orders-table__header-order-number woocommerce-orders-table__header-subscription-id"><span class="nobr"><?php esc_html_e( 'ID', 'woocommerce-subscriptions' ); ?></span></th>
			<th class="subscription-status order-status woocommerce-orders-table__header woocommerce-orders-table__header-order-status woocommerce-orders-table__header-subscription-status"><span class="nobr"><?php esc_html_e( 'Status', 'woocommerce-subscriptions' ); ?></span></th>
			<th class="subscription-next-payment order-date woocommerce-orders-table__header woocommerce-orders-table__header-order-date woocommerce-orders-table__header-subscription-next-payment"><span class="nobr"><?php echo 'Prochain prélèvement'; ?></span></th>
			<th class="subscription-total order-total woocommerce-orders-table__header woocommerce-orders-table__header-order-total woocommerce-orders-table__header-subscription-total"><span class="nobr"><?php echo esc_html_x( 'Total', 'table heading', 'woocommerce-subscriptions' ); ?></span></th>
			<th class="subscription-actions order-actions woocommerce-orders-table__header woocommerce-orders-table__header-order-actions woocommerce-orders-table__header-subscription-actions">&nbsp;</th>
		</tr>
	</thead>

	<tbody>
	<?php /** @var WC_Subscription $subscription */ ?>
	<?php foreach ( $subscriptions as $subscription_id => $subscription ) : ?>
		<tr class="order woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr( $subscription->get_status() ); ?>">
			<td class="subscription-id order-number woocommerce-orders-table__cell woocommerce-orders-table__cell-subscription-id woocommerce-orders-table__cell-order-number" data-title="<?php esc_attr_e( 'ID', 'woocommerce-subscriptions' ); ?>">
				<!-- <a href="<?php echo esc_url( $subscription->get_view_order_url() ); ?>"> -->
					<?php echo esc_html( sprintf( _x( '#%s', 'hash before order number', 'woocommerce-subscriptions' ), $subscription->get_order_number() ) ); ?>					
				<!-- </a> -->
				<?php do_action( 'woocommerce_my_subscriptions_after_subscription_id', $subscription ); ?>
			</td>
			<td class="subscription-status order-status woocommerce-orders-table__cell woocommerce-orders-table__cell-subscription-status woocommerce-orders-table__cell-order-status" data-title="<?php esc_attr_e( 'Status', 'woocommerce-subscriptions' ); ?>">
				<?php 
					if( 'Active' != esc_attr( wcs_get_subscription_status_name( $subscription->get_status() ) ) ){
						echo 'Annulé';
					}else{
						echo esc_attr( wcs_get_subscription_status_name( $subscription->get_status() ) );
					}
				?>
			</td>
			<td class="subscription-next-payment order-date woocommerce-orders-table__cell woocommerce-orders-table__cell-subscription-next-payment woocommerce-orders-table__cell-order-date" data-title="<?php echo esc_attr_x( 'Next Payment', 'table heading', 'woocommerce-subscriptions' ); ?>">
				<?php echo str_replace('In','Dans', $subscription->get_date_to_display( 'next_payment' ) );  ?>
				<!-- <?php if ( ! $subscription->is_manual() && $subscription->has_status( 'active' ) && $subscription->get_time( 'next_payment' ) > 0 ) : ?>
				<br/><small><?php echo esc_attr( $subscription->get_payment_method_to_display( 'customer' ) ); ?></small>
				<?php endif; ?> -->
			</td>
			<td class="subscription-total order-total woocommerce-orders-table__cell woocommerce-orders-table__cell-subscription-total woocommerce-orders-table__cell-order-total" data-title="<?php echo esc_attr_x( 'Total', 'Used in data attribute. Escaped', 'woocommerce-subscriptions' ); ?>">
				<?php 
					$output = wp_kses_post( $subscription->get_formatted_order_total() ); 
					echo str_replace(array('/ month','/ year'), array('/ mois','/ an'), $output);
				?>
			</td>
			<td class="subscription-actions order-actions woocommerce-orders-table__cell woocommerce-orders-table__cell-subscription-actions woocommerce-orders-table__cell-order-actions">
				<?php if( 'Active' == esc_attr( wcs_get_subscription_status_name( $subscription->get_status() ) ) ):
					$actions = wc_get_account_orders_actions( $subscription->get_order_number() );
					$url_self_recalc_subscription = '?order_id=' .$subscription->get_order_number() . '&subscription=' . $subscription->id;
				?>
				<?php if ( ! empty( $actions ) ) { ?>
						<div class="item_btn">
							<a href="<?= $actions['invoice']['url'] ?>" class="boutton_action print_order">Télécharger la facture</a>
						</div>
						<div class="item_btn">
							<? if($desabonne || $subscription->get_date( 'end' )): ?>
							<a href="<?= $url_self_recalc_subscription ?>" class="button boutton_action desabonner desabled">Désabonné</a>
						<? else: ?>
							<a href="<?= $url_self_recalc_subscription ?>" class="button boutton_action desabonner">Désabonner</a>
						<? endif; ?>
						</div>
					<?php } ?>
				<?php endif; ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>

	</table>
		<?php if ( 1 < $max_num_pages ) : ?>
			<div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">
			<?php if ( 1 !== $current_page ) : ?>
				<a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo esc_url( wc_get_endpoint_url( 'subscriptions', $current_page - 1 ) ); ?>"><?php esc_html_e( 'Previous', 'woocommerce-subscriptions' ); ?></a>
			<?php endif; ?>

			<?php if ( intval( $max_num_pages ) !== $current_page ) : ?>
				<a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo esc_url( wc_get_endpoint_url( 'subscriptions', $current_page + 1 ) ); ?>"><?php esc_html_e( 'Next', 'woocommerce-subscriptions' ); ?></a>
			<?php endif; ?>
			</div>
		<?php endif; ?>
	<?php else : ?>
		<p class="no_subscriptions woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info">
			<?php if ( 1 < $current_page ) :
				printf( esc_html__( 'You have reached the end of subscriptions. Go to the %sfirst page%s.', 'woocommerce-subscriptions' ), '<a href="' . esc_url( wc_get_endpoint_url( 'subscriptions', 1 ) ) . '">', '</a>' );
			else :
				esc_html_e( 'Vous n\'avez aucun abonnement actif ' );
				?>
				<a class="woocommerce-Button button" href="<?php echo site_url('partenaires') ?>">
					<?php esc_html_e( 'Voir les packs' ); ?>
				</a>
			<?php
		endif; ?>
		</p>

	<?php endif; ?>

</div>
