<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$allowed_html = array(
	'a' => array(
		'href' => array(),
	),
	'br'
);
?>

<p>
	<?php
	$user_meta = get_user_meta( $current_user->ID );

	printf(
		/* translators: 1: user display name 2: logout url */
		wp_kses( __( 'Bonjour %1$s %2$s', 'woocommerce' ), $allowed_html ),
		esc_html( ucfirst( $user_meta['first_name'][0] ) ),
		esc_html( ucfirst( $user_meta['last_name'][0] ) ),
		esc_url( wc_logout_url() )
	);

	?>
</p>

<!-- <p> -->
	<?php
	/* translators: 1: Orders URL 2: Address URL 3: Account URL. */
	// $dashboard_desc = __( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">billing address</a>, and <a href="%3$s">edit your password and account details</a>.', 'woocommerce' );
	// if ( wc_shipping_enabled() ) {
	// 	 translators: 1: Orders URL 2: Addresses URL 3: Account URL. 
	// 	$dashboard_desc = __( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">shipping and billing addresses</a>, and <a href="%3$s">edit your password and account details</a>.', 'woocommerce' );
	// }
	// $dashboard_desc = '<p>Bienvenue sur votre tableau de bord</p>';
	// $dashboard_desc .= '<p>Vous pouvez <a href="%1$s">accéder</a> aux fiches que vous avez acheté.</p>';
	// $dashboard_desc .= '<p>Vous pouvez <a href="%2$s">éditer votre profil</a></p>';
	// $dashboard_desc .= '<p>Si vous êtes nouveau, veuillez <a href="%3$s">réinitiliser votre mot de passe</a></p>';
	$dashboard_desc .= '<p class="welcome"><b>Bienvenue sur votre tableau de bord Partenaire.</p>';
	printf( 
		$dashboard_desc, 
		esc_url( wc_get_endpoint_url( 'orders' ) ), 
		esc_url( wc_get_endpoint_url( 'edit-account' ) ),
		esc_url( wp_lostpassword_url() )
	);
	?>
<!-- </p> -->
	<div class="list_link_dashboard">
		<div class="item">
			<div class="inner_item">
				<div class="content">
					<div class="top_link">
						<div class="picto">
							<img src="<?= IMG_DIR.'file-earmark-medical2.svg' ?>">
						</div>
						<div class="title">MES FICHES</div>
					</div>
					<p>Toutes vos fiches immobilières<br> que vous avez achetée.</p>
					<a href="<?= esc_url( wc_get_endpoint_url( 'orders' ) )  ?>" class="link_dashboard">voir mes fiches</a>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="inner_item">
				<div class="content">
					<div class="top_link">
						<div class="picto">
							<img src="<?= IMG_DIR.'identification2.svg' ?>">
						</div>
						<div class="title">détails du compte</div>
					</div>
					<p>Consultez les informations<br> de votre compte personnel.</p>
					<a href="<?= esc_url( wc_get_endpoint_url( 'edit-account' ) ) ?>" class="link_dashboard">voir mes détails</a>
				</div>
			</div>
		</div>
		<div class="item moyen_paiement">
			<div class="inner_item">
				<div class="content">
					<div class="top_link">
						<div class="picto">
							<img src="<?= IMG_DIR.'card-outline2.svg' ?>">
						</div>
						<div class="title">mes moyens de paiements</div>
					</div>
					<p>Ajoutez ou supprimez<br> un moyen de paiement.</p>
					<a href="<?= esc_url( wc_get_endpoint_url( 'payment-methods' ) ) ?>" class="link_dashboard">modifier</a>
				</div>
			</div>
		</div>
	</div>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	// do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
