<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="woocommerce-order">

	<?php
	if ( $order ) :
		$items = $order->get_items();

		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>
		<?php if ( $order->has_status( 'failed' ) ) : ?>
			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>
		<?php else : ?>
			<?php foreach( $items as $item ){
				$product = $item->get_product();
			}

			$product_id = $product->get_id();
			if( $product_id == 176 || $product_id == 360 ){
					
					//send estimation
		        $str = http_build_query( $_SESSION );
		        parse_str($str, $Data);

		        extract($Data);
		        $prixvente = $envisage_vendre_bien;
		        $ret = [];

		        $destinataire = $email;

		        $sender = get_field('sender','option');

		        $subject = "Estimation de mon bien";    

		        $atts = array();
		        $atts = json_decode( $attachment );
		        $influence = json_decode( $influence );
		       
		        $pour_client = true;
		        ob_start();
		            include INC_DIR .'template_email/email_estimation.php';
		        $body_mail = ob_get_clean();

		        $headers = array("Reply-To : $sender", "Cc: $sender",'Content-Type: text/html; charset=UTF-8');


		        if(@wp_mail( $destinataire, $subject, $body_mail, $headers, $atts )){
		            // $attachments[] = $pdf; 
		            // $headers[] = 'Cc: '.$sender;
		            $pour_client = false;
		            ob_start();
		                include INC_DIR .'template_email/email_estimation.php';
		            $body_mail = ob_get_clean();
		            @wp_mail( $sender, $subject, $body_mail, $headers, $atts );

		            turn_lead_into_client( $email );
		        }


			}elseif( $product_id == 364 || $product_id == 374 ) {
		?>
			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">
				<b>Votre paiement pour l'acquisition du book a bien été effectué.</b>
				<br>
				Vendezmonbien.be vous remercie de votre confiance.
			</p>
			<?php 
			}elseif($product_id == ID_ABO || $product_id == ID_ABO_AN ){
					echo "Veuillez patienter...";
					
			}elseif( 'property' == get_post_type( get_current_product_fiche($product_id) ) ){
				echo "Veuillez patienter...";

				$propID = get_current_product_fiche( $product_id );

				$ref = get_field('reference', $propID );
				$obj = "REF $ref : un accès a été acheté !";

				$sender = get_field('sender','option');
				$julie = get_field('julie','option');

				ob_start();
		            include INC_DIR .'template_email/fiche_achetee.php';
		        $body_mail = ob_get_clean();

		        $headers = array("Reply-To : $sender", "Cc: $sender","Cc: $julie",'Content-Type: text/html; charset=UTF-8');

		        $partners_ = get_all_partners();

		        $partners = array();

		        foreach ($partners_ as $p ) {
		        	if( $p->user_email != wp_get_current_user()->user_email ){
		        		$partners[] = $p->user_email;
		        	}
		        }

		        @wp_mail( $partners, $obj, $body_mail, $headers );

		        $obj = "REF $ref : Félicitation pour votre achat !";

		        ob_start();
		            include INC_DIR .'template_email/fiche_achetee_notif_acheteur.php';
		        $body_mail = ob_get_clean();

		        $current_agent = wp_get_current_user();

		        @wp_mail( $current_agent->user_email, $obj, $body_mail, $headers );

		        /** refund les autres **/

		        if( isset( $_SESSION['buy_now'] ) && $_SESSION['buy_now'] ){

				        $orders = get_orders_ids_by_product_id( $product_id );

						    $reason = "Ref $ref : Accès exclusif acheté";

						    if( is_array( $orders ) && count( $orders ) ){
						      foreach( $orders as $order_id ){
						      	if( $order_id == $order->ID ){
						      		continue;
						      	}
						        @vmb_wc_refund_order( $order_id, $reason );
						      }
						    }

						    update_post_meta( $propID, 'was_bought_now', true );

						    unset( $_SESSION['buy_now'] );

				  	}

				    /** fin refund **/

			}
		?>

		<?php endif; ?>
		<?php //WC()->mailer()->emails['WC_Email_Customer_Invoice']->trigger($order->ID); ?>
	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>
	<!-- <div class="btnTransform">
        <a href="<?= site_url(); ?>" class="btn">retour à l'accueil</a>                                                                        
    </div> -->

</div>
