<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
// if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
// 	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
// 	return;
// }

?>

<?php
		foreach( WC()->cart->get_cart() as $cart_item ){
		    $product_id = $cart_item['product_id'];
		}
	?>

<form name="checkout" method="post" class="checkout woocommerce-checkout<?php if( 
				$product_id == ID_ABO 
				|| $product_id == ID_ABO_AN || 
				'property' == get_post_type( get_current_product_fiche($product_id) ) 
			) echo ' produit_special' ?>" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>

			<!-- bloc vaovao : afaka atsofoka atsy @ io col-1 io ra ilaina, eo ambonin'lé do_action( ) -->
			<?php if( 
				$product_id == ID_ABO 
				|| $product_id == ID_ABO_AN || 
				'property' == get_post_type( get_current_product_fiche($product_id) ) 
			) : 
				$user = wp_get_current_user();

				$i_a_n = get_user_meta( $user->ID, 'invoice_agence_name', true );
				$i_i_n = get_user_meta( $user->ID, 'invoice_ipi_number', true );
				$i_t_n = get_user_meta( $user->ID, 'invoice_tva_number', true );
			?>
					<div class="new-col">
						<div class="woocommerce-billing-fields__field-wrapper">
							<p class="form-row form-row-wide" id="ipi_number_field">
								<label for="ipi_number" class=""></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="ipi_number" id="ipi_number" placeholder="Numero IPI" value="<?= $i_i_n ?>" autocomplete="Numero ipi"></span>
							</p>
							<p class="form-row form-row-wide" id="postal_code_field">
								<label for="nom_agence" class=""></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="nom_agence" id="nom_agence" placeholder="Nom de l'agence" value="<?= $i_a_n ?>" autocomplete="Nom de l'agence"></span>
							</p>
							<p class="form-row form-row-wide w-100" id="tva_field">
								<label for="tva" class=""></label><span class="woocommerce-input-wrapper"><input type="text" class="input-text " name="num_tva" id="tva" placeholder="Numéro de TVA" value="<?= $i_t_n ?>" autocomplete="Numéro de TVA"></span>
							</p>
						</div>
					</div>
			<?php endif; ?>
			<!-- fin bloc vaovao -->

		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>
	<!-- zebre -->
	<div id="zebre_wrap">
		<?php if( (isset( $_GET['pay'] ) && !empty( $_GET['pay'] ) && strip_tags( $_GET['pay'])  == 'full') || $product_id != 176  ): ?>
		<?php else: ?>
		<!-- <div id="zebre">
			<p class="form-row form-row-first">
				<input type="text" id="fake_coupon_code" class="input-text" placeholder="Insérer un code promo" id="coupon_code" value="" />
			</p>

			<p class="form-row form-row-last">
				<a href="#" class="button" id="fake_valid_coupon"><?php esc_html_e( 'Apply coupon', 'woocommerce' ); ?></a>
			</p>

			<div class="clear"></div>
		</div> -->
	<?php endif; ?>
		<!-- fin zebre -->
		<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
		
		<div id="apres_zebre">
			
			<h3 id="order_review_heading">
				<?php echo 'DÉTAIL DE LA COMMANDE'; ?>
			</h3>		
			
			<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

			<div id="order_review" class="woocommerce-checkout-review-order <?php if( $product_id != 176 ): echo 'non_barre'; else: echo 'barre'; endif; ?>">
				<?php do_action( 'woocommerce_checkout_order_review' ); ?>
			<!-- </div> -->
		</div>
	</div>
	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
