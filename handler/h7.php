<?php
add_action('wp_ajax_handler_7', 'handler_7');
add_action('wp_ajax_nopriv_handler_7', 'handler_7');

function handler_7(){
	if(isset($_POST) && !empty($_POST)){
		$str = http_build_query($_POST);
    parse_str($str, $Data);

    extract($Data);

    $current_user = get_unfinished_lead_expertise( sess('email') );
    $id = $current_user->id;

    update_lead_step_7(  $id,$orientation,$influence  );

    save_session_step_7( $orientation,$influence );

    ob_start();
			include ASTRA_THEME_CHILD_DIR . 'steps/s8.php';
		$out = ob_get_clean();

		echo $out;
		wp_die();
	}
}

function update_lead_step_7( $id,$orientation,$influence ){
	global $wpdb;

	$lead_expertise = $wpdb->prefix.'lead_expertise';

  $new_data = array(
    'derniere_etape' => 7,
    'derniere_expertise' => date("d-m-Y H:i:s"),
    'orientation'     => $orientation,
    "influence"     => json_encode( $influence ),
    );

    $update = $wpdb->update( 
        $lead_expertise,
        $new_data,
        array(
            'id' => $id
        ),
    );

    if( $update ){
        return true;
    }else{
        return false;
    }

}

function save_session_step_7( $orientation,$influence ){
	$_SESSION['orientation'] = $orientation;
  $_SESSION['influence'] = json_encode( $influence );

	return true;
}

add_action('wp_ajax_back_from_7', 'back_from_7');
add_action('wp_ajax_nopriv_back_from_7', 'back_from_7');

function back_from_7(){
  if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);

      ob_start();
        include ASTRA_THEME_CHILD_DIR . 'steps/s'. $show .'.php';
      $out = ob_get_clean();

      echo $out;
      wp_die();

    }
}