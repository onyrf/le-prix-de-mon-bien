<?php
add_action('wp_ajax_handler_6', 'handler_6');
add_action('wp_ajax_nopriv_handler_6', 'handler_6');

function handler_6(){
	if(isset($_POST) && !empty($_POST)){
		$str = http_build_query($_POST);
    parse_str($str, $Data);

    extract($Data);

    $current_user = get_unfinished_lead_expertise( sess('email') );
    $id = $current_user->id;

    update_lead_step_6(  $id,$nuisance,$vis_a_vis,$infiltration,$environnement );

    save_session_step_6( $nuisance,$vis_a_vis,$infiltration,$environnement );

    ob_start();
			include ASTRA_THEME_CHILD_DIR . 'steps/s7.php';
		$out = ob_get_clean();

		echo $out;
		wp_die();
	}
}

function update_lead_step_6( $id,$nuisance,$vis_a_vis,$infiltration,$environnement ){
	global $wpdb;

	$lead_expertise = $wpdb->prefix.'lead_expertise';

  $new_data = array(
    'derniere_etape' => 6,
    'derniere_expertise' => date("d-m-Y H:i:s"),
    'nuisance'    => $nuisance,
    "vis_a_vis"     => $vis_a_vis,
    "infiltration"      => $infiltration,
    "environnement"      => $environnement,
    );

    $update = $wpdb->update( 
        $lead_expertise,
        $new_data,
        array(
            'id' => $id
        ),
    );

    if( $update ){
        return true;
    }else{
        return false;
    }

}

function save_session_step_6( $nuisance,$vis_a_vis,$infiltration,$environnement ){
	$_SESSION['nuisance']       = $nuisance;
  $_SESSION['vis_a_vis'] = $vis_a_vis;
  $_SESSION['infiltration'] = $infiltration;
  $_SESSION['environnement'] = $environnement;

	return true;
}

add_action('wp_ajax_back_from_6', 'back_from_6');
add_action('wp_ajax_nopriv_back_from_6', 'back_from_6');

function back_from_6(){
  if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);

      ob_start();
        include ASTRA_THEME_CHILD_DIR . 'steps/s'. $show .'.php';
      $out = ob_get_clean();

      echo $out;
      wp_die();

    }
}
