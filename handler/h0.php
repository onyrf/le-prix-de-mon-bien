<?php

add_action('wp_ajax_handler_0', 'handler_0');
add_action('wp_ajax_nopriv_handler_0', 'handler_0');

function handler_0(){
	if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);
      $mode = 'reduit';
      if( isset( $_SESSION['mode'] ) && $_SESSION['mode'] == "plein" ){
        $mode = 'plein';
      }elseif( ( isset( $_SESSION['mode'] ) && $_SESSION['mode'] == "reduit" ) || !isset( $_SESSION['mode'] ) ) {
        $mode = 'reduit';
      }
      // stockéna en session izay nampidiriny
      $sess_id = save_session_step_0( $lname,$fname,$telephone,$email,$adresse,$numero,$postal,$ville,$property_type );

      $current_lead_expertise = get_unfinished_lead_expertise( $email );
      // ra tsy mbola ao d répertoriéna zany
      if( false === $current_lead_expertise ){      	
        $lead_id = save_lead_step_0( $lname,$fname,$telephone,$email,$adresse,$numero,$postal,$ville,$property_type, $mode );
          $show = 1;

          /* nombre de demande */
          $old_nbr = (int) $current_nbr_demande;
          $old_nbr++;

          update_field( 'total_nbr_demande', $old_nbr, get_option('page_on_front') );
          $out['msg'] = 'nbr_updated';
          $out['value'] = $old_nbr;
          /* fin nombre demande */

      }else{ // ra efa tao d updatena ny date de dernière demande dexpertise
      	init_all_session_variables( $email );
        $lead_id = update_lead_step_0( $current_lead_expertise->id,$lname,$fname,$telephone,$adresse,$numero,$postal,$ville,$property_type,$mode ); 
        $last = $current_lead_expertise->derniere_etape; 
        if( $last == '0'){
          $show = 1;  
        }else{
          $show = (int)$last; 
        }
      }
         
      
      ob_start();
				include ASTRA_THEME_CHILD_DIR . 'steps/s'.$show.'.php';
			$out['step'] = ob_get_clean();

			echo json_encode( $out );
			wp_die();
        
  }  
}

function save_lead_step_0( $lname,$fname,$telephone,$email,$adresse,$numero,$postal,$ville,$property_type, $mode ){
	global $wpdb;

  $lead_expertise = $wpdb->prefix.'lead_expertise';

  $wpdb->insert($lead_expertise, array(
  	"email" 		 		=> $email,
  	'derniere_expertise' => date("d-m-Y H:i:s"),
  	"derniere_etape"=> 0,
  	'mode' 					=> $mode,
    'lname'    			=> $lname,
    'fname'    			=> $fname,
    'telephone' 	  => $telephone,    
    "adresse" 		 	=> $adresse,
    "numero" 		 		=> $numero,
    "postal" 				=> $postal,
    "ville" 				=> $ville,
    "property_type" => $property_type,    
  ));

  return $wpdb->insert_id;	
}

function update_lead_step_0( $id,$lname,$fname,$telephone,$adresse,$numero,$postal,$ville,$property_type, $mode ){
    global $wpdb;

  $lead_expertise = $wpdb->prefix.'lead_expertise';

  $new_data = array(
        'derniere_expertise' => date("d-m-Y H:i:s"),
    "derniere_etape"=> $_SESSION['derniere_etape'],
    'mode'                  => $mode,
    'lname'             => $lname,
    'fname'             => $fname,
    'telephone'       => $telephone,    
    "adresse"           => $adresse,
    "numero"                => $numero,
    "postal"                => $postal,
    "ville"                 => $ville,
    "property_type" => $property_type,
    );

    $update = $wpdb->update( 
        $lead_expertise,
        $new_data,
        array(
            'id' => $id
        ),
    );

    if( $update ){
        return true;
    }else{
        return false;
    }   
}

function save_session_step_0( $lname,$fname,$telephone,$email,$adresse,$numero,$postal,$ville,$property_type){
	$_SESSION['lname']				 = $lname;
	$_SESSION['fname']				 = $fname;
	$_SESSION['telephone']		 = $telephone;
	$_SESSION['adresse'] 			 = $adresse;
	$_SESSION['email'] 				 = $email;
	$_SESSION['numero'] 			 = $numero;
	$_SESSION['postal'] 			 = $postal;
	$_SESSION['ville'] 				 = $ville;
	$_SESSION['property_type'] = $property_type;

	return true;
}

function sess( $str ){
	if( isset( $_SESSION[$str] ) ){
		return $_SESSION[$str];
	}
}

function init_all_session_variables( $email ){
  $info = get_unfinished_lead_expertise( $email );
  
  $_SESSION['derniere_etape']= $info->derniere_etape;
  $_SESSION['lname']         = $info->lname;
  $_SESSION['fname']         = $info->fname;
  $_SESSION['telephone']     = $info->telephone;
  $_SESSION['adresse']       = $info->adresse;
  $_SESSION['email']         = $info->email;
  $_SESSION['numero']        = $info->numero;
  $_SESSION['postal']        = $info->postal;
  $_SESSION['ville']         = $info->ville;
  $_SESSION['property_type'] = $info->property_type;
  
  $_SESSION['HabitableSurface']                                          = $info->HabitableSurface;
  $_SESSION['classified_property_livingDescription_netHabitableSurface'] = $info->classified_property_livingDescription_netHabitableSurface;
  $_SESSION['chambre_nbr']                                               = $info->chambre_nbr;
  $_SESSION['facade_nbr']                                                = $info->facade_nbr;
  $_SESSION['sdb_nbr']                                                   = $info->sdb_nbr;
  $_SESSION['garden_exist']                                              = $info->garden_exist;
  $_SESSION['gardenSurface']                                             = $info->gardenSurface;
  $_SESSION['gardenSurfaceOutput']                                       = $info->gardenSurfaceOutput;
  $_SESSION['terrasse']                                                  = $info->terrasse;
  $_SESSION['terraseSurface']                                            = $info->terraseSurface;
  $_SESSION['surfaceTerrasse']                                           = $info->surfaceTerrasse;
  $_SESSION['grenier']                                                   = $info->grenier;
  $_SESSION['grenierSuperficie']                                         = $info->grenierSuperficie;
  $_SESSION['grenierSurface']                                            = $info->grenierSurface;
  $_SESSION['charge']                                                    = $info->charge;
  $_SESSION['construct_year']    = $info->construct_year;
  $_SESSION['AnneSlide']         = $info->AnneSlide;
  $_SESSION['floor_batiment_nbr']= $info->floor_batiment_nbr;
  $_SESSION['floor_batiment']    = $info->floor_batiment;
  $_SESSION['cave']              = $info->cave;
  $_SESSION['parking']           = $info->parking;
  $_SESSION['parking_exterieur'] = $info->parking_exterieur;
  $_SESSION['parking_interieur'] = $info->parking_interieur;
  $_SESSION['batiment']        = $info->batiment;
  $_SESSION['renovation_date'] = $info->renovation_date;
  $_SESSION['renovation_date_nbr'] = $info->renovation_date_nbr;
  $_SESSION['renew_date']     = $info->renew_date;
  $_SESSION['renew_date_nbr'] = $info->renew_date_nbr;
  $_SESSION['beneficie'] = $info->beneficie;
  $_SESSION['standing'] = $info->standing;
  $_SESSION['toiture']       = $info->toiture;
  $_SESSION['chassis'] = $info->chassis;
  $_SESSION['chauffage'] = $info->chauffage;
  $_SESSION['electricite']    = $info->electricite;
  $_SESSION['cuisine']       = $info->cuisine;
  $_SESSION['sanitaire'] = $info->sanitaire;
  $_SESSION['revetement_sol'] = $info->revetement_sol;
  $_SESSION['peinture']     = $info->peinture;
  $_SESSION['charme']        = $info->charme;
  $_SESSION['luminosite'] = $info->luminosite;
  $_SESSION['vue_degagee'] = $info->vue_degagee;
  $_SESSION['nuisance']        = $info->nuisance;
  $_SESSION['vis_a_vis'] = $info->vis_a_vis;
  $_SESSION['infiltration'] = $info->infiltration;
  $_SESSION['environnement'] = $info->environnement;
  $_SESSION['orientation'] = $info->orientation;
  $_SESSION['influence'] = $info->influence;
  $_SESSION['attachment'] = $info->attachment;
  $_SESSION['proprietaire_bien'] = $info->proprietaire_bien;
  $_SESSION['envisage_vendre_bien'] = $info->envisage_vendre_bien;
  $_SESSION['informe_evolution_marche'] = $info->informe_evolution_marche;
  $_SESSION['informe_achat_bien'] = $info->informe_achat_bien;
  $_SESSION['motiv'] = $info->motiv;
  $_SESSION['acceptement'] = $info->acceptement;
  $_SESSION['obje_estim'] = $info->obje_estim;
  $_SESSION['rgpd'] = $info->rgpd;

  $_SESSION['select_call'] = $info->select_call;
  $_SESSION['date'] = $info->date;
}

function prepare_checkout(){
  global $woocommerce;
  $mode_lead = $_SESSION['mode'];
  if( $_SESSION['mode'] == 'plein' ){
      $prod_id = 360;
  }elseif( $_SESSION['mode'] == 'reduit' || !isset( $_SESSION['mode'] ) ) {
      $prod_id = 176;
  }
  $woocommerce->cart->empty_cart();

  $woocommerce->cart->add_to_cart( $prod_id, 1 );
}

