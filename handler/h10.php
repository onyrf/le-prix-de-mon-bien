<?php
add_action('wp_ajax_handler_10', 'handler_10');
add_action('wp_ajax_nopriv_handler_10', 'handler_10');

function handler_10(){
	if(isset($_POST) && !empty($_POST)){
		$str = http_build_query($_POST);
    parse_str($str, $Data);

    extract($Data);

    $current_user = get_unfinished_lead_expertise( sess('email') );
    $id = $current_user->id;

    if( $select_call == "non" ){
      $date = '';
    }
    update_lead_step_10( $id,$proprietaire_bien,$prixvente,$informe_evolution_marche, $informe_achat_bien,$rgpd,$motiv,$obje_estim,$select_call,$date,$acceptement );

    save_session_step_10( $proprietaire_bien,$prixvente,$informe_evolution_marche, $informe_achat_bien,$rgpd,$motiv,$obje_estim,$select_call,$date,$acceptement );

    $ret = array();
    prepare_checkout();
    $ret['ck'] = site_url('/paiement');

		echo json_encode($ret);
		wp_die();
	}
}

function update_lead_step_10( $id,$proprietaire_bien,$prixvente,$informe_evolution_marche, $informe_achat_bien,$rgpd,$motiv,$obje_estim, $select_call,$date,$acceptement ){
	global $wpdb;

	$lead_expertise = $wpdb->prefix.'lead_expertise';

  $new_data = array(
    'derniere_etape'            => 10,
    'derniere_expertise' => date("d-m-Y H:i:s"),
    
    
    
    
    
    );

    $update = $wpdb->update( 
        $lead_expertise,
        $new_data,
        array(
            'id' => $id
        ),
    );

    if( $update ){
        return true;
    }else{
        return false;
    }

}

function save_session_step_10( $proprietaire_bien,$prixvente,$informe_evolution_marche, $informe_achat_bien,$rgpd,$motiv, $obje_estim,$select_call,$date,$acceptement ){
	
  
	return true;
}

add_action('wp_ajax_back_from_10', 'back_from_10');
add_action('wp_ajax_nopriv_back_from_10', 'back_from_10');

function back_from_10(){
  if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);

      ob_start();
        include ASTRA_THEME_CHILD_DIR . 'steps/s'. $show .'.php';
      $out = ob_get_clean();

      echo $out;
      wp_die();

    }
}