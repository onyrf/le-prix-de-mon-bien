<?php
add_action('wp_ajax_handler_1', 'handler_1');
add_action('wp_ajax_nopriv_handler_1', 'handler_1');

function handler_1(){
  if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);

      $current_user = get_unfinished_lead_expertise( sess('email') );
      $current_user = $current_user->id;

      // stockéna en session izay nampidiriny
      $sess_id = save_session_step_1($HabitableSurface,$classified_property_livingDescription_netHabitableSurface,$chambre_nbr,$facade_nbr,$sdb_nbr );
      
      // tonga d update fona @zay satria efa azo id courant 
      update_lead_step_1( $current_user,$HabitableSurface,$classified_property_livingDescription_netHabitableSurface,$chambre_nbr,$facade_nbr,$sdb_nbr );

      ob_start();
        include ASTRA_THEME_CHILD_DIR . 'steps/s2.php';
      $out = ob_get_clean();

      echo $out;
      wp_die();
    }
}

function save_session_step_1( $HabitableSurface,$classified_property_livingDescription_netHabitableSurface,$chambre_nbr,$facade_nbr,$sdb_nbr ){
  $_SESSION['HabitableSurface']                                          = $HabitableSurface;
  $_SESSION['classified_property_livingDescription_netHabitableSurface'] = $classified_property_livingDescription_netHabitableSurface;
  $_SESSION['chambre_nbr']                                               = $chambre_nbr;
  $_SESSION['facade_nbr']                                                = $facade_nbr;
  $_SESSION['sdb_nbr']                                                   = $sdb_nbr;

  return true;
}

function update_lead_step_1( $id,$HabitableSurface,$classified_property_livingDescription_netHabitableSurface,$chambre_nbr,$facade_nbr,$sdb_nbr ){
    global $wpdb;

  $lead_expertise = $wpdb->prefix.'lead_expertise';

  $new_data = array(
    'derniere_etape'                                           => 1,
    'derniere_expertise' => date("d-m-Y H:i:s"),
    'HabitableSurface'                                         => $HabitableSurface,
    "classified_property_livingDescription_netHabitableSurface"=> $classified_property_livingDescription_netHabitableSurface,
    'chambre_nbr'                                              => $chambre_nbr,
    'facade_nbr'                                               => $facade_nbr,
    'sdb_nbr'                                                  => $sdb_nbr,
    
    );

    $update = $wpdb->update( 
        $lead_expertise,
        $new_data,
        array(
            'id' => $id
        ),
    );

    if( $update ){
        return true;
    }else{
        return false;
    }   
}


add_action('wp_ajax_back_from_1', 'back_from_1');
add_action('wp_ajax_nopriv_back_from_1', 'back_from_1');

function back_from_1(){
  if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);

      ob_start();
        include ASTRA_THEME_CHILD_DIR . 'steps/s'. $show .'.php';
      $out = ob_get_clean();

      echo $out;
      wp_die();

    }
}