<?php
add_action('wp_ajax_handler_3', 'handler_3');
add_action('wp_ajax_nopriv_handler_3', 'handler_3');

function handler_3(){
	if(isset($_POST) && !empty($_POST)){
		$str = http_build_query($_POST);
    parse_str($str, $Data);

    extract($Data);

    $current_user = get_unfinished_lead_expertise( sess('email') );
    $id = $current_user->id;

    update_lead_step_3( $id,$construct_year,$AnneSlide,$floor_batiment_nbr,$floor_batiment,$cave,$parking,$parking_exterieur,$parking_interieur );

    save_session_step_3( $construct_year,$AnneSlide,$floor_batiment_nbr,$floor_batiment,$cave,$parking,$parking_exterieur,$parking_interieur );

    ob_start();
			include ASTRA_THEME_CHILD_DIR . 'steps/s4.php';
		$out = ob_get_clean();

		echo $out;
		wp_die();
	}
}

function update_lead_step_3( $id,$construct_year,$AnneSlide,$floor_batiment_nbr,$floor_batiment,$cave,$parking,$parking_exterieur,$parking_interieur ){
    global $wpdb;

  $lead_expertise = $wpdb->prefix.'lead_expertise';

  $new_data = array(
    'derniere_etape'    => 3,
    'derniere_expertise' => date("d-m-Y H:i:s"),
    'construct_year'    => $construct_year,
    "AnneSlide"					=> $AnneSlide,
    'floor_batiment_nbr'=> $floor_batiment_nbr,
    'floor_batiment'    => $floor_batiment,
    'cave'              => $sdb_nbr,
    'parking'           => $parking,    
    "parking_exterieur" => $parking_exterieur,
    "parking_interieur" => $parking_interieur,
    );

    $update = $wpdb->update( 
        $lead_expertise,
        $new_data,
        array(
            'id' => $id
        ),
    );

    if( $update ){
        return true;
    }else{
        return false;
    }   
}

function save_session_step_3( $construct_year,$AnneSlide,$floor_batiment_nbr,$floor_batiment,$cave,$parking,$parking_exterieur,$parking_interieur ){
	$_SESSION['construct_year']		 = $construct_year;
	$_SESSION['AnneSlide']				 = $AnneSlide;
	$_SESSION['floor_batiment_nbr']= $floor_batiment_nbr;
	$_SESSION['floor_batiment'] 	 = $floor_batiment;
	$_SESSION['cave'] 				 		 = $cave;
	$_SESSION['parking'] 					 = $parking;
	$_SESSION['parking_exterieur'] = $parking_exterieur;
	$_SESSION['parking_interieur'] = $parking_interieur;

	return true;
}

add_action('wp_ajax_back_from_3', 'back_from_3');
add_action('wp_ajax_nopriv_back_from_3', 'back_from_3');

function back_from_3(){
  if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);

      ob_start();
        include ASTRA_THEME_CHILD_DIR . 'steps/s'. $show .'.php';
      $out = ob_get_clean();

      echo $out;
      wp_die();

    }
}