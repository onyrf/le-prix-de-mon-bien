<?php
add_action('wp_ajax_handler_4', 'handler_4');
add_action('wp_ajax_nopriv_handler_4', 'handler_4');

function handler_4(){
	if(isset($_POST) && !empty($_POST)){
		$str = http_build_query($_POST);
    parse_str($str, $Data);

    extract($Data);

    $current_user = get_unfinished_lead_expertise( sess('email') );
    $id = $current_user->id;

    update_lead_step_4( $id,$cuisine,$sanitaire,$revetement_sol,$peinture,$toiture,$chassis,$chauffage,$electricite );

    save_session_step_4( $cuisine,$sanitaire,$revetement_sol,$peinture,$toiture,$chassis,$chauffage,$electricite );

    ob_start();
			include ASTRA_THEME_CHILD_DIR . 'steps/s5.php';
		$out = ob_get_clean();

		echo $out;
		wp_die();
	}
}

function update_lead_step_4($id,$cuisine,$sanitaire,$revetement_sol,$peinture,$toiture,$chassis,$chauffage,$electricite ){
	global $wpdb;

	$lead_expertise = $wpdb->prefix.'lead_expertise';

  $new_data = array(
    'derniere_etape'   		=> 4,
    'derniere_expertise' => date("d-m-Y H:i:s"),
    'cuisine'     => $cuisine,
    "sanitaire"     => $sanitaire,
    'revetement_sol'   => $revetement_sol,
    'peinture' => $peinture,
    'toiture'     => $toiture,
    "chassis"     => $chassis,
    'chauffage'   => $chauffage,
    'electricite' => $electricite,
    );

    $update = $wpdb->update( 
        $lead_expertise,
        $new_data,
        array(
            'id' => $id
        ),
    );

    if( $update ){
        return true;
    }else{
        return false;
    }

}

function save_session_step_4( $cuisine,$sanitaire,$revetement_sol,$peinture,$toiture,$chassis,$chauffage,$electricite ){
	$_SESSION['cuisine']        = $cuisine;
  $_SESSION['sanitaire'] = $sanitaire;
  $_SESSION['revetement_sol'] = $revetement_sol;
  $_SESSION['peinture']     = $peinture;
  $_SESSION['toiture']       = $toiture;
  $_SESSION['chassis'] = $chassis;
  $_SESSION['chauffage'] = $chauffage;
  $_SESSION['electricite']    = $electricite;

	return true;
}

add_action('wp_ajax_back_from_4', 'back_from_4');
add_action('wp_ajax_nopriv_back_from_4', 'back_from_4');

function back_from_4(){
  if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);

      ob_start();
        include ASTRA_THEME_CHILD_DIR . 'steps/s'. $show .'.php';
      $out = ob_get_clean();

      echo $out;
      wp_die();

    }
}