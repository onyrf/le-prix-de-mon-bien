<?php
add_action('wp_ajax_handler_5', 'handler_5');
add_action('wp_ajax_nopriv_handler_5', 'handler_5');

function handler_5(){
	if(isset($_POST) && !empty($_POST)){
		$str = http_build_query($_POST);
    parse_str($str, $Data);

    extract($Data);

    $current_user = get_unfinished_lead_expertise( sess('email') );
    $id = $current_user->id;

    update_lead_step_5( $id,$batiment,$renovation_date,$renovation_date_nbr,$renew_date,$renew_date_nbr,$beneficie,$standing,$charme,$luminosite,$vue_degagee );

    save_session_step_5($batiment,$renovation_date,$renovation_date_nbr,$renew_date,$renew_date_nbr,$beneficie,$standing,$charme,$luminosite,$vue_degagee );

    ob_start();
			include ASTRA_THEME_CHILD_DIR . 'steps/s6.php';
		$out = ob_get_clean();

		echo $out;
		wp_die();
	}
}

function update_lead_step_5( $id,$batiment,$renovation_date,$renovation_date_nbr,$renew_date,$renew_date_nbr,$beneficie,$standing,$charme,$luminosite,$vue_degagee ){
	global $wpdb;

	$lead_expertise = $wpdb->prefix.'lead_expertise';

  $new_data = array(
    'derniere_etape' => 5,
    'derniere_expertise' => date("d-m-Y H:i:s"),
    'batiment'            => $batiment,
    "renovation_date"     => $renovation_date,
    'renovation_date_nbr' => $renovation_date_nbr,
    'renew_date'          => $renew_date,
    'renew_date_nbr'      => $renew_date_nbr,
    'beneficie'           => $beneficie,    
    "standing"            => $standing,
    'charme'    => $charme,
    "luminosite"      => $luminosite,
    "vue_degagee"      => $vue_degagee,
    );

    $update = $wpdb->update( 
        $lead_expertise,
        $new_data,
        array(
            'id' => $id
        ),
    );

    if( $update ){
        return true;
    }else{
        return false;
    }

}

function save_session_step_5( $batiment,$renovation_date,$renovation_date_nbr,$renew_date,$renew_date_nbr,$beneficie,$standing,$charme,$luminosite,$vue_degagee ){
	$_SESSION['batiment']       = $batiment;
  $_SESSION['renovation_date'] = $renovation_date;
  $_SESSION['renovation_date_nbr'] = $renovation_date_nbr;
  $_SESSION['renew_date']     = $renew_date;
  $_SESSION['renew_date_nbr'] = $renew_date_nbr;
  $_SESSION['beneficie'] = $beneficie;
  $_SESSION['standing'] = $standing;
  $_SESSION['charme']        = $charme;
  $_SESSION['luminosite'] = $luminosite;
  $_SESSION['vue_degagee'] = $vue_degagee;

	return true;
}

add_action('wp_ajax_back_from_5', 'back_from_5');
add_action('wp_ajax_nopriv_back_from_5', 'back_from_5');

function back_from_5(){
  if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);

      ob_start();
        include ASTRA_THEME_CHILD_DIR . 'steps/s'. $show .'.php';
      $out = ob_get_clean();

      echo $out;
      wp_die();

    }
}
