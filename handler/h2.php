<?php
add_action('wp_ajax_handler_2', 'handler_2');
add_action('wp_ajax_nopriv_handler_2', 'handler_2');

function handler_2(){
	if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);

      $attachments = array();

      if ( isset($_FILES)){
        if (!function_exists('wp_handle_upload')) {
                require_once(ABSPATH . 'wp-admin/includes/file.php');
        }
        $upload_overrides = array('test_form' => false);

        $files = $_FILES['file'];
        foreach ($files['name'] as $key => $value) {
          if ($files['name'][$key]) {
            $file = array(
              'name'     => $files['name'][$key],
              'type'     => $files['type'][$key],
              'tmp_name' => $files['tmp_name'][$key],
              'error'    => $files['error'][$key],
              'size'     => $files['size'][$key]
            );
            $movefile = wp_handle_upload($file, $upload_overrides); 
            if ($movefile && !isset($movefile['error'])) {
                 $attachments[] = $movefile['file'];
            } else {
                echo $movefile['error']. '</br>';
            }
          }
        }
      }

      $current_user = get_unfinished_lead_expertise( sess('email') );
      $current_user = $current_user->id;

      if( count($attachments) > 0 ) save_attachments( $current_user,$attachments );

      // stockéna en session izay nampidiriny
      $sess_id = save_session_step_2($terrasse,$terraseSurface,$surfaceTerrasse,$grenier,$grenierSuperficie,$grenierSurface,$charge,$garden_exist,$gardenSurface,$gardenSurfaceOutput );
      
      // tonga d update fona @zay satria efa azo id courant 
      update_lead_step_2( $current_user,$terrasse,$terraseSurface,$surfaceTerrasse,$grenier,$grenierSuperficie,$grenierSurface,$charge,$garden_exist,$gardenSurface,$gardenSurfaceOutput );

      ob_start();
				include ASTRA_THEME_CHILD_DIR . 'steps/s3.php';
			$out = ob_get_clean();

			echo $out;
			wp_die();
    }
}

function save_session_step_2( $terrasse = 'Non',$terraseSurface,$surfaceTerrasse,$grenier = 'Non',$grenierSuperficie,$grenierSurface,$charge,$garden_exist = 'Non',$gardenSurface,$gardenSurfaceOutput ){
  $_SESSION['terrasse']                                                  = $terrasse;
	$_SESSION['terraseSurface'] 				 						                       = $terraseSurface;
  $_SESSION['surfaceTerrasse']                                           = $surfaceTerrasse;
  $_SESSION['grenier']                                                   = $grenier;
	$_SESSION['grenierSuperficie'] 										                     = $grenierSuperficie;
	$_SESSION['grenierSurface'] 										                       = $grenierSurface;
	$_SESSION['charge'] 												                           = $charge;
  $_SESSION['garden_exist']                                              = $garden_exist;
  $_SESSION['gardenSurface']                                             = $gardenSurface;
  $_SESSION['gardenSurfaceOutput']                                       = $gardenSurfaceOutput;

	return true;
}

function update_lead_step_2( $id,$terrasse = 'Non',$terraseSurface,$surfaceTerrasse,$grenier = 'Non',$grenierSuperficie,$grenierSurface,$charge,$garden_exist = 'Non',$gardenSurface,$gardenSurfaceOutput ){
    global $wpdb;

  $lead_expertise = $wpdb->prefix.'lead_expertise';

  $new_data = array(
    'derniere_etape'                                           => 2,
    'derniere_expertise' => date("d-m-Y H:i:s"),
    'garden_exist'                                             => $garden_exist,    
    "gardenSurface"                                            => $gardenSurface,
    "gardenSurfaceOutput"                                      => $gardenSurfaceOutput,
    "terrasse"                                                 => $terrasse,
    "terraseSurface"                                           => $terraseSurface,
    "surfaceTerrasse"                                          => $surfaceTerrasse,
    "grenier"                                                  => $grenier,
    "grenierSuperficie"                                        => $grenierSuperficie,
    "grenierSurface"                                           => $grenierSurface,
    "charge"                                                   => $charge,
    );

    $update = $wpdb->update( 
        $lead_expertise,
        $new_data,
        array(
            'id' => $id
        ),
    );

    if( $update ){
        return true;
    }else{
        return false;
    }   
}

function save_attachments( $id,$attachments ){
  global $wpdb;

  $lead_expertise = $wpdb->prefix.'lead_expertise';

  $new_data = array(
  'attachment' => json_encode($attachments),
  );

  $update = $wpdb->update( 
      $lead_expertise,
      $new_data,
      array(
          'id' => $id
      ),
  );
  $_SESSION['attachment'] = json_encode($attachments);
  if( $update ){
    
    return true;
  }else{
      return false;
  }
}

add_action('wp_ajax_back_from_2', 'back_from_2');
add_action('wp_ajax_nopriv_back_from_2', 'back_from_2');

function back_from_2(){
  if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);

      ob_start();
        include ASTRA_THEME_CHILD_DIR . 'steps/s'. $show .'.php';
      $out = ob_get_clean();

      echo $out;
      wp_die();

    }
}