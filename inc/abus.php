<?php

add_action('wp_ajax_signalement_abus', 'signalement_abus');

add_action('wp_ajax_nopriv_signalement_abus', 'signalement_abus');

function signalement_abus(){
	if(isset($_POST) && !empty($_POST)){
		$str = http_build_query($_POST);
    parse_str($str, $Data);

    extract($Data);

    add_filter( 'wp_mail_from', 'sender_email_lpdmb' );
    add_filter( 'wp_mail_from_name', 'sender_name_lpdmb' );
    $destinataire = get_field('sender','option');

    $headers = array('Reply-To: '. $email,'Content-Type: text/html; charset=UTF-8');

    ob_start();
        include 'template_email/abus.php';
    $body_mail = ob_get_clean();

    if(@wp_mail( $destinataire, 'Report d\'un abus', $body_mail, $headers )){

	      echo "success";

	  }else {

	  		echo 'fail';

	  }

	}
}