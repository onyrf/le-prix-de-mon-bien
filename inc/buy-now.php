<?php

function get_orders_ids_by_product_id( $product_id ) {
    global $wpdb;
    
    // Define HERE the orders status to include in  <==  <==  <==  <==  <==  <==  <==
    $orders_statuses = "'wc-completed', 'wc-processing', 'wc-on-hold'";

    # Get All defined statuses Orders IDs for a defined product ID (or variation ID)
    return $wpdb->get_col( "
        SELECT DISTINCT woi.order_id
        FROM {$wpdb->prefix}woocommerce_order_itemmeta as woim, 
             {$wpdb->prefix}woocommerce_order_items as woi, 
             {$wpdb->prefix}posts as p
        WHERE  woi.order_item_id = woim.order_item_id
        AND woi.order_id = p.ID
        AND p.post_status IN ( $orders_statuses )
        AND woim.meta_key IN ( '_product_id', '_variation_id' )
        AND woim.meta_value LIKE '$product_id'
        ORDER BY woi.order_item_id DESC"
    );
}

/**
* Process Order Refund through Code
* @return WC_Order_Refund|WP_Error
*/
function vmb_wc_refund_order( $order_id, $refund_reason = '' ) {
  
  $order  = wc_get_order( $order_id );

  // If it's something else such as a WC_Order_Refund, we don't want that.
  if( ! is_a( $order, 'WC_Order') ) {
    return new WP_Error( 'wc-order', 'ID de commande invalide' );
  }
  
  if( 'refunded' == $order->get_status() ) {
    return new WP_Error( 'wc-order', 'Commande déjà remboursée' );
  }
  
  
  // Get Items
  $order_items   = $order->get_items();
  
  // Refund Amount
  $refund_amount = 0;

  // Prepare line items which we are refunding
  $line_items = array();
  
  if ( $order_items ) {
    foreach( $order_items as $item_id => $item ) {
      
      $item_meta  = $order->get_item_meta( $item_id );
      
      $tax_data = $item_meta['_line_tax_data'];
      
      $refund_tax = 0;

      if( is_array( $tax_data[0] ) ) {

        $refund_tax = array_map( 'wc_format_decimal', $tax_data[0] );

      }

      $refund_amount = wc_format_decimal( $refund_amount ) + wc_format_decimal( $item_meta['_line_total'][0] );

      $line_items[ $item_id ] = array( 
        'qty' => $item_meta['_qty'][0], 
        'refund_total' => wc_format_decimal( $item_meta['_line_total'][0] ), 
        'refund_tax' =>  $refund_tax );
      
    }
  }

  $refund = wc_create_refund( array(
    'amount'         => $refund_amount,
    'reason'         => $refund_reason,
    'order_id'       => $order_id,
    'line_items'     => $line_items,
    'refund_payment' => true
  ));

  return $refund;
}