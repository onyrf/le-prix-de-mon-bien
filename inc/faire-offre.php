<?php
 add_action('init', 'create_offre_pt');

 function create_offre_pt(){

 	register_post_type( 'offre',
    // CPT Options
      array(
          'labels' => array(
              'name' => __( 'Offres' ),
              'singular_name' => __( 'Offre' )
          ),
          'public' => true,
          'has_archive' => true,
          'rewrite' => array('slug' => 'offre'),
          'show_in_rest' => true,
          'menu_icon' 	=> 'dashicons-randomize',
      )
    );
 }

 function loop_offres( $propID = ''){
 	$propID = $propID ? $propID : 0;

 	$offres = get_field('field_6113cd69a399b', $propID);
 	
 	return $offres; 	
 }

add_filter( 'posts_where', 'devplus_wpquery_where' );
function devplus_wpquery_where( $where ){
    global $current_user;

    if( is_user_logged_in() ){
         // logged in user, but are we viewing the library?
         if( isset( $_POST['action'] ) && ( $_POST['action'] == 'query-attachments' ) ){
            // here you can add some extra logic if you'd want to.
            $where .= ' AND post_author='.$current_user->data->ID;
        }
    }

    return $where;
}

add_action('wp_ajax_faire_offre', 'faire_offre');
add_action('wp_ajax_nopriv_faire_offre', 'faire_offre');

function faire_offre(){
	if(isset($_POST) && !empty($_POST)){
		$str = http_build_query($_POST);
    parse_str($str, $Data);

    extract($Data);

    $now = new DateTime();

    $date_creation = get_field('field_creation', $propID ) ? get_field('field_creation', $propID ) : "01/01/2021 13:42";

    $date_creation_1 = new DateTime( $date_creation );
    $date_creation_1 = $date_creation_1->format('Y-m-d H:i:s');
    $date_creation_1 = new DateTime( $date_creation_1 );

    $date_creation_2 = new DateTime( $date_creation );
    $date_creation_2 = $date_creation_2->format('Y-m-d H:i:s');
    $date_creation_2 = new DateTime( $date_creation_2 );

		$_10j_apres_creation     = $date_creation_1->add( new DateInterval('P10D') );
		$_12j_apres_creation     = $date_creation_2->add( new DateInterval('P12D') );

		if( $now->diff( $_12j_apres_creation )->invert > 0 || $now->diff( $_10j_apres_creation )->invert == 0 ){
		ob_start();
?>
			<script type="text/javascript">
				alert('Les offres ont déjà été clôturées');
				window.location.reload();
 			</script>
 <?php
			$out = ob_get_clean();

			echo $out;

			wp_die();
		}

    $prop = array(
    	'post_type' => 'offre',
    	'post_status' => 'publish',
    	'post_title'  => $agence .' - '. get_the_title( $propID ),
    );

    $offreID = wp_insert_post( $prop );

    $attach = array();

    if( $offreID ){
	    if ( isset($_FILES)){

	      if (!function_exists('wp_handle_upload')) {
	              require_once(ABSPATH . 'wp-admin/includes/file.php');
	      }
	      $upload_overrides = array('test_form' => false);


	      $files = $_FILES['files'];
	      
	      foreach ($files['name'] as $key => $value) {
	        if ($files['name'][$key]) {
	          $file = array(
	            'name'     => $files['name'][$key],
	            'type'     => $files['type'][$key],
	            'tmp_name' => $files['tmp_name'][$key],
	            'error'    => $files['error'][$key],
	            'size'     => $files['size'][$key]
	          );
	          $movefile = wp_handle_upload($file, $upload_overrides); 
	          if ($movefile && !isset($movefile['error'])) {
	          		if( $key == 0 ){
	          			$cin = $movefile['file'];
	          			$attach[] = $movefile['file'];
									update_field('carte_didentite', $cin, $offreID ); 
	          		}

								if( $key == 1 ){
									$doc_offre = $movefile['file'];
									$attach[] = $movefile['file'];
									update_field('doc_offre', $doc_offre, $offreID ); 
								}

	          } else {
	              echo $movefile['error']. '</br>';
	          }
	        }
	      }
	     

	    } 
			$now = date('d/m/Y');
			$now_long = date('d/m/Y H:i');
			$date = $now;
			update_field('agence', $agence, $offreID );
			update_field('date_de_loffre', $now_long, $offreID );
			// update_field('delai_validite', $validite, $offreID );
			update_field('prix_de_loffre', $prix, $offreID );
			update_field('id_du_bien', $propID, $offreID );
			update_field('condition_suspensive', $condition_suspensive, $offreID );
			update_field('paiement_accompte', $paiement_accompte, $offreID );

			if ( $paiement_accompte == 'oui'){
				update_field('taux_accompte', $taux_accompte, $offreID );
			}

			update_post_meta( $offreID, 'email_agent', $email_agence );

			$old_offers = is_array( loop_offres(  $propID ) ) ? loop_offres( $propID  ) : array();

			$of_ids = array();

			foreach( $old_offers as $old_offer ){
				$of_ids[] = $old_offer->ID;
			}

			$of_ids[] = $offreID;
			
			update_field( 'field_6113cd69a399b', $of_ids, $propID);

		}

		ob_start();
		?>
			<div class="item">
          <div class="date">
              <?= $now ?> 
          </div>
          <div class="agence">
             <?= $agence ?>
          </div>
          <div class="prix">
               <b><?= millier( $prix ) ?>€</b>
          </div>
      </div>

		<?php
		$row = ob_get_clean();

		/* Bloc envoi mail */
		add_filter( 'wp_mail_from', 'sender_email_lpdmb' );
    add_filter( 'wp_mail_from_name', 'sender_name_lpdmb' );

    $reply = get_field('sender','option');
    $destinataire = get_field('julie','option');

		$headers = array("Reply-To: $reply","Cc: $reply", 'Content-Type: text/html; charset=UTF-8');

		$ref = get_field('reference', $propID );
		$obj = "REF $ref : Une offre a été ajoutée au bien !";

		ob_start();
			include INC_DIR . 'template_email/offre_julie.php';
		$body_mail = ob_get_clean();		

    if( wp_mail( array( $destinataire, $email_agence ), $obj, $body_mail, $headers, $attach ) ){
    	//mail @partenaire
			// $partenaires = get_all_partners();
			// foreach( $partenaires as $partner ){
			// 	wp_mail( $partner->user_email, $obj, $body_mail, $headers );
			// }

   //  	ob_start();
			// 	include INC_DIR . 'template_email/offre_agence.php';
			// $body_mail = ob_get_clean();

   //  	wp_mail( $email_agence, 'Offre placée', $body_mail, $headers, array( $movefile['url'] ) );
    }else{    	

    }

		/* End mail */

		echo $row;
	}
	wp_die();
}

add_action('wp_ajax_valid_offre', 'valid_offre');
add_action('wp_ajax_nopriv_valid_offre', 'valid_offre');

function valid_offre(){
	if(isset($_POST) && !empty($_POST)){
		$str = http_build_query($_POST);
    parse_str($str, $Data);

    extract($Data);

    $offreID =  (int)$offre_id;
    $propID =  (int)$propID;

    $out = array();

    $fiche_cloturee = get_post_meta( $propID, 'fiche_cloturee', true );
    if( $fiche_cloturee ){
    	$out['error'] = 'error';
    	$out['titre'] = 'Erreur !';
    	$out['message'] = "Une offre a déjà été validée pour ce bien.";
    	die;
    }

    $reply = get_field('sender','option');
    $destinataire = get_field('julie','option');

    $email_agence = get_post_meta( $offreID, 'email_agent', true ) ? get_post_meta( $offreID, 'email_agent', true ) : 'herihasina.rr@gmail.com' ;

    $agence 							= get_field('agence', $offreID );
		$date 								= get_field('date_de_loffre', $offreID );
		$prix 								= get_field('prix_de_loffre', $offreID );
		$condition_suspensive = get_field('condition_suspensive', $offreID );
		$paiement_accompte 		= get_field('paiement_accompte', $offreID );
		$taux_accompte 				= get_field('taux_accompte', $offreID );

		$nom_proprio = get_field('nom_proprietaire', $propID );
		$prenom_proprio = get_field('prenom_proprietaire', $propID );
		$email_proprio = get_field('email_proprietaire', $propID );

		$attach = array( get_field('carte_didentite', $offreID ), get_field('doc_offre', $offreID ) );

		$headers = array("Reply-To: $reply","Cc: $reply", 'Content-Type: text/html; charset=UTF-8');

		$ref = get_field('reference', $propID );

		/* mail ho an'agent rehetra nividy anlé fiche */
		$obj = "REF $ref : Une offre a été validée !";

		ob_start();
				include INC_DIR . 'template_email/offre_validee_notif_acheteurs.php';
		$body_mail = ob_get_clean();

		$product_id = get_product_id_of_property( $propID );
    $customers = get_all_partners_by_fiche( $product_id );
    @wp_mail( $customers, $obj, $body_mail, $headers );
    /* \\mail ho an'agent rehetra nividy anlé fiche */

		ob_start();
			include INC_DIR . 'template_email/offre_validee_notif_vmb.php';
		$body_mail = ob_get_clean();		

    if( @wp_mail( $destinataire, $obj, $body_mail, $headers, $attach ) ){ // mail ho'an VMB //


    		$obj = "REF $ref : Félicitation, vous avez validé une offre !";

	    	ob_start();
					include INC_DIR . 'template_email/offre_validee_notif_proprio.php';
				$body_mail = ob_get_clean();

				if( @wp_mail( $email_proprio, $obj, $body_mail, $headers, $attach ) ){

					update_post_meta( $propID, 'fiche_cloturee', true );
					update_field('offre_validee', $offreID, $propID);

					$out['error'] = 'success';
					$out['titre'] = 'Félicitation !';
	    		$out['message'] = "L'offre a été validée avec succès.";

				}else{
					$out['error'] = 'error';
					$out['titre'] = 'Erreur !';
	    		$out['message'] = "Erreur interne.";
				}

    }else{
    	$out['error'] = 'error';
    	$out['message'] = "Erreur interne.";
    }

    echo json_encode( $out );
    die;
  }
}

add_filter( 'manage_offre_posts_columns', 'set_custom_edit_offre_columns' );
function set_custom_edit_offre_columns($columns) {
	unset( $columns['date'] );
    $columns['nom'] = "Nom";
    $columns['prix'] = "Prix";
    $columns['date_offre'] = "Date";

    return $columns;
}

add_action( 'manage_offre_posts_custom_column' , 'custom_offre_column', 10, 2 );
function custom_offre_column( $column, $post_id ) {
    switch ( $column ) {
        case 'nom' :
        		echo get_field( 'agence', $post_id );
            break;

        case 'date_offre':
        		the_field('date_de_loffre', $post_id );
        		break;

        case 'prix' :
            $prix = get_field('prix_de_loffre', $post_id );
            echo number_format_i18n( $prix ).'€';
            break;

    }
}