<?php

add_action('wp_ajax_estimation', 'estimation');

add_action('wp_ajax_nopriv_estimation', 'estimation');



function estimation()

{

    if(isset($_POST) && !empty($_POST))
    {
        global $woocommerce;
        $str = http_build_query($_POST);
        parse_str($str, $Data);

        extract($Data);

        $ret = []; 

        $attachments = array();

        if ( isset($_FILES)){
            if (!function_exists('wp_handle_upload')) {
                    require_once(ABSPATH . 'wp-admin/includes/file.php');
            }
            $upload_overrides = array('test_form' => false);

            $files = $_FILES['file'];
            foreach ($files['name'] as $key => $value) {
              if ($files['name'][$key]) {
                $file = array(
                  'name'     => $files['name'][$key],
                  'type'     => $files['type'][$key],
                  'tmp_name' => $files['tmp_name'][$key],
                  'error'    => $files['error'][$key],
                  'size'     => $files['size'][$key]
                );
                $movefile = wp_handle_upload($file, $upload_overrides); 
                if ($movefile && !isset($movefile['error'])) {
                     $attachments[] = $movefile['file'];
                } else {
                    echo $movefile['error']. '</br>';
                }
              }
            }
        }
        $_SESSION['pj'] = $attachments;

        $_SESSION['Data'] = $Data;

        $mode_lead = 'reduit';
        if( $mode == 'plein' ){
            $prod_id = 360;
            $mode_lead = 'plein';
        }else{
            $prod_id = 176;
        }

        // check ra efa répertorié tsy nahavita achat ilay email
        $current_lead_expertise = get_unfinished_lead_expertise( $email );

        // ra tsy mbola ao d répertoriéna zany
        if( false === $current_lead_expertise ){
            $save_lead_expertise = save_lead_expertise( $email, $telephone, json_encode( $Data ), json_encode( $attachments ), $mode_lead );
        }else{ // ra efa tao d updatena ny date de dernière demande dexpertise
            $update_lead_expertise = update_lead_expertise( $email,$telephone, json_encode( $Data ), json_encode( $attachments ), $mode_lead );
        }  

        $woocommerce->cart->empty_cart();
    
        $woocommerce->cart->add_to_cart( $prod_id, 1 );

        $ret['chekout'] = site_url( '/paiement' );        

        echo json_encode($ret);

        die();  

}
}


add_action('wp_ajax_demande_exp', 'demande_exp');

add_action('wp_ajax_nopriv_demande_exp', 'demande_exp');

function demande_exp(){
    if(isset($_POST) && !empty($_POST))
    {
        $str = $_POST['data'];
        parse_str($str, $Data);
        extract($Data);
        $ret = array();

        $subject = 'Demande d\'expertise';        

        ob_start();
            include 'template_email/email_merci.php';
        $body_mail = ob_get_clean();        

        if( 'coach' == $ct ):             
        //     // Hooking up our functions to WordPress filters 
            add_filter( 'wp_mail_from', 'sender_email_wicoach' );
            add_filter( 'wp_mail_from_name', 'sender_name_wicoach' );
            $destinataire = get_field('coach','option');
        //     include 'template_email/email_pour_coach.php';
        elseif( 'agence' == $ct ):
            add_filter( 'wp_mail_from', 'sender_email_lpdmb' );
            add_filter( 'wp_mail_from_name', 'sender_name_lpdmb' );
            $destinataire = get_field('agent_immo','option');            
        endif;        

        $headers = array('Reply-To: '. get_field('sender','option'),'Cc:'. get_field('sender','option'),'Content-Type: text/html; charset=UTF-8'); 
            
        if(@wp_mail( $destinataire, $subject, $body_mail, $headers )){

            $ret['status'] = 'OK';

            $ret['result'] = 1;

            // if( 'coach' == $ct ){
            //     $ret['url'] = site_url( 'merci-wicoach' ).'?org=nl&email='.$email.'&ct='.$ct;
            // }else{
            //     $ret['url'] = site_url( 'merci' ).'?org=nl&email='.$email.'&ct='.$ct;
            // }     

            $ret['url'] = site_url( 'merci' ).'?org=nl&email='.$email.'&ct='.$ct;       

        }else {

            $ret['status'] = 'KO';

            $ret['result'] = 0;

            $ret['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">une erreur est survenue</p>';

        }
    }

    echo json_encode($ret);

    wp_die();
}

/* CRON email de rappel
*
*/
add_filter( 'cron_schedules', 'myprefix_add_weekly_cron_schedule' );
function myprefix_add_weekly_cron_schedule( $schedules ) {
    $schedules['fefakadiny'] = array(
        'interval' => 900, // 1 week in seconds
        'display'  => __( '15mn' ),
    );
 
    return $schedules;
}

if (! wp_next_scheduled ( 'rappel_abandon_panier' )) {
   wp_schedule_event(time(), 'fefakadiny', 'rappel_abandon_panier');
}

add_action('rappel_abandon_panier','email_rappel_abandon_panier');

function email_rappel_abandon_panier(){
     global $wpdb;
  $lead_expertise = $wpdb->prefix.'lead_expertise';

    $result = $wpdb->get_results(
        "SELECT * FROM $lead_expertise 
        WHERE rappel = 0
        AND converti = 0"
      );
      
      if (!empty($result)) {    
        $leads = $result;
      }else {
        $leads = array();
      }

    
    if( !empty( $leads ) ){
        foreach( $leads as $lead ){
            $id    = $lead->id;
            $email = $lead->email;
            $fname = $lead->fname;
            $lname = $lead->lname;
            $last  = $lead->derniere_expertise;

            $datetime = DateTime::createFromFormat('d-m-Y H:i:s', $last);
            $datetime->setTimezone(new DateTimeZone('Europe/Paris'));
            $last = convert_day_to_fr(convert_month_to_fr($datetime->format('l d F Y à H:i')));
            
            $lastDT  = new DateTime($lead->derniere_expertise);

            $now = date('d-m-Y H:i:s');
            $nowDT = new DateTime( $now );

            $diff = $lastDT->diff($nowDT);

            $diff = $diff->format("%H");

            if( $diff >= 1 ){

               $subject = "Finalisez votre demande d'expertise";            

               ob_start();
                   include("template_email/email_rappel.php");
               $body = ob_get_clean();        

               $cc = 'jacky@kahan.com';

               $headers = array("From: $email", 'Content-Type: text/html; charset=UTF-8');
               $headers[] = "Cc: ". get_field('sender','option');
               if($cc) {
                   $headers[] = "Cc: $cc";
               }

               if(@wp_mail( $email, $subject, $body, $headers )){
                  $lead_expertise = $wpdb->prefix.'lead_expertise';

                    $new_data = array(
                        'rappel' => 1,
                    );

                    $update = $wpdb->update( $lead_expertise,
                        $new_data,
                        array(
                            'id' => $id
                        )
                    );
               }

            }
        }
    }
}

