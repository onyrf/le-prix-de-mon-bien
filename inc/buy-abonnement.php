<?php
	add_action('wp_ajax_buy_abo', 'buy_abo');
	add_action('wp_ajax_nopriv_buy_abo', 'buy_abo');

	function buy_abo(){
		if(isset($_POST) && !empty($_POST)){
			global $woocommerce;

			$str = http_build_query($_POST);
	    parse_str($str, $Data);

	    extract($Data);
	    if( $modalite == '' || !$modalite ) wp_die('TENA MIAIKY NY HAFETSENAO AH E');
	    if( $modalite != 'mois' && $modalite != 'annee' ) wp_die('TENA SAHIRANA MITS ENAO ZAN');
			if( $nom == '' ) wp_die('Le nom est vide');
			if( $prenom == '' ) wp_die('Le prénom est vide');
			if( $email == '' ) wp_die('Email vide ou incorrect');
			if( $telephone == '' ) wp_die('Le téléphone est vide');
			if( $num_ipi == '' ) wp_die('Le nom IPI est vide');
			if( $paiement == '' ) wp_die('Le mode de paiement est vide');

	    $paiement = wp_strip_all_tags( $paiement ); 

	    WC()->session->set( 'chosen_payment_method', $paiement );

	    $woocommerce->cart->empty_cart();

	    $add_prod = $modalite == 'annee' ? ID_ABO_AN : ID_ABO;
	    $woocommerce->cart->add_to_cart( $add_prod, 1 );

	    $_SESSION['email'] = $email;
	    $_SESSION['fname'] = $prenom;
	    $_SESSION['lname'] = $nom;
	    $_SESSION['ipi'] 	 = $num_ipi;
	    $_SESSION['telephone'] = $telephone; 

	    if( !is_user_logged_in() ):

		    $user_id = sanitize_title( $prenom ) . wp_rand( wp_rand(1, 5), 10 * wp_rand(1, 5) );
	      do {
	        $username = sanitize_title( $prenom ) . wp_rand( wp_rand(1, 5), 10 * wp_rand(1, 5) );
	      } while ( username_exists( $username ) );

	      $user_id = username_exists( $username );
	 
	      if ( !email_exists($email) ) {
	            $pass = wp_generate_password(7, true, false);
	          $user_id = wp_create_user( $username, $pass , $email );
	          if( !is_wp_error($user_id) ) {
	              $user = get_user_by( 'id', $user_id );
	              $user->set_role( 'customer' );
	              auto_login_new_user( $username, $pass );
	          }
	      }else{
	      	// require('/wp-blog-header.php');

	      	$exist_user = get_user_by( 'email', $email );
	      	
					$user_login = $exist_user->data->user_login; 
					$user = get_userdatabylogin($user_login);
					$user_id = $user->ID;

					wp_set_current_user($user_id, $user_login);
					wp_set_auth_cookie($user_id); 

					// do_action('wp_login', $user_login); 
	      }

    	endif;

	    ob_start();
	    ?>
	    	<iframe src="<?= site_url('paiement') .'?method='.$paiement.'&ck=iframe' ?>" style="width:100%; min-height: 100vh" onload="resizeIframe(this)"></iframe>
	    	<script>
			  function resizeIframe(obj) {
			    obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 100 + 'px';
			  }
			</script>
	    <?php      
	    $out = ob_get_clean();
	    echo $out;
   
		}

	wp_die();

	}
?>