<?php
add_action( 'init', 'create_lead_expertise_table');

function create_lead_expertise_table(){
  global $wpdb;

  $table_name = $wpdb->prefix . "lead_expertise";

  if( $wpdb->query( 'SELECT * FROM ' . $table_name ) === false ) {

    $query = "CREATE TABLE " . $table_name . " (
			id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
			email varchar(60) NOT NULL,
			derniere_expertise varchar(60) NOT NULL,
			derniere_etape varchar(2) NOT NULL,
			data varchar(3000) NOT NULL,
			attachment varchar(3000) NOT NULL,
			mode varchar(15) NOT NULL,
			rappel tinyint(1) NOT NULL,
			converti tinyint(1) NOT NULL,
			lname varchar(50) NOT NULL,
			fname varchar(50) NOT NULL,
			telephone varchar(20) NOT NULL,
			adresse varchar(150) NOT NULL,
			numero varchar(10) NOT NULL,
			postal varchar(10) NOT NULL,
			ville varchar(30) NOT NULL,
			property_type varchar(20) NOT NULL,
			
			HabitableSurface varchar(5) NOT NULL,
			classified_property_livingDescription_netHabitableSurface varchar(5) NOT NULL,
			chambre_nbr varchar(5) NOT NULL,
			facade_nbr varchar(5) NOT NULL,
			sdb_nbr varchar(5) NOT NULL,
			garden_exist varchar(5) NOT NULL,
			gardenSurface varchar(5) NOT NULL,
			gardenSurfaceOutput varchar(10) NOT NULL,
			terrasse varchar(10) NOT NULL,
			terraseSurface varchar(5) NOT NULL,
			surfaceTerrasse varchar(10) NOT NULL,
			grenier varchar(5) NOT NULL,
			grenierSuperficie varchar(10) NOT NULL,
			grenierSurface varchar(10) NOT NULL,
			charge varchar(10) NOT NULL,
			construct_year varchar(5) NOT NULL,
			AnneSlide varchar(5) NOT NULL,
			floor_batiment_nbr varchar(10) NOT NULL,
			floor_batiment varchar(10) NOT NULL,
			cave varchar(5) NOT NULL,
			parking varchar(5) NOT NULL,
			parking_exterieur varchar(10) NOT NULL,
			parking_interieur varchar(10) NOT NULL,

			batiment varchar(20) NOT NULL,
			renovation_date varchar(20) NOT NULL,
			renovation_date_nbr varchar(20) NOT NULL,
			renew_date varchar(20) NOT NULL,
			renew_date_nbr varchar(20) NOT NULL,
			beneficie varchar(40) NOT NULL,
			standing varchar(60) NOT NULL,

			toiture varchar(30) NOT NULL,
			chassis varchar(30) NOT NULL,
			chauffage varchar(30) NOT NULL,
			electricite varchar(30) NOT NULL,

			cuisine varchar(30) NOT NULL,
			sanitaire varchar(30) NOT NULL,
			revetement_sol varchar(30) NOT NULL,
			peinture varchar(30) NOT NULL,

			charme varchar(30) NOT NULL,
			luminosite varchar(30) NOT NULL,
			vue_degagee varchar(30) NOT NULL,

			nuisance varchar(30) NOT NULL,
			vis_a_vis varchar(30) NOT NULL,
			infiltration varchar(30) NOT NULL,
			environnement varchar(30) NOT NULL,

			orientation varchar(20) NOT NULL,
			influence varchar(1000) NOT NULL,

			proprietaire_bien varchar(5) NOT NULL,
			envisage_vendre_bien varchar(100) NOT NULL,
			informe_evolution_marche varchar(100) NOT NULL,
			informe_achat_bien varchar(100) NOT NULL,
			rgpd varchar(20) NOT NULL,

			select_call varchar(5) NOT NULL,
			date varchar(15) NOT NULL,
			obje_estim varchar(30) NOT NULL,
			motiv varchar(30) NOT NULL,
			acceptement varchar(30) NOT NULL,
      PRIMARY KEY  (id)
    );";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($query);
  }
 
}

function save_lead_expertise( $email,$phone, $data, $attachment = '', $mode = 'reduit' ){
	global $wpdb;

  $lead_expertise = $wpdb->prefix.'lead_expertise';

  $wpdb->insert($lead_expertise, array(
    "email"    					 => $email,
    "telephone"    					 => $phone,
    "derniere_expertise" => date("d-m-Y H:i:s"),
    "data" 		 				 	 => $data,
    "attachment" 		 		 => $attachment,
    "mode" 		 		 			 => $mode,
    "rappel" 					   =>	0,
    "converti" 					 => 0,
  ));

  return $wpdb->insert_id;
}

function get_unfinished_lead_expertise( $email = '' ){
  global $wpdb;
  $lead_expertise = $wpdb->prefix.'lead_expertise';

  if( $email != '' ){

  	$result = $wpdb->get_results(
	    "SELECT * FROM $lead_expertise 
	    WHERE email = '$email'
	    AND rappel = 0
	    AND converti = 0"
	  );
	  if (!empty($result)) {  	
	    return $result[0];
	  }else {
	      return false;
	  }

  }else{

  	$result = $wpdb->get_results(
	    "SELECT * FROM $lead_expertise 
	    WHERE rappel = 0
	    AND converti = 0"
	  );
	  
	  if (!empty($result)) {  	
	    return $result;
	  }else {
	      return false;
	  }

  }  
  
}

function update_lead_expertise( $email,$phone, $data, $attachment = '', $mode = 'reduit' ){
	global $wpdb;
	$lead_expertise = $wpdb->prefix.'lead_expertise';

	$new_data = array(
		'telephone'				 			 => $phone,
		'derniere_expertise' => date("d-m-Y H:i:s"),
		"data" 		 					 => $data,
		"attachment" 		 		 => $attachment,
		"mode" 		 					 => $mode,
	);

	$update = $wpdb->update( $lead_expertise,
		$new_data,
		array(
			'email' => $email
		)
	);

	if( $update ){
		return true;
	}else{
		return false;
	}
}

function turn_lead_into_client( $email ){
	global $wpdb;
	$lead_expertise = $wpdb->prefix.'lead_expertise';

	$new_data = array(
		'converti' => 1,
	);

	$update = $wpdb->update( $lead_expertise,
		$new_data,
		array(
			'email' => $email
		)
	);

	if( $update ){
		return true;
	}else{
		return false;
	}
}

function update_rappel_lead_expertise( $id ){
	global $wpdb;
	$lead_expertise = $wpdb->prefix.'lead_expertise';

	$new_data = array(
		'rappel' => 1,
	);

	$update = $wpdb->update( $lead_expertise,
		$new_data,
		array(
			'id' => $id
		)
	);

	if( $update ){
		return true;
	}else{
		return false;
	}
}

function get_lead_data( $id,$email ){
	global $wpdb;
	$lead_expertise = $wpdb->prefix.'lead_expertise';

	$result = $wpdb->get_results(
		"SELECT * FROM $lead_expertise 
		WHERE email = '$email'
		AND id = $id
		AND converti = 0"
	);
	if (!empty($result)) {  	
		return $result[0];
	}else {
	  return false;
	}
}


?>