

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <title>Votre message</title>
      <style type="text/css">
         #outlook a{padding:0;} 
         .ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
         .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span {color:#000000; text-decoration:none !important; border-bottom:none !important; background:none !important; cursor:default; }
         body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
         img{-ms-interpolation-mode:bicubic;} 
         body{margin:0; padding:0;}
         img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;display:block; }
         table{border-collapse:collapse !important;border-spacing:0;}
         table, td{mso-table-lspace:0pt !important; mso-table-rspace:0pt !important;}
         body{height:100% !important; margin:0; padding:0; width:100% !important;}
         .appleBody a {color:#68440a; text-decoration: none;}
         .appleFooter a {color:#999999; text-decoration: none;}
         br, strong br, b br, em br, i br { line-height:100%; }
         h1, h2, h3, h4, h5, h6, p, a { line-height: 100%; -webkit-font-smoothing: antialiased; margin:0; padding:0}
         strong          {font-weight:700;}    
         @import url(https://fonts.googleapis.com/css?family=Open+Sans&display=swap);
         @media screen and (max-width: 680px) { 
         table[class=w40],   td[class=w40],  img[class=w40]  { width:25px !important; }
         table[class=w600],  td[class=w600], img[class=w600] { width:424px !important; }
         table[class=w680],  td[class=w680], img[class=w680] { width:480px !important; }
         }
         @media screen and (max-width: 480px) { 
         table[class=w40],   td[class=w40],  img[class=w40]  { width:17px !important; }
         table[class=w600],  td[class=w600], img[class=w600] { width:286px !important; }
         table[class=w680],  td[class=w680], img[class=w680] { width:320px !important; }
         }
         @media print{
         table[class=w40],   td[class=w40],  img[class=w40]  { display:none !important; }
         }
          @media only screen and (min-width:480px) {
            .mj-column-px-228 {
            width: 228px !important;
            max-width: 228px;
            }
            .mj-column-px-120 {
            width: 120px !important;
            max-width: 120px;
            }
            .mj-column-px-229 {
            width: 229px !important;
            max-width: 229px;
            }
         }

         @media only screen and (max-width:480px) {
            table.mj-full-width-mobile {
            width: 100% !important;
            }
            td.mj-full-width-mobile {
            width: auto !important;
            }
         }

         @media only screen and (max-width:679px) {
            .col-100 {
            width: 100% !important;
            max-width: 100% !important
            }
            .ou {
            padding: 25px 0 !important
            }
            .hidden-mob {
            display: none !important
            }
            .blcBanner>table>tbody>tr>td{ padding: 35px 0!important; }
         }
      </style>
   </head>
   <body style="background-color:#F9F9F9">
      <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#F9F9F9" >
         <tr style="border-collapse:collapse;" >
            <td width="100%" valign="top" align="center" bgcolor="#F9F9F9">
               <table class="w680" width="680" border="0" cellpadding="0" cellspacing="0" align="center" style="font-family: 'Open Sans', sans-serif; color:#555555 !important; font-size:14px; line-height:26px; -webkit-font-smoothing: antialiased; text-decoration:none;">
                  <!-- spacer -->
                  <tr>
                     <td colspan="3" height="40" bgcolor="#F9F9F9"></td>
                  </tr>
                  <!-- Logo -->
                  <tr>
                     <td colspan="3" bgcolor="#FFFFFF" align="center" valign="top">
                        <div class="logo">
                           <a href="<?= site_url(); ?>" target="_blank" style="font-family: 'Open Sans', sans-serif; color:#000;  -webkit-font-smoothing:antialiased; font-size:12px; line-height:20px; -webkit-font-smoothing: antialiased; margin-bottom:0px !important; text-decoration:none;">
                           <img style="display: inline-block;" src="<?= IMG_DIR ?>banner_particulier.gif" class="w680" width="680" alt="vendezmonbien.be" border="0"/>
                           </a>
                        </div>
                     </td>
                  </tr>
                  <!-- spacer -->
                  <tr>
                     <td colspan="3" height="20" bgcolor="#FFFFFF">&nbsp;</td>
                  </tr>
                  <!-- titre -->
                  <tr>
                     <td width="40"  class="w40"  bgcolor="#FFFFFF">&nbsp;</td>
                     <td width="600" class="w600" bgcolor="#FFFFFF" align="left">
                        <!-- <h3 style="color:#fa7667 ;  -webkit-font-smoothing:antialiased; font-size:14px; line-height:20px; margin-bottom:10px; font-weight:700;">vendezmonbien.be
                        </h3> -->
                        <p style="color:#1b0f3a; -webkit-font-smoothing:antialiased; font-size:16px; line-height:20px; margin-bottom:40px; font-weight:700;"><?= $fname.' '.$lname ?>,<br><br> Merci pour votre demande d'estimation.
                        </p>
                     </td>
                     <td width="40" class="w40" bgcolor="#FFFFFF">&nbsp;</td>
                  </tr>
                  <tr >
                     <td width="40"  class="w40"  bgcolor="#FFFFFF">&nbsp;</td>
                     <td width="600" class="w600" bgcolor="#FFFFFF" align="left">
                        <p style="color:#1b0f3a; -webkit-font-smoothing:antialiased; font-size:20px; line-height:20px; margin-bottom:20px; font-weight:700;"><strong>Vos informations de contact</strong></p>
                        <?php if( false === $pour_client ){ ?>
                           <p style="line-height: 20px;">Nom : <span style="font-size: 1.5em; font-weight: bold; color: red"><?= $lname ?></span></p>
                           <p style="line-height: 20px;">Prénom : <span style="font-size: 1.5em; font-weight: bold; color: red"><?= $fname ?></span></p>
                           <p style="line-height: 20px;">Email : <span style="font-size: 1.5em; font-weight: bold; color: red"><?= $email ?></span></p>
                           <p style="line-height: 20px;">Téléphone : <span style="font-size: 1.5em; font-weight: bold; color: red"><?= $telephone ?></span></p>
                     <?php }else{ ?>
                           <p style="line-height: 20px;"><strong>Nom : </strong><?= $lname ?></p>
                           <p style="line-height: 20px;"><strong>Prénom : </strong><?= $fname ?></p>
                           <p style="line-height: 20px;"><strong>Email : </strong><?= $email ?></p>
                           <p style="line-height: 20px;"><strong>Téléphone : </strong><?= $telephone ?></p>
                     <?php } ?> 
                     </td>
                     <td width="40"  class="w40"  bgcolor="#FFFFFF">&nbsp;</td>
                  </tr>
                  </table>
                  <table class="w680" width="680" border="0">
                     <tbody>
                        <tr>
                           <td width="40"  class="w40"  bgcolor="#FFFFFF">&nbsp;</td>
                           <td class="w600" width="600" bgcolor="#FFFFFF" style="border-bottom:2px solid #fa7667; background-color:#FFF;">&nbsp;</td>
                           <td width="40"  class="w40"  bgcolor="#FFFFFF">&nbsp;</td>
                        </tr>
                        <tr>
                           <td colspan="3" height="20" bgcolor="#FFFFFF">&nbsp;</td>
                        </tr>
                     </tbody>
                  </table>
                  <table class="w680" width="680" border="0" cellpadding="0" cellspacing="0" align="center" >
                  <tr>
                     <td colspan="3" bgcolor="#FFFFFF" align="center" valign="top" >
                        <div class="logo">
                           <a href="<?= site_url(); ?>" target="_blank" style="font-family: 'Open Sans', sans-serif; color:#000;  -webkit-font-smoothing:antialiased; font-size:12px; line-height:20px; -webkit-font-smoothing: antialiased; margin-bottom:0px !important; text-decoration:none;">
                           <img style="display: inline-block;" src="<?= IMG_DIR ?>timeline_mail.jpg" class="w600" width="680" alt="vendezmonbien.be" border="0"/>
                           </a>
                        </div>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="3" height="20" bgcolor="#FFFFFF">&nbsp;</td>
                  </tr>
                  <tr >
                     <td width="40"  class="w40"  bgcolor="#FFFFFF">&nbsp;</td>
                     <td width="600" class="w600" bgcolor="#FFFFFF" align="left">
                        <p style="line-height: 20px;">Voici ce que nous avons reçu. <br />Un de nos experts analyse votre dossier,<br>il vous répondra dans les 48H ! TOP Chrono ! </p>
                     </td>
                     <td width="40" class="w40" bgcolor="#FFFFFF">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="3" height="20" bgcolor="#FFFFFF">&nbsp;</td>
                  </tr>
                  <?php if( !isset($_SESSION['mode']) || $_SESSION['mode'] != "plein" ): ?>
                      <tr>
                        <td  bgcolor="#FFFFFF"  colspan="3" height="20" align="center" >
                           <a href="https://leprixdemonbien.be/Nos%2010%20astuces%20du%20parfait%20vendeur%20-%20Vendezmonbien.pdf" target="_blank" style="font-family: 'Open Sans', sans-serif; color:#000;  -webkit-font-smoothing:antialiased; font-size:12px; line-height:20px; -webkit-font-smoothing: antialiased; margin-bottom:0px !important; text-decoration:none;">
                              <img style="display: inline-block;" src="<?= IMG_DIR ?>bouton.png"  width="280" alt="vendezmonbien.be" border="0"/>
                              </a>
                        </td>
                     </tr>
                  <?php endif; ?>
                  <tr>
                     <td class="w40" width="40" bgcolor="#FFFFFF" style="background-color:#FFF;">&nbsp;</td>
                     <td class="w600" width="600" bgcolor="#FFFFFF"  style="border-bottom:2px solid #fa7667; background-color:#FFF;">&nbsp;</td>
                     <td class="w40" width="40" bgcolor="#FFFFFF" style="background-color:#FFF;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td  bgcolor="#FFFFFF"  colspan="3" height="20" >&nbsp;</td>
                  </tr>
                  <!-- Text  -->
                  <tr>
                     <td width="40"  class="w40"  bgcolor="#FFFFFF">&nbsp;</td>
                     <td width="600" class="w600" bgcolor="#FFFFFF" align="left">
                        <p style="color:#1b0f3a; -webkit-font-smoothing:antialiased; font-size:20px; line-height:20px; margin-bottom:20px; font-weight:700;"><strong>Votre demande d'estimation</strong></p>
                        <p style="line-height: 20px;"><strong>Bien &agrave; estimer</strong></p>
                        <p style="line-height: 20px;"><strong>Adresse du bien &agrave; estimer :</strong> <?= $adresse.' ' .$postal. ' '. $ville; ?><br><strong>Type de bien :</strong> <?= $property_type; ?></p>
                        <p style="line-height: 20px;">&nbsp;</p>
                        <p style="line-height: 20px;">&nbsp;</p>
                        <p style="line-height: 20px;"><strong>Charge totale mensuelle :</strong><?= $charge ?> €</p>
                        <table>
                           <tbody>
                              <tr>
                                 <td class="w600" width="600" bgcolor="#FFFFFF" style="border-bottom:2px solid #fa7667; background-color:#FFF;">&nbsp;</td>
                              </tr>
                           </tbody>
                        </table>
                        <p style="line-height: 29px;">&nbsp;</p>
                        <p style="color:#1b0f3a; -webkit-font-smoothing:antialiased; font-size:20px; line-height:20px; margin-bottom:20px; font-weight:700;"><strong>Les informations</strong></p>
                        <table>
                           <tr>
                              <td><strong>Ann&eacute;e de construction : </strong></td>
                              <td><?= $construct_year ?></td>
                           </tr>
                           <tr>
                              <td><strong>Surface habitable : </strong></td>
                              <td><?= $HabitableSurface ?> m<sup>2</sup></td>
                           </tr>
                           <tr>
                              <td><strong>Orientation : </strong></td>
                              <td><?= $orientation ?></td>
                           </tr>
                           <tr>
                              <td><strong>Y a-t-il une terrasse ?</strong></td>
                              <td><?= $terrasse ?> <?php if($terrasse == 'Oui'){ echo '<br>'. $terraseSurface.' m<sup>2</sup>'; } else{ echo "Non" ;} ?> </td>
                           </tr>
                           <tr>
                              <td><strong>Cave : </strong></td>
                              <td><?= ucfirst( $cave ? $cave : 'Non'); ?></td>
                           </tr>
                           <tr>
                              <td><strong>Y a-t-il un grenier ?</strong></td>
                              <td><?= $grenier ?> <?php if($grenier == 'Oui'){ echo '<br>'.$grenierSurface.' m<sup>2</sup>'; } else{ echo "Non" ;} ?> </td>
                           </tr>
                           <tr>
                              <td><strong> Y a-t-il un jardin ?</strong></td>
                              <td><?= $garden_exist ?> <?php if($garden_exist == 'Oui'){ echo '<br>'. $gardenSurface.' m<sup>2</sup>'; } else{ echo "Non" ;} ?> </td>
                           </tr>
                           <tr>
                              <td><strong>Parking : </strong></td>
                              <td><?= ucfirst($parking ? $parking : 'Non'); ?></td>
                           </tr>
                           <?php if($parking == 'Oui'): ?>
                           <tr>
                              <td> - Parking exterieur : </td>
                              <td><?= $parking_exterieur; ?></td>
                           </tr>
                           <tr>
                              <td> - Parking interieur :</td>
                              <td><?= $parking_interieur; ?></td>
                           </tr>
                           <?php endif; ?>

                           <tr>
                              <td><strong>Nombre de façades : </strong></td>
                              <td><?= $facade_nbr ?></td>
                           </tr>
                           <tr>
                              <td><strong>Nombre de chambres : </strong></td>
                              <td><?= $chambre_nbr ?></td>
                           </tr>                           
                           <tr>
                              <td><strong>Nombre de salles de bains : </strong></td>
                              <td><?= $sdb_nbr ?></td>
                           </tr>
                           <tr>
                              <td><strong>Nombre &Eacute;tages bâtiment : </strong></td>
                              <td><?= $floor_batiment_nbr ?></td>
                           </tr>
                           <?php if($property_type == 'Appartement'): ?>
                           <tr>
                              <td><strong>Sur l'étage : </strong></td>
                              <td><?= $floor_batiment ?></td>
                           </tr>
                           <?php endif; ?>
                        </table>
                        <table>
                           <tbody>
                              <tr>
                                 <td class="w600" width="600" bgcolor="#FFFFFF" style="border-bottom:2px solid #fa7667; background-color:#FFF;">&nbsp;</td>
                              </tr>
                           </tbody>
                        </table>
                        <p style="line-height: 29px;">&nbsp;</p>
                        <table>
                           <tr>
                              <td><strong>Etat batiment : </strong></td>
                              <td><?= $batiment ?></td>
                           </tr>
                           <?php if($batiment == 'En bon état'): 
                              if($renovation_date == 'with_date'):
                              
                              ?>
                           <tr>
                              <td><strong>Dernière renovation : </strong></td>
                              <td><?= $renovation_date_nbr ?></td>
                           </tr>
                           <?php else: ?>
                           <tr>
                              <td><strong>Dernière renovation : </strong></td>
                              <td>Je ne sais pas</td>
                           </tr>
                           <?php endif; ?>
                           <?php elseif($batiment == 'Rénové/Neuf'):  
                              if($renew_date == 'with_date'):
                              
                              ?>
                           <tr>
                              <td><strong>Dernière renovation : </strong></td>
                              <td><?= $renew_date_nbr ?></td>
                           </tr>
                           <?php else: ?>
                           <tr>
                              <td><strong>Dernière renovation : </strong></td>
                              <td>Je ne sais pas</td>
                           </tr>
                           <?php endif; ?>
                           <?php endif; ?>
                        </table>
                        <table>
                           <tbody>
                              <tr>
                                 <td class="w600" width="600" bgcolor="#FFFFFF" style="border-bottom:2px solid #fa7667; background-color:#FFF;">&nbsp;</td>
                              </tr>
                           </tbody>
                        </table>
                        <p style="line-height: 29px;">&nbsp;</p>
                        <table>
                           <tr>
                              <td><strong>Toiture : </strong></td>
                              <td><?= $toiture ?></td>
                           </tr>
                           <tr>
                              <td><strong>Chassis/ Vitrage : </strong></td>
                              <td><?= $chassis ?></td>
                           </tr>
                           <tr>
                              <td><strong>Chauffage : </strong></td>
                              <td><?= $chauffage ?></td>
                           </tr>
                           <tr>
                              <td><strong>Cuisine : </strong></td>
                              <td><?= $cuisine ?></td>
                           </tr>
                           <tr>
                              <td><strong>Sanitaires : </strong></td>
                              <td><?= $sanitaire ?></td>
                           </tr>
                           <tr>
                              <td><strong>Revêtement de sol : </strong></td>
                              <td><?= $revetement_sol ?></td>
                           </tr>
                           <tr>
                              <td><strong>Peinture : </strong></td>
                              <td><?= $peinture ?></td>
                           </tr>                           
                           <tr>
                              <td><strong>Eléctricité : </strong></td>
                              <td><?= $electricite ?></td>
                           </tr>
                        </table>
                        <table>
                           <tbody>
                              <tr>
                                 <td class="w600" width="600" bgcolor="#FFFFFF" style="border-bottom:2px solid #fa7667; background-color:#FFF;">&nbsp;</td>
                              </tr>
                           </tbody>
                        </table>
                        <p style="line-height: 29px;">&nbsp;</p>
                        <table>
                           <tr>
                              <td><strong>Nuisances sonores : </strong></td>
                              <td><?= $nuisance ?></td>
                           </tr>
                           <tr>
                              <td><strong>Vis-à-vis : </strong></td>
                              <td><?= $vis_a_vis ?></td>
                           </tr>
                           <tr>
                              <td><strong>Infiltrations /  humidité : </strong></td>
                              <td><?= $infiltration ?></td>
                           </tr>
                           <tr>
                              <td><strong>Environnement / voisinage : </strong></td>
                              <td><?= $environnement ?></td>
                           </tr>                           
                        </table>
                        <table>
                           <tbody>
                              <tr>
                                 <td class="w600" width="600" bgcolor="#FFFFFF" style="border-bottom:2px solid #fa7667; background-color:#FFF;">&nbsp;</td>
                              </tr>
                           </tbody>
                        </table>
                        <p style="line-height: 29px;">&nbsp;</p>
                        <table>
                           <tr>
                              <td><strong>Charme/ Style Global : </strong></td>
                              <td><?= $charme ?></td>
                           </tr>
                           <tr>
                              <td><strong>Luminosité : </strong></td>
                              <td><?= $luminosite ?></td>
                           </tr>
                           <tr>
                              <td><strong>Vue dégagée : </strong></td>
                              <td><?= $vue_degagee ?></td>
                           </tr>
                           <tr>
                              <td><strong>Vue : </strong></td>
                              <td><?= $beneficie; ?></td>
                           </tr>
                           <tr>
                              <td><strong>Standing du bien : </strong></td>
                              <td><?= $standing; ?></td>
                           </tr>
                        </table>
                        <table>
                           <tbody>
                              <tr>
                                 <td class="w600" width="600" bgcolor="#FFFFFF" style="border-bottom:2px solid #fa7667; background-color:#FFF;">&nbsp;</td>
                              </tr>
                           </tbody>
                        </table>
                        <p style="line-height: 29px;">&nbsp;</p>
                        <table>
                           
                           <tr>
                              <td colspan="2"><strong>Autres éléments qui influencent le prix : </strong></td>
                           </tr>
                           <?php if(!empty($influence)): 
                              foreach ($influence as $item_influence ) : ?>
                           <tr>
                              <td colspan="2"><?= $item_influence ?></td>
                           </tr>
                           <?php endforeach; endif; ?>
                        </table>
                        <table>
                           <tbody>
                              <tr>
                                 <td class="w600" width="600" bgcolor="#FFFFFF" style="border-bottom:2px solid #fa7667; background-color:#FFF;">&nbsp;</td>
                              </tr>
                           </tbody>
                        </table>
                        <p style="line-height: 29px;">&nbsp;</p>
                        <table>
                           <tr>
                              <td><strong>Êtes-vous propriétaire du bien ?</strong></td>
                              <td><?= ucfirst($proprietaire_bien ? $proprietaire_bien : 'Non'); ?></td>
                           </tr>
                           <tr>
                              <td><strong>Votre numéro téléphone </strong></td>
                              <td><?= $telephone ?></td>
                           </tr>
                           <tr>
                              <td><strong>Peux-t-on vous appeler ? </strong></td>
                              <?php $dispo = $select_call == 'oui' ? $select_call : 'ne pas me contacter' ?>
                              <?php if( false === $pour_client ){ ?>
                                 <td><span style="font-weight: bold; color: red"> <?= $dispo ?></span></td>
                              <?php }else{ ?>
                                 <td> <?= $dispo ?></td>
                              <?php
                              } 
                              ?>                              
                           </tr> 
                           <?php if($select_call == 'oui'): ?>                            
                               <tr>
                                    <td><strong>Date :</strong></td> 
                                    <?php if( false === $pour_client ){ ?>
                                    <td> <span style="font-size: 1.5em; font-weight: bold; color: red"><?= $date; ?></span></td>
                                    <?php }else{ ?>
                                       <td> <?= $date; ?></td>
                                    <?php } ?> 
                                </tr>
                           <?php endif; ?>
                           <tr>
                              <td><strong>Quel est l'objet de cette estimation ?</strong></td>
                              <td><?= $obje_estim?></td>
                           </tr>
                           <?php if( $prixvente != '' ): ?>
                              <tr>
                                 <td><strong>Quelle est la valeur de votre bien ?</strong></td>
                                  <?php if( false === $pour_client ){ ?>
                                    <td><span style="font-weight: bold; color: red"> <?= $prixvente ?></span></td>
                                 <?php }else{ ?>
                                    <td> <?= $prixvente ?> €</td>
                                 <?php
                                 } 
                                 ?>  
                              </tr>
                           <?php endif; ?>
                           <tr>
                              <td>
                                 <strong>Je certifie que les informations mentionnées sont réelles :</strong>
                              </td>
                              <td> <?= $acceptement ?></td>
                           </tr>
                           <tr>
                              <td><strong>En envoyant ma demande de contact, je déclare accepter que mes données complétées dans ce formulaire soient utilisées pour les buts mentionnés.</strong></td>
                              <td> <?= $rgpd ?></td>
                           </tr>
                           <tr>
                                <td></td> 
                                <td height="15" style="height: 15px">&nbsp;</td>
                           </tr>
                           <tr>
                              <td><strong>Sur une échelle de 0 à 10, quelle est votre motivation à vendre votre bien?</strong></td>
                              <td>Estimation : <?= $motiv; ?></td>
                           </tr>
                        </table>
                        <table>
                           <tbody>
                              <tr>
                                 <td class="w600" width="600" bgcolor="#FFFFFF" style="border-bottom:2px solid #fa7667; background-color:#FFF;">&nbsp;</td>
                              </tr>
                           </tbody>
                        </table>
                        <p style="line-height: 29px;">&nbsp;</p>
                        <table>
                           <tr>
                              <td colspan="2"><strong>Je souhaite être informé par vendezmonbien.be</strong></td>
                           </tr>
                           <tr>
                              <td><strong>Sur l'évolution du marché immobilier </strong></td>
                              <td><?= ucfirst($informe_evolution_marche ? $informe_evolution_marche : 'Non'); ?></td>
                           </tr>
                           <tr>
                              <td><strong>Sur l'achat d'un bien immobilier </strong></td>
                              <td><?= ucfirst($informe_achat_bien ? $informe_achat_bien : 'Non'); ?></td>
                           </tr>
                        </table>
                        <table>
                           <tbody>
                              <tr>
                                 <td class="w600" width="600" bgcolor="#FFFFFF" style=" background-color:#FFF;">&nbsp;</td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                     <td width="40" class="w40" bgcolor="#FFFFFF">&nbsp;</td>
                  </tr>
                  <?php  if( !isset($_SESSION['mode']) || $_SESSION['mode'] != "plein" ): ?>
                     <tr>
                        <td  bgcolor="#FFFFFF"  colspan="3" height="20" align="center" >
                           <a href="https://leprixdemonbien.be/Nos%2010%20astuces%20du%20parfait%20vendeur%20-%20Vendezmonbien.pdf" target="_blank" style="font-family: 'Open Sans', sans-serif; color:#000;  -webkit-font-smoothing:antialiased; font-size:12px; line-height:20px; -webkit-font-smoothing: antialiased; margin-bottom:0px !important; text-decoration:none;">
                              <img style="display: inline-block;" src="<?= IMG_DIR ?>bouton.png"  width="280" alt="vendezmonbien.be" border="0"/>
                              </a>
                        </td>
                     </tr>
                  <?php endif; ?>
                  <tr><td colspan="3" bgcolor="#FFFFFF" style="line-height: 35px">&nbsp;</td></tr>
                  <tr>
                     <td colspan="3" bgcolor="#002850" style="text-align: center;-webkit-font-smoothing:antialiased; font-size:20px; line-height:30px;">
                        <a href="<?= site_url(); ?>" target="_blank"><img style="display: inline-block;" src="<?= IMG_DIR ?>footer-mail-v2.jpg" class="w680" width="680" alt="&copy; vendezmonbien.be" border="0"/></a>
                     </td>
                  </tr>
                  <tr>
                     <td width="53"  class="w53"  bgcolor="#002850">&nbsp;</td>
                     <td width="494" class="w494" bgcolor="#002850" align="center">
                        <p style="color:#FF7800; font-size: 13px; line-height:18px;text-align: center ">
                           <img style="display: inline-block; margin-top: 3px; vertical-align: top" src="<?= IMG_DIR ?>mail_office.png"border="0"/> <a href="mailto:contact@vendezmonbien.be" style="color:#ffffff; text-decoration: none">contact@vendezmonbien.be</a>
                        </p>
                       
                        <!-- <p style="color:#FF7800; font-size: 13px; line-height:18px;text-align: center; margin-top: 10px ">
                            <a href="tel:080012249"><img style="display: inline-block; margin-top: 3px; vertical-align: top;" src="<?= IMG_DIR ?>tel-office.jpg" border="0"/></a>
                        </p> -->
                     </td>
                     <td width="53" class="w53" bgcolor="#002850">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="3" height="37" bgcolor="#002850" align="center" valign="top";></td>
                  </tr>
                  <!-- spacer -->
                  <tr>
                     <td colspan="3" height="40" bgcolor="transparent">&nbsp;</td>
                  </tr>
               </table>
            </td>
         </tr>
         
      </table>
   </body>
</html>

