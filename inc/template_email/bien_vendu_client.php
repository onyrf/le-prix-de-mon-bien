

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <title>Demande d'expertise</title>
      <style type="text/css">
         #outlook a{padding:0;} 
         .ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
         .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span {color:#000000; text-decoration:none !important; border-bottom:none !important; background:none !important; cursor:default; }
         body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} 
         img{-ms-interpolation-mode:bicubic;} 
         body{margin:0; padding:0;}
         img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;display:block; }
         table{border-collapse:collapse !important;border-spacing:0;}
         table, td{mso-table-lspace:0pt !important; mso-table-rspace:0pt !important;}
         body{height:100% !important; margin:0; padding:0; width:100% !important;}
         .appleBody a {color:#68440a; text-decoration: none;}
         .appleFooter a {color:#999999; text-decoration: none;}
         br, strong br, b br, em br, i br { line-height:100%; }
         h1, h2, h3, h4, h5, h6, p, a {  -webkit-font-smoothing: antialiased; margin:0; padding:0}
         strong          {font-weight:700;}    
         @import url(https://fonts.googleapis.com/css?family=Open+Sans&display=swap);
         @media screen and (max-width: 680px) { 
         table[class=w40],   td[class=w40],  img[class=w40]  { width:25px !important; }
         table[class=w600],  td[class=w600], img[class=w600] { width:424px !important; }
         table[class=w680],  td[class=w680], img[class=w680] { width:480px !important; }
         }
         @media screen and (max-width: 480px) { 
         table[class=w40],   td[class=w40],  img[class=w40]  { width:17px !important; }
         table[class=w600],  td[class=w600], img[class=w600] { width:286px !important; }
         table[class=w680],  td[class=w680], img[class=w680] { width:320px !important; }
         }
         @media print{
         table[class=w40],   td[class=w40],  img[class=w40]  { display:none !important; }
         }
          @media only screen and (min-width:480px) {
            .mj-column-px-228 {
            width: 228px !important;
            max-width: 228px;
            }
            .mj-column-px-120 {
            width: 120px !important;
            max-width: 120px;
            }
            .mj-column-px-229 {
            width: 229px !important;
            max-width: 229px;
            }
         }

         @media only screen and (max-width:480px) {
            table.mj-full-width-mobile {
            width: 100% !important;
            }
            td.mj-full-width-mobile {
            width: auto !important;
            }
         }

         @media only screen and (max-width:679px) {
            .col-100 {
            width: 100% !important;
            max-width: 100% !important
            }
            .ou {
            padding: 25px 0 !important
            }
            .hidden-mob {
            display: none !important
            }
            .blcBanner>table>tbody>tr>td{ padding: 35px 0!important; }
         }
      </style>
   </head>
   <body style="background-color:#F9F9F9">
      <table height="100%" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#F9F9F9" >
         <tr style="border-collapse:collapse;" >
            <td width="100%" valign="top" align="center" bgcolor="#F9F9F9">
               <table class="w680" width="680" border="0" cellpadding="0" cellspacing="0" align="center" style="font-family: 'Open Sans', sans-serif; color:#555555 !important; font-size:14px; line-height:26px; -webkit-font-smoothing: antialiased; text-decoration:none;">
                  <!-- spacer -->
                  <tr>
                     <td colspan="3" height="40" bgcolor="#F9F9F9"></td>
                  </tr>
                  <!-- Logo -->
                  <tr>
                     <td colspan="3" bgcolor="#FFFFFF" align="center" valign="top">
                        <div class="logo">
                           <a href="<?= site_url(); ?>" target="_blank" style="font-family: 'Open Sans', sans-serif; color:#000;  -webkit-font-smoothing:antialiased; font-size:12px; line-height:20px; -webkit-font-smoothing: antialiased; margin-bottom:0px !important; text-decoration:none;">
                           <img style="display: inline-block;" src="<?= IMG_DIR ?>banner_particulier.gif" class="w680" width="680" alt="vendezmonbien.be" border="0"/>
                           </a>
                        </div>
                     </td>
                  </tr>
                  <!-- spacer -->
                  <tr>
                     <td colspan="3" height="20" bgcolor="#FFFFFF">&nbsp;</td>
                  </tr>
                  <!-- titre -->
                  <tr>
                    <td width="40"  class="w40"  bgcolor="#FFFFFF">&nbsp;</td>
                    <td width="600" class="w600" bgcolor="#FFFFFF" align="left">
                        <h3 style="color:#fa7667 ;  -webkit-font-smoothing:antialiased; font-size:14px; line-height:20px; margin-bottom:10px; font-weight:700;">vendezmonbien.be
                        </h3>
                           <h1 style="color:#1b0f3a; -webkit-font-smoothing:antialiased; font-size:20px; line-height:20px; margin-bottom:40px; font-weight:700;">Un Bien a été vendu.
                           </h1> 

                        <p style="line-height: 20px;">&nbsp;</p>

                        <p style="color:#2b7895;font-weight:700;font-size:18px">Détail concernant le bien :</p>
                        <p style="color:#2b7895;">
                           <b>Nom du bien :</b> <?= $name ?><br>
                           <b>Référence   :</b> <?= $ref ?><br>
                           <b>ID du bien  :</b> <?= $id ?>
                        </p>
                        <p style="line-height: 20px;">&nbsp;</p>
                        <p style="line-height: 20px;">&nbsp;</p>
                        <p style="color:#2b7895;">
                           <b>Adresse du bien    :</b><br>
                           <?= $estate->number.' '.$estate->address.' '.$estate->zip.' '.$estate->city ?>
                        </p>
                        <p style="line-height: 20px;">&nbsp;</p>
                        <p style="line-height: 20px;">&nbsp;</p>
                        <p style="color:#2b7895;">
                           <?= do_shortcode( "[add_peb post_id=$id]" ) ?>
                        </p>
                        </td>
                        <td width="40" class="w40" bgcolor="#FFFFFF">&nbsp;</td>
                    </tr>
                  <tr>
                     <td class="w40" width="40" bgcolor="#FFFFFF" style="background-color:#FFF;">&nbsp;</td>
                     <td class="w600" width="600" bgcolor="#FFFFFF"  style="border-bottom:2px solid #fa7667; background-color:#FFF;">&nbsp;</td>
                     <td class="w40" width="40" bgcolor="#FFFFFF" style="background-color:#FFF;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td  bgcolor="#FFFFFF"  colspan="3" height="20" >&nbsp;</td>
                  </tr>
                  <!-- Text  -->
                  <tr>
                     <td colspan="3" bgcolor="#002850" style="text-align: center;-webkit-font-smoothing:antialiased; font-size:20px; line-height:30px;">
                        <a href="<?= site_url(); ?>" target="_blank"><img style="display: inline-block;" src="<?= IMG_DIR ?>footer-mail-v2.jpg" class="w680" width="680" alt="&copy; vendezmonbien.be" border="0"/></a>
                     </td>
                  </tr>
                  <tr>
                     <td width="53"  class="w53"  bgcolor="#002850">&nbsp;</td>
                     <td width="494" class="w494" bgcolor="#002850" align="center">
                        <p style="color:#FF7800; font-size: 13px; line-height:18px;text-align: center ">
                           <img style="display: inline-block; margin-top: 3px; vertical-align: top" src="<?= IMG_DIR ?>mail_office.png"border="0"/> <a href="mailto:contact@vendezmonbien.be" style="color:#ffffff; text-decoration: none">contact@vendezmonbien.be</a>
                        </p>
                        <!-- <p style="color:#FF7800; font-size: 13px; line-height:18px;text-align: center; margin-top: 10px ">
                            <a href="tel:080012249"><img style="display: inline-block; margin-top: 3px; vertical-align: top;" src="<?= IMG_DIR ?>tel-office.jpg" border="0"/></a>
                        </p> -->
                     </td>
                     <td width="53" class="w53" bgcolor="#002850">&nbsp;</td>
                  </tr>
                  <tr>
                     <td colspan="3" height="37" bgcolor="#002850" align="center" valign="top";></td>
                  </tr>
                  <!-- spacer -->
                  <tr>
                     <td colspan="3" height="40" bgcolor="transparent">&nbsp;</td>
                  </tr>
               </table>
            </td>
         </tr>
         
      </table>
   </body>
</html>

