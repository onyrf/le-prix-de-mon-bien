<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
   <head>
      <title>
      </title>
      <!--[if !mso]><!-- -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!--<![endif]-->
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style type="text/css">
         #outlook a {
         padding: 0;
         }
         body {
         margin: 0;
         padding: 0;
         -webkit-text-size-adjust: 100%;
         -ms-text-size-adjust: 100%;
         }
         table,
         td {
         border-collapse: collapse;
         mso-table-lspace: 0pt;
         mso-table-rspace: 0pt;
         }
         img {
         border: 0;
         height: auto;
         line-height: 100%;
         outline: none;
         text-decoration: none;
         -ms-interpolation-mode: bicubic;
         }
         p {
         display: block;
         margin: 13px 0;
         }
      </style>
      <!--[if mso]>
      <xml>
         <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <!--[if lte mso 11]>
      <style type="text/css">
         .mj-outlook-group-fix { width:100% !important; }
      </style>
      <![endif]-->
      <style type="text/css">
         @media only screen and (min-width:480px) {
         .mj-column-per-100 {
         width: 100% !important;
         max-width: 100%;
         }
         .mj-column-per-33-333333333333336 {
         width: 33.333333333333336% !important;
         max-width: 33.333333333333336%;
         }
         }
      </style>
      <style type="text/css">
         @media only screen and (max-width:480px) {
         table.mj-full-width-mobile {
         width: 100% !important;
         }
         td.mj-full-width-mobile {
         width: auto !important;
         }
         }
      </style>
      <style type="text/css">
         a {
         text-decoration: none;
         }
         @media only screen and (max-width:480px) {
         .pad_mob>table>tbody>tr>td {
         padding: 40px 20px 40px !important
         }
         .sec_titre>table>tbody>tr>td {
         padding: 35px 20px !important
         }
         .no_pad>table>tbody>tr>td {
         padding: 0 !important
         }
         .pad_col_right>table>tbody>tr>td {
         padding-top: 20px !important
         }
         .top_detail>table>tbody>tr>td {
         padding: 30px 20px 0 !important
         }
         .sec_img>table>tbody>tr>td {
         padding: 0 20px !important
         }
         }
      </style>
   </head>
   <body style="background-color:#dddddd;">
      <div style="background-color:#dddddd;">
         <!--[if mso | IE]>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div style="margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:650px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <!--[if mso | IE]>
                                                                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                                     <tr>
                                                                        <td height="25" style="height:25px;">
                                                                           <![endif]-->
                                                                           <div style="height:25px;"> &nbsp; </div>
                                                                           <!--[if mso | IE]>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                                  <![endif]-->
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div style="margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:650px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:650px;">
                                                                              <a href="http://leprixdemonbien.be/" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/banner_partenaire.gif" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="650" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="pad_mob-outlook" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div class="pad_mob" style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:50px 35px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:580px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" class="titre" style="font-size:0px;padding:0px;padding-bottom:20px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:30px;font-weight:bold;line-height:35px;text-align:center;color:#002850;">Un accès au bien immobilier (réf. <?= $ref ?>) vient d'être acheté !</div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:60px;">
                                                                              <a href="#" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/deco_titre_mail.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="60" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="sec_img-outlook" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div class="sec_img" style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:0 40px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:570px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                         <?php 
                                                            $existing_imgs = get_field('field_60e44529224ad', $propID);
                                                            if( is_array($existing_imgs) && count( $existing_imgs ) ):
                                                              $pic = $existing_imgs[0];
                                                               if( array_key_exists('image_large', $pic)):
                                                         ?>
                                                                  <tr>
                                                                     <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                           <tbody>
                                                                              <tr>
                                                                                 <td style="width:290px;">
                                                                                    <a href="#" target="_blank">
                                                                                    <img height="auto" src="<?= $pic['image_large'] ?>" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="290" />
                                                                                    </a>
                                                                                 </td>
                                                                              </tr>
                                                                           </tbody>
                                                                        </table>
                                                                     </td>
                                                                  </tr>
                                                               <?php endif; ?>
                                                            <?php endif; ?>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="top_detail-outlook" style="vertical-align:top;width:650px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-100 mj-outlook-group-fix top_detail" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:30px 40px 0;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" class="s-titre" style="font-size:0px;padding:0px;padding-top:10px;padding-bottom:10px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:20px;font-weight:bold;line-height:24px;text-align:center;color:#a29f78;"><?= get_field('field_60e43f256c388', $propID ) ?></div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td align="center" class="s-titre" style="font-size:0px;padding:0px;padding-top:0;padding-bottom:10px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:20px;font-weight:bold;line-height:24px;text-align:center;color:#a29f78;"><?= millier( get_field('field_60e442f86c38b', $propID ) ) ?> €</div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td align="center" class="text" style="font-size:0px;padding:0px;padding-top:20px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:16px;line-height:24px;text-align:center;color:#002850;">
                                                                  <?= get_field('field_60e44a6bf62db', $propID ) ?> ch. - <?= get_field('field_60e44887d4dee', $propID ) ?> m² 
                                                               <?= do_shortcode( "[add_peb post_id=$propID]" ) ?>
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="pad_mob-outlook" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div class="pad_mob" style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:50px 35px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:580px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" class="text" style="font-size:0px;padding:0px;padding-top:0;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:16px;line-height:24px;text-align:center;color:#002850;"><?= get_field('field_60e444aaf9629', $propID ) ?>
                                                                  
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:0 0 50px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:650px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;padding-top:0;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:188px;">
                                                                              <a href="<?= get_permalink( $propID ) ?>" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/boutton_voir_fiche.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="188" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="sec_deco1-outlook" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div class="sec_deco1" style="margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:650px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:650px;">
                                                                              <a href="#" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/deco_mail1.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="650" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="sec_list_atout-outlook pad_mob-outlook" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div class="sec_list_atout pad_mob" style="background:#f5f5f5;background-color:#f5f5f5;margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f5f5f5;background-color:#f5f5f5;width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:0 25px 56px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:200px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0 15px 25px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:170px;">
                                                                              <a href="#" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/atout_mail1.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="170" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td align="center" class="text" style="font-size:0px;padding:0px;padding-top:12px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:16px;font-weight:bold;line-height:24px;text-align:center;color:#002850;">Le 1er réseau d’offres immobilières</div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          class="" style="vertical-align:top;width:200px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0 15px 25px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:170px;">
                                                                              <a href="#" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/atout_mail2.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="170" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td align="center" class="text" style="font-size:0px;padding:0px;padding-top:12px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:16px;font-weight:bold;line-height:24px;text-align:center;color:#002850;">Un tableau de bord unique</div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          class="" style="vertical-align:top;width:200px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0 15px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:170px;">
                                                                              <a href="#" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/atout_mail3.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="170" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td align="center" class="text" style="font-size:0px;padding:0px;padding-top:12px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:16px;font-weight:bold;line-height:24px;text-align:center;color:#002850;">Un accès VIP aux offres quotidiennes</div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="sec_deco2-outlook" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div class="sec_deco2" style="margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:650px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:650px;">
                                                                              <a href="#" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/deco_mail2.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="650" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="sec_titre-outlook" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div class="sec_titre" style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:35px 35px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:580px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" class="s-titre" style="font-size:0px;padding:0px;padding-bottom:20px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:24px;line-height:30px;text-align:center;color:#002850;">Pourquoi devenir partenaire ? <br> <strong>Recevez des mandats pour votre région</strong></div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:0 25px 0;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:200px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0 15px 35px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:130px;">
                                                                              <a href="#" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/timeline1.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="130" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td align="center" class="text" style="font-size:0px;padding:0px;padding-top:15px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:16px;line-height:24px;text-align:center;color:#002850;">Vendezmonbien.be<br> vous offre des<br> mandats de vente<br> immobilières</div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          class="" style="vertical-align:top;width:200px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0 15px 35px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:130px;">
                                                                              <a href="#" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/timeline2.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="130" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td align="center" class="text" style="font-size:0px;padding:0px;padding-top:15px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:16px;line-height:24px;text-align:center;color:#002850;">Proposez les biens à vos clients acheteurs ! <br>Vous avez 48H pour <br> bénéficier d'une exclusivité sur ce bien !</div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          class="" style="vertical-align:top;width:200px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0 15px 35px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:130px;">
                                                                              <a href="#" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/timeline1.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="130" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td align="center" class="text" style="font-size:0px;padding:0px;padding-top:15px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:16px;line-height:24px;text-align:center;color:#002850;">Le bien est acheté par<br> votre client, SUPER ! <br>Vous bénéficiez de<br> 80% de commissions <br>sur la vente !</div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:0 25px 5px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:200px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0 15px 35px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:130px;">
                                                                              <a href="#" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/timeline4.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="130" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td align="center" class="text" style="font-size:0px;padding:0px;padding-top:15px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:16px;line-height:24px;text-align:center;color:#002850;">Vous percevez votre<br> commission à <br>l'acte de vente.</div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          class="" style="vertical-align:top;width:200px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0 15px 0;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:130px;">
                                                                              <a href="#" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/timeline5.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="130" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td align="center" class="text" style="font-size:0px;padding:0px;padding-top:15px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:16px;line-height:24px;text-align:center;color:#002850;">Les visites débutent<br>avec des acheteurs<br>qualifiés !</div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          class="" style="vertical-align:top;width:200px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-33-333333333333336 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="footer-outlook" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div class="footer" style="background:#002850;background-color:#002850;margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#002850;background-color:#002850;width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:40px 20px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:610px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:500px;">
                                                                              <a href="<?= site_url(); ?>" target="_blank">
                                                                              <img height="auto" src="<?= IMG_DIR ?>mail-partenaires/logo_foot_mail.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:16px;" width="500" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td align="center" class="text" style="font-size:0px;padding:0px;padding-top:15px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:16px;line-height:24px;text-align:center;color:#ffffff;"><img src="<?= IMG_DIR ?>mail-partenaires/picto_mail.png" style="vertical-align:middle"><a href="mailto:contact@vendezmonbien.be" style="color:#ffffff; text-decoration: none">contact@vendezmonbien.be</a></div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div style="margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" style="vertical-align:top;width:650px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td style="font-size:0px;padding:0px;word-break:break-word;">
                                                                  <!--[if mso | IE]>
                                                                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                                     <tr>
                                                                        <td height="25" style="height:25px;">
                                                                           <![endif]-->
                                                                           <div style="height:25px;"> &nbsp; </div>
                                                                           <!--[if mso | IE]>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                                  <![endif]-->
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <![endif]-->
      </div>
   </body>
</html>